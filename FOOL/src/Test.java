import org.antlr.runtime.*;

import interfaces.Node;

import java.io.*;

public class Test {
	public static void main(String[] args) throws Exception {

		String fileName = "prova.fool";

		/*-------------GENERAZIONE AST-------------*/
		Node ast = getAbstractSintaxTree(fileName);

		/*-TYPECHECKING-*/
		typeChecking(ast);

		/*-----------------CODE-GENERATION-----------------*/
		SVMParser parserASM = codeGeneration(ast, fileName);

		/* ESECUZIONE DEL CODICE */
		executeCode(parserASM);
	}

	private static Node getAbstractSintaxTree(String fileName) throws RecognitionException, IOException {
		ANTLRFileStream input = new ANTLRFileStream(fileName);
		FOOLLexer lexer = new FOOLLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		FOOLParser parser = new FOOLParser(tokens);

		// generazione AST con Id associate a relative entry symbol table
		Node ast = parser.prog();

		System.out.println("You had: " + lexer.lexicalErrors + " lexical errors and " + parser.getNumberOfSyntaxErrors() + " syntax errors.\n");

		System.out.println("-------------------------------------AST----------------------------------------\n");
		System.out.println(ast.toPrint(""));
		System.out.println("--------------------------------------------------------------------------------\n");
		return ast;
	}

	private static void typeChecking(Node ast) {
		Node type = ast.typeCheck(); // Avviene bottom-up
		System.out.println(type.toPrint("Type checking ok! Type of the program is: "));
	}

	private static SVMParser codeGeneration(Node ast, String fileName) throws RecognitionException, IOException {
		// codice oggetto prodotto verr� messo nel file 'prova.fool.asm'
		String code = ast.codeGeneration();
		BufferedWriter out = new BufferedWriter(new FileWriter(fileName + ".asm"));
		out.write(code);
		out.close();
		System.out.println("Code generated! Assembling and running generated code.\n");

		ANTLRFileStream inputASM = new ANTLRFileStream(fileName + ".asm");
		SVMLexer lexerASM = new SVMLexer(inputASM);
		CommonTokenStream tokensASM = new CommonTokenStream(lexerASM);
		SVMParser parserASM = new SVMParser(tokensASM);

		parserASM.assembly();

		if (lexerASM.lexicalErrors > 0 || parserASM.getNumberOfSyntaxErrors() > 0)
			System.exit(1);

		return parserASM;
	}

	private static void executeCode(SVMParser parserASM) {
		System.out.println("Starting Virtual Machine...\n");
		System.out.println("--------------------OUTPUT--------------------");
		ExecuteVM vm = new ExecuteVM(parserASM.code);
		vm.cpu();
		System.out.println("----------------------------------------------");
	}
}