// $ANTLR 3.5.2 C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g 2017-01-30 16:15:26

import ast.*;
import java.util.ArrayList;
import java.util.HashMap;
import lib.*;
import st.*;
import ct.*;
import interfaces.*;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class FOOLParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "AND", "ARROW", "ASS", "BOOL", 
		"CLASS", "CLPAR", "COLON", "COMMA", "COMMENT", "CRPAR", "DIV", "DOT", 
		"ELSE", "EQ", "ERR", "EXTENDS", "FALSE", "FUN", "GE", "ID", "IF", "IN", 
		"INT", "INTEGER", "LE", "LET", "LPAR", "MINUS", "NEW", "NOT", "NULL", 
		"OR", "PLUS", "PRINT", "RPAR", "SEMIC", "THEN", "TIMES", "TRUE", "VAR", 
		"WHITESP"
	};
	public static final int EOF=-1;
	public static final int AND=4;
	public static final int ARROW=5;
	public static final int ASS=6;
	public static final int BOOL=7;
	public static final int CLASS=8;
	public static final int CLPAR=9;
	public static final int COLON=10;
	public static final int COMMA=11;
	public static final int COMMENT=12;
	public static final int CRPAR=13;
	public static final int DIV=14;
	public static final int DOT=15;
	public static final int ELSE=16;
	public static final int EQ=17;
	public static final int ERR=18;
	public static final int EXTENDS=19;
	public static final int FALSE=20;
	public static final int FUN=21;
	public static final int GE=22;
	public static final int ID=23;
	public static final int IF=24;
	public static final int IN=25;
	public static final int INT=26;
	public static final int INTEGER=27;
	public static final int LE=28;
	public static final int LET=29;
	public static final int LPAR=30;
	public static final int MINUS=31;
	public static final int NEW=32;
	public static final int NOT=33;
	public static final int NULL=34;
	public static final int OR=35;
	public static final int PLUS=36;
	public static final int PRINT=37;
	public static final int RPAR=38;
	public static final int SEMIC=39;
	public static final int THEN=40;
	public static final int TIMES=41;
	public static final int TRUE=42;
	public static final int VAR=43;
	public static final int WHITESP=44;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public FOOLParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public FOOLParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return FOOLParser.tokenNames; }
	@Override public String getGrammarFileName() { return "C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g"; }


	/* 
	Inizialmente l'ambiente esterno ha nestinglevel = 0, quindi siccome incremento, lo setto a -1
	all'ingresso di ogni scope il nesting level viene incrementato quando entro nel primo scope 
	passa a 0 il che � il livello dell'ambiente globale, qui sono dichiarate anche le classi (i loro nomi)
	le virtual table (symbol table per i componenti delle classi) sono a nesting level 1
	*/
	private int nestingLevel = -1;

	/*
	String � il nome e STentry � il valore
	Ogni volta che entraimo in uno scope, visitiamo un ambiente, aggiungiamo una hashmap, che avr� posizione coincidente con il nesting level, quindi il frontedell'array � la tabella relativa 
	all'ambiente in cui sono, quindi al livello di nesting level
	*/
	private ArrayList<HashMap<String, STentry>> symTable = new  ArrayList<HashMap<String, STentry>>();

	// la class table mappa il nome della classe alla CTEntry che contiene varie cose tra cui pure la virtual table
	private HashMap<String, CTentry> classTable = new HashMap<String, CTentry>();

	// mappa ID di classi in ID di classi super, mi servir� poi per il sub-typing
	private HashMap<String, String> superType = new HashMap<String,String>();



	// $ANTLR start "prog"
	// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:46:1: prog returns [Node ast] : (e= exp SEMIC | LET c= cllist d= declist IN e= exp SEMIC );
	public final Node prog() throws RecognitionException {
		Node ast = null;


		Node e =null;
		ArrayList<Node> c =null;
		ArrayList<Node> d =null;

		try {
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:47:2: (e= exp SEMIC | LET c= cllist d= declist IN e= exp SEMIC )
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0==FALSE||(LA1_0 >= ID && LA1_0 <= IF)||LA1_0==INTEGER||LA1_0==LPAR||(LA1_0 >= NEW && LA1_0 <= NULL)||LA1_0==PRINT||LA1_0==TRUE) ) {
				alt1=1;
			}
			else if ( (LA1_0==LET) ) {
				alt1=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}

			switch (alt1) {
				case 1 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:47:4: e= exp SEMIC
					{
					pushFollow(FOLLOW_exp_in_prog42);
					e=exp();
					state._fsp--;

					match(input,SEMIC,FOLLOW_SEMIC_in_prog44); 
					ast = new ProgNode(e);
					}
					break;
				case 2 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:48:5: LET c= cllist d= declist IN e= exp SEMIC
					{
					match(input,LET,FOLLOW_LET_in_prog52); 

					              // all'inizio del parsing passo il riferimento della map supertype a FOOLLib per poterlo utilizzare nel typechecking
					              FOOLlib.setSuperTypeMap(superType);
					              nestingLevel++;//Inizialmente voglio che sia la prima hashmap in livello 0
					              HashMap<String, STentry> hm = new HashMap<String, STentry>();
					              symTable.add(hm);
					           
					pushFollow(FOLLOW_cllist_in_prog93);
					c=cllist();
					state._fsp--;

					pushFollow(FOLLOW_declist_in_prog129);
					d=declist();
					state._fsp--;

					match(input,IN,FOLLOW_IN_in_prog153); 
					pushFollow(FOLLOW_exp_in_prog190);
					e=exp();
					state._fsp--;

					match(input,SEMIC,FOLLOW_SEMIC_in_prog192); 
					 symTable.remove(nestingLevel--);//Sto uscendo dallo scope allora rimuovo il fronte e decremento il nesting-level
					                        ast = new ProgLetInNode(c, d, e);
					                       
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "prog"



	// $ANTLR start "cllist"
	// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:75:1: cllist returns [ArrayList<Node> astList] : ( CLASS i= ID ( EXTENDS ei= ID )? LPAR (ffid= ID COLON fft= basic ( COMMA ofid= ID COLON oft= basic )* )? RPAR CLPAR ( FUN mid= ID COLON mt= basic LPAR (mpfid= ID COLON mpft= type ( COMMA mpnid= ID COLON mtnt= type )* )? RPAR ( LET ( VAR vid= ID COLON vty= basic ASS vexp= exp SEMIC )* IN )? mexp= exp SEMIC )* CRPAR )* ;
	public final ArrayList<Node> cllist() throws RecognitionException {
		ArrayList<Node> astList = null;


		Token i=null;
		Token ei=null;
		Token ffid=null;
		Token ofid=null;
		Token mid=null;
		Token mpfid=null;
		Token mpnid=null;
		Token vid=null;
		ParserRuleReturnScope fft =null;
		ParserRuleReturnScope oft =null;
		ParserRuleReturnScope mt =null;
		Node mpft =null;
		Node mtnt =null;
		ParserRuleReturnScope vty =null;
		Node vexp =null;
		Node mexp =null;

		try {
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:76:3: ( ( CLASS i= ID ( EXTENDS ei= ID )? LPAR (ffid= ID COLON fft= basic ( COMMA ofid= ID COLON oft= basic )* )? RPAR CLPAR ( FUN mid= ID COLON mt= basic LPAR (mpfid= ID COLON mpft= type ( COMMA mpnid= ID COLON mtnt= type )* )? RPAR ( LET ( VAR vid= ID COLON vty= basic ASS vexp= exp SEMIC )* IN )? mexp= exp SEMIC )* CRPAR )* )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:76:5: ( CLASS i= ID ( EXTENDS ei= ID )? LPAR (ffid= ID COLON fft= basic ( COMMA ofid= ID COLON oft= basic )* )? RPAR CLPAR ( FUN mid= ID COLON mt= basic LPAR (mpfid= ID COLON mpft= type ( COMMA mpnid= ID COLON mtnt= type )* )? RPAR ( LET ( VAR vid= ID COLON vty= basic ASS vexp= exp SEMIC )* IN )? mexp= exp SEMIC )* CRPAR )*
			{
			astList = new ArrayList<Node>();
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:78:3: ( CLASS i= ID ( EXTENDS ei= ID )? LPAR (ffid= ID COLON fft= basic ( COMMA ofid= ID COLON oft= basic )* )? RPAR CLPAR ( FUN mid= ID COLON mt= basic LPAR (mpfid= ID COLON mpft= type ( COMMA mpnid= ID COLON mtnt= type )* )? RPAR ( LET ( VAR vid= ID COLON vty= basic ASS vexp= exp SEMIC )* IN )? mexp= exp SEMIC )* CRPAR )*
			loop10:
			while (true) {
				int alt10=2;
				int LA10_0 = input.LA(1);
				if ( (LA10_0==CLASS) ) {
					alt10=1;
				}

				switch (alt10) {
				case 1 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:78:4: CLASS i= ID ( EXTENDS ei= ID )? LPAR (ffid= ID COLON fft= basic ( COMMA ofid= ID COLON oft= basic )* )? RPAR CLPAR ( FUN mid= ID COLON mt= basic LPAR (mpfid= ID COLON mpft= type ( COMMA mpnid= ID COLON mtnt= type )* )? RPAR ( LET ( VAR vid= ID COLON vty= basic ASS vexp= exp SEMIC )* IN )? mexp= exp SEMIC )* CRPAR
					{
					match(input,CLASS,FOLLOW_CLASS_in_cllist246); 
					i=(Token)match(input,ID,FOLLOW_ID_in_cllist250); 
					     
					      // la symbol table di livello 0 include STEntry per i nomi delle classi
					      // questo per controllare che nello stesso scope non vengano dichiarate altre cose
					      // con lo stesso nome
					    
					      // l'offset della entry della dichiarazoine della classe non verr� mai utilizzato in quanto
					      // la dichiarazione non viene messa in memoria (stack o heap), quindi mettiamo un numero fittizio    
					      HashMap<String,STentry> hm = symTable.get(nestingLevel);
					      if(hm.put((i!=null?i.getText():null),new STentry(nestingLevel, null, 3333))!=null){ //Vado ad inserire a nstlv 0 il nome della classe, qu� controllo che la classe non sia gi� dichiarata nella lista a lv.0
					        System.out.println("Error: id "+(i!=null?i.getText():null) +" at line "+ (i!=null?i.getLine():0) +" already declared!");
					        System.exit(0);
					      }
					    
					      // creo la CTEntry che conterr�le info della classe
					      CTentry classEntry = new CTentry();
					      //Non ho superclassi per ora
					      CTentry superClassEntry = null;
					    
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:97:4: ( EXTENDS ei= ID )?
					int alt2=2;
					int LA2_0 = input.LA(1);
					if ( (LA2_0==EXTENDS) ) {
						alt2=1;
					}
					switch (alt2) {
						case 1 :
							// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:97:5: EXTENDS ei= ID
							{
							match(input,EXTENDS,FOLLOW_EXTENDS_in_cllist264); 
							ei=(Token)match(input,ID,FOLLOW_ID_in_cllist268); 

							        // se finisco qua dentro significa che estendo da qualcuno, allora devo recuperare la CTEntry
							        // dalla class table relativa alla super classe (controllando che esista) e utilizzare il II costruttore di CTEntry
							        // quella che copia di netto la super-CTentry
							        superClassEntry = classTable.get((ei!=null?ei.getText():null));
							        if(superClassEntry == null){
							           System.out.println("Error: class "+(ei!=null?ei.getText():null) +" at line "+ (ei!=null?ei.getLine():0) +" not declared!");
							           System.exit(0);
							        }
							        // Copio la CTEntry della superclass, andando a ridefinire la vecchia versione
							        classEntry = new CTentry(superClassEntry);
							      
							        //aggiorno la superType map tenendo conto di: chi eredita da chi
							        superType.put((i!=null?i.getText():null), (ei!=null?ei.getText():null));   
							     
							}
							break;

					}

					      //Lista di campi vuota
					          ArrayList<Node> fieldsList = new ArrayList<Node>();         
					          //Lista di metodi vuota
					          ArrayList<Node> methodsList = new ArrayList<Node>();
					          
					          // creo un nodo di tipo classe contenente il nome della classe
					          ClassTypeNode classType = new ClassTypeNode((i!=null?i.getText():null));
					          
					          // creo un nodo classe a cui passo le intestazioni della classe
					          ClassNode c = new ClassNode(classType, fieldsList, methodsList, classEntry, superClassEntry);
					          
					          // lo aggiungo alla lista delle classi (lista che verr�infine restituita)
					          astList.add(c);
					    
					          // Inserisco l'entry anche nella class table (il controllo che non sia un nome gi� usato � stato fatto prima tramite la symbol table).
					          classTable.put((i!=null?i.getText():null), classEntry);
					    
					          
					          //viene creato un nuovo livello e la relativa Symbol Table (anzich� creata vuota):
					          //viene settata alla Virtual Table contenuta dentro la nuova CTentry (quindi gli metto dentro tutti i metodi/campi dell'oggetto)
					          //incremento perch� la virtual table � sempre ad offset 1
					          //in questo modo alla fine riesco a rimuoverla correttamente
					          //i campi e metodi vengono settati a nesting level 1 di default (senza doverglielo passare, vedi addMethod e addField in CTEntry)
					          nestingLevel++; 
					          symTable.add(classEntry.getVirtualTable());
					   
					match(input,LPAR,FOLLOW_LPAR_in_cllist322); 
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:149:10: (ffid= ID COLON fft= basic ( COMMA ofid= ID COLON oft= basic )* )?
					int alt4=2;
					int LA4_0 = input.LA(1);
					if ( (LA4_0==ID) ) {
						alt4=1;
					}
					switch (alt4) {
						case 1 :
							// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:149:12: ffid= ID COLON fft= basic ( COMMA ofid= ID COLON oft= basic )*
							{
							ffid=(Token)match(input,ID,FOLLOW_ID_in_cllist328); 
							match(input,COLON,FOLLOW_COLON_in_cllist330); 
							pushFollow(FOLLOW_basic_in_cllist334);
							fft=basic();
							state._fsp--;

							// lo aggiungo alla CTEntry e alla classe
							           // addField ritorna il nodo field che vado ad aggiungere alla lista dei campi definiti in questa classe
							           // FieldList � la lista di campi della classe, che ho gi� creato sopra, la vado a popolare (sia quella che la CTentry)
							           fieldsList.add(classEntry.addField((ffid!=null?ffid.getText():null),(fft!=null?((FOOLParser.basic_return)fft).ast:null)));
							         
							// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:156:10: ( COMMA ofid= ID COLON oft= basic )*
							loop3:
							while (true) {
								int alt3=2;
								int LA3_0 = input.LA(1);
								if ( (LA3_0==COMMA) ) {
									alt3=1;
								}

								switch (alt3) {
								case 1 :
									// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:156:11: COMMA ofid= ID COLON oft= basic
									{
									match(input,COMMA,FOLLOW_COMMA_in_cllist365); 
									ofid=(Token)match(input,ID,FOLLOW_ID_in_cllist369); 
									match(input,COLON,FOLLOW_COLON_in_cllist371); 
									pushFollow(FOLLOW_basic_in_cllist375);
									oft=basic();
									state._fsp--;

									fieldsList.add(classEntry.addField((ofid!=null?ofid.getText():null),(oft!=null?((FOOLParser.basic_return)oft).ast:null)));
									}
									break;

								default :
									break loop3;
								}
							}

							}
							break;

					}

					match(input,RPAR,FOLLOW_RPAR_in_cllist398); 
					match(input,CLPAR,FOLLOW_CLPAR_in_cllist418); 
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:161:11: ( FUN mid= ID COLON mt= basic LPAR (mpfid= ID COLON mpft= type ( COMMA mpnid= ID COLON mtnt= type )* )? RPAR ( LET ( VAR vid= ID COLON vty= basic ASS vexp= exp SEMIC )* IN )? mexp= exp SEMIC )*
					loop9:
					while (true) {
						int alt9=2;
						int LA9_0 = input.LA(1);
						if ( (LA9_0==FUN) ) {
							alt9=1;
						}

						switch (alt9) {
						case 1 :
							// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:162:7: FUN mid= ID COLON mt= basic LPAR (mpfid= ID COLON mpft= type ( COMMA mpnid= ID COLON mtnt= type )* )? RPAR ( LET ( VAR vid= ID COLON vty= basic ASS vexp= exp SEMIC )* IN )? mexp= exp SEMIC
							{
							match(input,FUN,FOLLOW_FUN_in_cllist428); 
							mid=(Token)match(input,ID,FOLLOW_ID_in_cllist432); 
							match(input,COLON,FOLLOW_COLON_in_cllist434); 
							pushFollow(FOLLOW_basic_in_cllist438);
							mt=basic();
							state._fsp--;



							       // creo il nodo dell'AST
							       MethodNode method = new MethodNode((mid!=null?mid.getText():null), (mt!=null?((FOOLParser.basic_return)mt).ast:null));
							       
							       // aggiungo il metodo alla lista dei metodi
							       methodsList.add(method);
							       
							       // memorizzo il tipo dei parametri il quale andr� insieme al tipo di ritorno a comporre il tipo complessivo del metodo
							       ArrayList<Node> parTypes = new ArrayList<Node>();
							        
							       // creo la symbol table che rappresenta il contesto del metodo
							       nestingLevel++;//Entro dentro lo scope relativo al metodo
							       HashMap<String, STentry> hmn = new HashMap<String,STentry>();
							       symTable.add(hmn);
							       
							       // qua mi salvo le dichiarazioni di variabili (per comodit� non permettiamo parametri funzionali)
							       ArrayList<Node> varList = new ArrayList<Node>();
							       
							       // per quanto riguarda il layout dei metodi devo rifarmi a quello delle funzioni: i parametri iniziano dall'offset 1 e vado ad incremento
							       int parOffset = 1;
							       
							       // le dichiarazioni da -2 e decremento
							       int varOffset = -2;
							       
							       // NOTA: a 0 ho l'AL e a -1 il RA
							       
							match(input,LPAR,FOLLOW_LPAR_in_cllist463); 
							// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:192:10: (mpfid= ID COLON mpft= type ( COMMA mpnid= ID COLON mtnt= type )* )?
							int alt6=2;
							int LA6_0 = input.LA(1);
							if ( (LA6_0==ID) ) {
								alt6=1;
							}
							switch (alt6) {
								case 1 :
									// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:194:12: mpfid= ID COLON mpft= type ( COMMA mpnid= ID COLON mtnt= type )*
									{
									mpfid=(Token)match(input,ID,FOLLOW_ID_in_cllist502); 
									match(input,COLON,FOLLOW_COLON_in_cllist504); 
									pushFollow(FOLLOW_type_in_cllist508);
									mpft=type();
									state._fsp--;


									             ParNode firstPar = new ParNode((mpfid!=null?mpfid.getText():null),mpft);
									             method.addParameter(firstPar);
									             parTypes.add(mpft);
									                        
									             // verifico eventuali duplicati e aggiungo alla symbol table (primo par) 
									             // NOTA: i parametri sono allo stesso livello del corpo della fun
									             if(hmn.put((mpfid!=null?mpfid.getText():null),new STentry(nestingLevel, mpft, parOffset++))!=null){ 
									               System.out.println("Error: id "+(mpfid!=null?mpfid.getText():null) +" at line "+ (mpfid!=null?mpfid.getLine():0) +" already declared!");
									               System.exit(0);
									             };     
									           
									// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:207:12: ( COMMA mpnid= ID COLON mtnt= type )*
									loop5:
									while (true) {
										int alt5=2;
										int LA5_0 = input.LA(1);
										if ( (LA5_0==COMMA) ) {
											alt5=1;
										}

										switch (alt5) {
										case 1 :
											// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:207:13: COMMA mpnid= ID COLON mtnt= type
											{
											match(input,COMMA,FOLLOW_COMMA_in_cllist536); 
											mpnid=(Token)match(input,ID,FOLLOW_ID_in_cllist540); 
											match(input,COLON,FOLLOW_COLON_in_cllist542); 
											pushFollow(FOLLOW_type_in_cllist546);
											mtnt=type();
											state._fsp--;


											              ParNode nextPat = new ParNode((mpnid!=null?mpnid.getText():null),mtnt);
											              method.addParameter(nextPat);
											              parTypes.add(mtnt);
											              
											              if(hmn.put((mpnid!=null?mpnid.getText():null),new STentry(nestingLevel,mtnt, parOffset++))!=null){ 
											                System.out.println("Error: id "+(mpnid!=null?mpnid.getText():null) +" at line "+ (mpnid!=null?mpnid.getLine():0) +" already declared!");
											                System.exit(0);
											              };  
											            
											}
											break;

										default :
											break loop5;
										}
									}

									}
									break;

							}

							match(input,RPAR,FOLLOW_RPAR_in_cllist569); 
							// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:219:7: ( LET ( VAR vid= ID COLON vty= basic ASS vexp= exp SEMIC )* IN )?
							int alt8=2;
							int LA8_0 = input.LA(1);
							if ( (LA8_0==LET) ) {
								alt8=1;
							}
							switch (alt8) {
								case 1 :
									// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:219:8: LET ( VAR vid= ID COLON vty= basic ASS vexp= exp SEMIC )* IN
									{
									match(input,LET,FOLLOW_LET_in_cllist585); 
									// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:221:9: ( VAR vid= ID COLON vty= basic ASS vexp= exp SEMIC )*
									loop7:
									while (true) {
										int alt7=2;
										int LA7_0 = input.LA(1);
										if ( (LA7_0==VAR) ) {
											alt7=1;
										}

										switch (alt7) {
										case 1 :
											// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:221:11: VAR vid= ID COLON vty= basic ASS vexp= exp SEMIC
											{
											match(input,VAR,FOLLOW_VAR_in_cllist606); 
											vid=(Token)match(input,ID,FOLLOW_ID_in_cllist610); 
											match(input,COLON,FOLLOW_COLON_in_cllist612); 
											pushFollow(FOLLOW_basic_in_cllist616);
											vty=basic();
											state._fsp--;

											match(input,ASS,FOLLOW_ASS_in_cllist618); 
											pushFollow(FOLLOW_exp_in_cllist622);
											vexp=exp();
											state._fsp--;

											match(input,SEMIC,FOLLOW_SEMIC_in_cllist624); 

											             VarNode v = new VarNode((vid!=null?vid.getText():null), (vty!=null?((FOOLParser.basic_return)vty).ast:null), vexp);
											             varList.add(v);
											             
											             // verifico eventuali duplicati
											             if(hmn.put((vid!=null?vid.getText():null),new STentry(nestingLevel,(vty!=null?((FOOLParser.basic_return)vty).ast:null), varOffset--))!=null){ 
											                System.out.println("Error: id "+(vty!=null?input.toString(vty.start,vty.stop):null) +" at line "+ (vid!=null?vid.getLine():0) +" already declared!");
											                System.exit(0);
											             };
											          
											}
											break;

										default :
											break loop7;
										}
									}

									method.addDec(varList);
									match(input,IN,FOLLOW_IN_in_cllist661); 
									}
									break;

							}

							 
							        // istanzio il nodo che rappresenta il tipo del metodo (tipi dei parametri, type del metodo)
							        ArrowTypeNode methodType = new ArrowTypeNode(parTypes, (mt!=null?((FOOLParser.basic_return)mt).ast:null));
							        
							        // aggiungo il tipo complessivo al MethodNode
							        method.addSymType(methodType);  
							        
							        // Aggiungo il metodo alla virtual table e ad allMethods
							        // NOTA: � necessario farlo prima di processare l'exp nel caso in cui il metodo richiami se stesso
							        classEntry.addMethod((mid!=null?mid.getText():null), method);
							      
							pushFollow(FOLLOW_exp_in_cllist703);
							mexp=exp();
							state._fsp--;

							method.addBody(mexp);
							match(input,SEMIC,FOLLOW_SEMIC_in_cllist727); 
							symTable.remove(nestingLevel--);
							}
							break;

						default :
							break loop9;
						}
					}

					match(input,CRPAR,FOLLOW_CRPAR_in_cllist760); 
					symTable.remove(nestingLevel--);
					}
					break;

				default :
					break loop10;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return astList;
	}
	// $ANTLR end "cllist"



	// $ANTLR start "declist"
	// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:262:1: declist returns [ArrayList<Node> astList] : ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+ ;
	public final ArrayList<Node> declist() throws RecognitionException {
		ArrayList<Node> astList = null;


		Token i=null;
		Token fid=null;
		Token id=null;
		Node t =null;
		Node e =null;
		Node fty =null;
		Node ty =null;
		ArrayList<Node> d =null;

		try {
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:262:43: ( ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+ )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:262:45: ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+
			{

			      // creo l'arraylist vuoto, esso conterr� le dichiarazioni
			      astList = new ArrayList<Node>();
			      
			      // l'offset mi serve per recuperare le cose in fase di esecuzione
			      // inizializzo l'offset a -2 perch� nel caso di AR dell'ambiente globale a -1 abbiamo il RA fittizio
			      // mentre nel caso di layout AR funzione, a 0 c'� l'AL e a -1 il RA vero
			      int offset = -2;
			    
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:272:5: ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+
			int cnt15=0;
			loop15:
			while (true) {
				int alt15=2;
				int LA15_0 = input.LA(1);
				if ( (LA15_0==FUN||LA15_0==VAR) ) {
					alt15=1;
				}

				switch (alt15) {
				case 1 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:274:5: ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC
					{
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:274:5: ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp )
					int alt14=2;
					int LA14_0 = input.LA(1);
					if ( (LA14_0==VAR) ) {
						alt14=1;
					}
					else if ( (LA14_0==FUN) ) {
						alt14=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 14, 0, input);
						throw nvae;
					}

					switch (alt14) {
						case 1 :
							// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:276:8: VAR i= ID COLON t= type ASS e= exp
							{
							match(input,VAR,FOLLOW_VAR_in_declist834); 
							i=(Token)match(input,ID,FOLLOW_ID_in_declist838); 
							match(input,COLON,FOLLOW_COLON_in_declist840); 
							pushFollow(FOLLOW_type_in_declist844);
							t=type();
							state._fsp--;

							match(input,ASS,FOLLOW_ASS_in_declist846); 
							pushFollow(FOLLOW_exp_in_declist850);
							e=exp();
							state._fsp--;


							          VarNode v = new VarNode((i!=null?i.getText():null), t, e);
							          astList.add(v);
							          // ora che ho dichiarato la var la aggiungo alla symbol table
							          // recupero l'hash table dell'ambiente dove sto parsando
							          HashMap<String,STentry> hm = symTable.get(nestingLevel);
							          // controllo che niente sia dichiarato con lo stesso nome
							          if(hm.put((i!=null?i.getText():null),new STentry(nestingLevel, t, offset--))!=null){ 
							            System.out.println("Error: id "+(i!=null?i.getText():null) +" at line "+ (i!=null?i.getLine():0) +" already declared!");
							            System.exit(0);
							            };
							         
							}
							break;
						case 2 :
							// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:291:8: FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp
							{
							match(input,FUN,FOLLOW_FUN_in_declist886); 
							i=(Token)match(input,ID,FOLLOW_ID_in_declist890); 
							match(input,COLON,FOLLOW_COLON_in_declist892); 
							pushFollow(FOLLOW_type_in_declist896);
							t=type();
							state._fsp--;


							          FunNode f = new FunNode((i!=null?i.getText():null), t); 
							          astList.add(f);  
							          HashMap<String,STentry> hm = symTable.get(nestingLevel);
							          
							         // creo una entry con solo il nesting level e l'offset
							         // ci metter� il tipo quando lo sapr� (lo trover� solo dopo aver letto il tipo di tutti i parametri)
							         STentry entry = new STentry(nestingLevel, offset);
							         
							         // la funzione occupa due offset (vedi layout high order)
							         offset -= 2;
							         
							         // inserisco l'ID della funzione nella symbol table                                               
							         if(hm.put((i!=null?i.getText():null), entry)!=null){
							          System.out.println("Error: id "+(i!=null?i.getText():null) +" at line "+ (i!=null?i.getLine():0) +" already declared!");
							          System.exit(0);
							         };
							         
							         // i parametri assumiamo facciano parte del corpo della funzione
							         // creo una hashmap che rappresenta il contesto interno alla funzione incrementando allora il nestingLvl.
							         nestingLevel++; 
							         HashMap<String,STentry> hmn = new HashMap<String,STentry>();
							         symTable.add(hmn);
							         
							         // creo un array list per mantenere il tipo dei parametri             
							         ArrayList<Node> parTypes = new ArrayList<Node>();
							         int parOffset = 1; //i parametri iniziano da 1 nel layout e l'offset si incrementa (quindi da 1 in su)
							         
							match(input,LPAR,FOLLOW_LPAR_in_declist909); 
							// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:322:11: (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )?
							int alt12=2;
							int LA12_0 = input.LA(1);
							if ( (LA12_0==ID) ) {
								alt12=1;
							}
							switch (alt12) {
								case 1 :
									// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:322:12: fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )*
									{
									fid=(Token)match(input,ID,FOLLOW_ID_in_declist945); 
									match(input,COLON,FOLLOW_COLON_in_declist947); 
									pushFollow(FOLLOW_type_in_declist951);
									fty=type();
									state._fsp--;

									 
									              parTypes.add(fty);
									              ParNode fpar = new ParNode((fid!=null?fid.getText():null), fty);
									              f.addParameter(fpar);
									              
									              // nel caso in cui sia presente qualche parametro di tipo funzionale devo riservare due spazi. 
									              if(fty instanceof ArrowTypeNode){
									                parOffset++;
									              }
									              
									              if(hmn.put((fid!=null?fid.getText():null),new STentry(nestingLevel,fty,parOffset++))!=null){
									                System.out.println("Error: id "+(fid!=null?fid.getText():null) +" at line "+ (fid!=null?fid.getLine():0) +" already declared!");
									                System.exit(0);
									              };
									              
									             
									// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:340:14: ( COMMA id= ID COLON ty= type )*
									loop11:
									while (true) {
										int alt11=2;
										int LA11_0 = input.LA(1);
										if ( (LA11_0==COMMA) ) {
											alt11=1;
										}

										switch (alt11) {
										case 1 :
											// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:340:15: COMMA id= ID COLON ty= type
											{
											match(input,COMMA,FOLLOW_COMMA_in_declist1025); 
											id=(Token)match(input,ID,FOLLOW_ID_in_declist1029); 
											match(input,COLON,FOLLOW_COLON_in_declist1031); 
											pushFollow(FOLLOW_type_in_declist1035);
											ty=type();
											state._fsp--;

											 
											                parTypes.add(ty);
											                ParNode par = new ParNode((id!=null?id.getText():null), ty);
											                f.addParameter(par);
											                if(ty instanceof ArrowTypeNode){parOffset++;}
											                if(hmn.put((id!=null?id.getText():null),new STentry(nestingLevel,ty, parOffset++))!=null){
											                  System.out.println("Error: id "+(id!=null?id.getText():null) +" at line "+ (id!=null?id.getLine():0) +" already declared!");
											                  System.exit(0);
											                };
											               
											               
											}
											break;

										default :
											break loop11;
										}
									}

									}
									break;

							}

							match(input,RPAR,FOLLOW_RPAR_in_declist1101); 
							// ora posso istanziare il nodo che rappresenta il tipo della funzione
							            ArrowTypeNode functionType = new ArrowTypeNode(parTypes,t);
							            entry.addType(functionType);
							            // aggiungo il tipo anche al FunNode
							            f.addSymType(functionType);
							          
							// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:361:10: ( LET d= declist IN )?
							int alt13=2;
							int LA13_0 = input.LA(1);
							if ( (LA13_0==LET) ) {
								alt13=1;
							}
							switch (alt13) {
								case 1 :
									// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:361:11: LET d= declist IN
									{
									match(input,LET,FOLLOW_LET_in_declist1126); 
									pushFollow(FOLLOW_declist_in_declist1130);
									d=declist();
									state._fsp--;

									match(input,IN,FOLLOW_IN_in_declist1132); 
									f.addDeclarations(d);
									}
									break;

							}

							pushFollow(FOLLOW_exp_in_declist1139);
							e=exp();
							state._fsp--;

							//chiudo lo scope
							            symTable.remove(nestingLevel--);
							            f.addBody(e);
							          
							}
							break;

					}

					match(input,SEMIC,FOLLOW_SEMIC_in_declist1164); 
					}
					break;

				default :
					if ( cnt15 >= 1 ) break loop15;
					EarlyExitException eee = new EarlyExitException(15, input);
					throw eee;
				}
				cnt15++;
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return astList;
	}
	// $ANTLR end "declist"



	// $ANTLR start "exp"
	// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:372:1: exp returns [Node ast] : v= term ( PLUS l= term | MINUS l= term | OR l= term )* ;
	public final Node exp() throws RecognitionException {
		Node ast = null;


		Node v =null;
		Node l =null;

		try {
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:372:24: (v= term ( PLUS l= term | MINUS l= term | OR l= term )* )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:373:3: v= term ( PLUS l= term | MINUS l= term | OR l= term )*
			{
			pushFollow(FOLLOW_term_in_exp1236);
			v=term();
			state._fsp--;

			ast = v;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:374:9: ( PLUS l= term | MINUS l= term | OR l= term )*
			loop16:
			while (true) {
				int alt16=4;
				switch ( input.LA(1) ) {
				case PLUS:
					{
					alt16=1;
					}
					break;
				case MINUS:
					{
					alt16=2;
					}
					break;
				case OR:
					{
					alt16=3;
					}
					break;
				}
				switch (alt16) {
				case 1 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:375:12: PLUS l= term
					{
					match(input,PLUS,FOLLOW_PLUS_in_exp1261); 
					pushFollow(FOLLOW_term_in_exp1267);
					l=term();
					state._fsp--;

					ast = new PlusNode(ast,l);
					}
					break;
				case 2 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:376:12: MINUS l= term
					{
					match(input,MINUS,FOLLOW_MINUS_in_exp1282); 
					pushFollow(FOLLOW_term_in_exp1288);
					l=term();
					state._fsp--;

					ast = new MinusNode(ast,l);
					}
					break;
				case 3 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:377:12: OR l= term
					{
					match(input,OR,FOLLOW_OR_in_exp1303); 
					pushFollow(FOLLOW_term_in_exp1310);
					l=term();
					state._fsp--;

					ast = new OrNode(ast,l);
					}
					break;

				default :
					break loop16;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "exp"



	// $ANTLR start "term"
	// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:383:1: term returns [Node ast] : f= factor ( TIMES l= factor | DIV l= factor | AND l= factor )* ;
	public final Node term() throws RecognitionException {
		Node ast = null;


		Node f =null;
		Node l =null;

		try {
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:384:2: (f= factor ( TIMES l= factor | DIV l= factor | AND l= factor )* )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:384:4: f= factor ( TIMES l= factor | DIV l= factor | AND l= factor )*
			{
			pushFollow(FOLLOW_factor_in_term1354);
			f=factor();
			state._fsp--;

			ast = f;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:385:6: ( TIMES l= factor | DIV l= factor | AND l= factor )*
			loop17:
			while (true) {
				int alt17=4;
				switch ( input.LA(1) ) {
				case TIMES:
					{
					alt17=1;
					}
					break;
				case DIV:
					{
					alt17=2;
					}
					break;
				case AND:
					{
					alt17=3;
					}
					break;
				}
				switch (alt17) {
				case 1 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:386:9: TIMES l= factor
					{
					match(input,TIMES,FOLLOW_TIMES_in_term1373); 
					pushFollow(FOLLOW_factor_in_term1379);
					l=factor();
					state._fsp--;

					ast = new MultNode (ast,l);
					}
					break;
				case 2 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:387:9: DIV l= factor
					{
					match(input,DIV,FOLLOW_DIV_in_term1391); 
					pushFollow(FOLLOW_factor_in_term1398);
					l=factor();
					state._fsp--;

					ast = new DivNode(ast,l);
					}
					break;
				case 3 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:388:9: AND l= factor
					{
					match(input,AND,FOLLOW_AND_in_term1410); 
					pushFollow(FOLLOW_factor_in_term1417);
					l=factor();
					state._fsp--;

					ast = new AndNode(ast,l);
					}
					break;

				default :
					break loop17;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "term"



	// $ANTLR start "factor"
	// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:394:1: factor returns [Node ast] : f= value ( EQ l= value | GE v= value | LE v= value )* ;
	public final Node factor() throws RecognitionException {
		Node ast = null;


		Node f =null;
		Node l =null;
		Node v =null;

		try {
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:395:2: (f= value ( EQ l= value | GE v= value | LE v= value )* )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:395:4: f= value ( EQ l= value | GE v= value | LE v= value )*
			{
			pushFollow(FOLLOW_value_in_factor1451);
			f=value();
			state._fsp--;

			ast = f;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:396:6: ( EQ l= value | GE v= value | LE v= value )*
			loop18:
			while (true) {
				int alt18=4;
				switch ( input.LA(1) ) {
				case EQ:
					{
					alt18=1;
					}
					break;
				case GE:
					{
					alt18=2;
					}
					break;
				case LE:
					{
					alt18=3;
					}
					break;
				}
				switch (alt18) {
				case 1 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:397:9: EQ l= value
					{
					match(input,EQ,FOLLOW_EQ_in_factor1470); 
					pushFollow(FOLLOW_value_in_factor1476);
					l=value();
					state._fsp--;

					ast = new EqualNode (ast,l);
					}
					break;
				case 2 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:398:9: GE v= value
					{
					match(input,GE,FOLLOW_GE_in_factor1488); 
					pushFollow(FOLLOW_value_in_factor1494);
					v=value();
					state._fsp--;

					ast = new GreaterEqualNode(ast,v);
					}
					break;
				case 3 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:399:10: LE v= value
					{
					match(input,LE,FOLLOW_LE_in_factor1507); 
					pushFollow(FOLLOW_value_in_factor1513);
					v=value();
					state._fsp--;

					ast = new LowerEqualNode(ast,v);
					}
					break;

				default :
					break loop18;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "factor"



	// $ANTLR start "value"
	// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:405:1: value returns [Node ast] : (n= INTEGER | TRUE | FALSE | NULL | LPAR e= exp RPAR | IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR | PRINT LPAR e= exp RPAR | NOT LPAR e= exp RPAR | NEW nid= ID LPAR (nfe= exp ( COMMA nne= exp )* )? RPAR |i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR | DOT mid= ID LPAR (fe= exp ( COMMA ne= exp )* )? RPAR )? );
	public final Node value() throws RecognitionException {
		Node ast = null;


		Token n=null;
		Token nid=null;
		Token i=null;
		Token mid=null;
		Node e =null;
		Node x =null;
		Node y =null;
		Node z =null;
		Node nfe =null;
		Node nne =null;
		Node fa =null;
		Node a =null;
		Node fe =null;
		Node ne =null;

		try {
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:406:2: (n= INTEGER | TRUE | FALSE | NULL | LPAR e= exp RPAR | IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR | PRINT LPAR e= exp RPAR | NOT LPAR e= exp RPAR | NEW nid= ID LPAR (nfe= exp ( COMMA nne= exp )* )? RPAR |i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR | DOT mid= ID LPAR (fe= exp ( COMMA ne= exp )* )? RPAR )? )
			int alt26=10;
			switch ( input.LA(1) ) {
			case INTEGER:
				{
				alt26=1;
				}
				break;
			case TRUE:
				{
				alt26=2;
				}
				break;
			case FALSE:
				{
				alt26=3;
				}
				break;
			case NULL:
				{
				alt26=4;
				}
				break;
			case LPAR:
				{
				alt26=5;
				}
				break;
			case IF:
				{
				alt26=6;
				}
				break;
			case PRINT:
				{
				alt26=7;
				}
				break;
			case NOT:
				{
				alt26=8;
				}
				break;
			case NEW:
				{
				alt26=9;
				}
				break;
			case ID:
				{
				alt26=10;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 26, 0, input);
				throw nvae;
			}
			switch (alt26) {
				case 1 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:406:4: n= INTEGER
					{
					n=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_value1563); 
					ast = new IntNode(Integer.parseInt((n!=null?n.getText():null)));
					}
					break;
				case 2 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:408:4: TRUE
					{
					match(input,TRUE,FOLLOW_TRUE_in_value1574); 
					ast = new BoolNode(true);
					}
					break;
				case 3 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:410:4: FALSE
					{
					match(input,FALSE,FOLLOW_FALSE_in_value1585); 
					ast = new BoolNode(false);
					}
					break;
				case 4 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:412:4: NULL
					{
					match(input,NULL,FOLLOW_NULL_in_value1596); 
					ast = new EmptyNode();
					}
					break;
				case 5 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:414:4: LPAR e= exp RPAR
					{
					match(input,LPAR,FOLLOW_LPAR_in_value1605); 
					pushFollow(FOLLOW_exp_in_value1609);
					e=exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_value1611); 
					ast = e;
					}
					break;
				case 6 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:416:4: IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR
					{
					match(input,IF,FOLLOW_IF_in_value1622); 
					pushFollow(FOLLOW_exp_in_value1626);
					x=exp();
					state._fsp--;

					match(input,THEN,FOLLOW_THEN_in_value1628); 
					match(input,CLPAR,FOLLOW_CLPAR_in_value1630); 
					pushFollow(FOLLOW_exp_in_value1634);
					y=exp();
					state._fsp--;

					match(input,CRPAR,FOLLOW_CRPAR_in_value1636); 
					match(input,ELSE,FOLLOW_ELSE_in_value1638); 
					match(input,CLPAR,FOLLOW_CLPAR_in_value1640); 
					pushFollow(FOLLOW_exp_in_value1644);
					z=exp();
					state._fsp--;

					match(input,CRPAR,FOLLOW_CRPAR_in_value1646); 
					ast = new IfNode(x,y,z);
					}
					break;
				case 7 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:418:4: PRINT LPAR e= exp RPAR
					{
					match(input,PRINT,FOLLOW_PRINT_in_value1657); 
					match(input,LPAR,FOLLOW_LPAR_in_value1659); 
					pushFollow(FOLLOW_exp_in_value1663);
					e=exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_value1665); 
					ast = new PrintNode(e);
					}
					break;
				case 8 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:420:4: NOT LPAR e= exp RPAR
					{
					match(input,NOT,FOLLOW_NOT_in_value1678); 
					match(input,LPAR,FOLLOW_LPAR_in_value1680); 
					pushFollow(FOLLOW_exp_in_value1684);
					e=exp();
					state._fsp--;

					ast = new NotNode(e);
					match(input,RPAR,FOLLOW_RPAR_in_value1688); 
					}
					break;
				case 9 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:422:4: NEW nid= ID LPAR (nfe= exp ( COMMA nne= exp )* )? RPAR
					{
					match(input,NEW,FOLLOW_NEW_in_value1697); 
					nid=(Token)match(input,ID,FOLLOW_ID_in_value1701); 
					match(input,LPAR,FOLLOW_LPAR_in_value1703); 

					          // creo la lista dei parametri che contengono le espressioni passate alla new
					          ArrayList<Node> parList = new ArrayList<Node>();
					          
					          // recupero la CTEntry che descrive la classe
					          CTentry classEntry = classTable.get((nid!=null?nid.getText():null));
					          
					          // verifico che sia effettivamente definita
					          if(classEntry == null){
					            System.out.println("Error: class "+(nid!=null?nid.getText():null)+" at line " +(nid!=null?nid.getLine():0)+ " not declared!");
					            System.exit(0);
					          }
					          
					          // creo il new node
					          NewNode nn = new NewNode((nid!=null?nid.getText():null), classEntry, parList);
					        
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:439:9: (nfe= exp ( COMMA nne= exp )* )?
					int alt20=2;
					int LA20_0 = input.LA(1);
					if ( (LA20_0==FALSE||(LA20_0 >= ID && LA20_0 <= IF)||LA20_0==INTEGER||LA20_0==LPAR||(LA20_0 >= NEW && LA20_0 <= NULL)||LA20_0==PRINT||LA20_0==TRUE) ) {
						alt20=1;
					}
					switch (alt20) {
						case 1 :
							// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:439:10: nfe= exp ( COMMA nne= exp )*
							{
							pushFollow(FOLLOW_exp_in_value1726);
							nfe=exp();
							state._fsp--;

							parList.add(nfe);
							// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:439:43: ( COMMA nne= exp )*
							loop19:
							while (true) {
								int alt19=2;
								int LA19_0 = input.LA(1);
								if ( (LA19_0==COMMA) ) {
									alt19=1;
								}

								switch (alt19) {
								case 1 :
									// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:439:44: COMMA nne= exp
									{
									match(input,COMMA,FOLLOW_COMMA_in_value1731); 
									pushFollow(FOLLOW_exp_in_value1735);
									nne=exp();
									state._fsp--;

									parList.add(nne);
									}
									break;

								default :
									break loop19;
								}
							}

							}
							break;

					}

					match(input,RPAR,FOLLOW_RPAR_in_value1763); 
					ast = nn;
					}
					break;
				case 10 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:443:4: i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR | DOT mid= ID LPAR (fe= exp ( COMMA ne= exp )* )? RPAR )?
					{
					i=(Token)match(input,ID,FOLLOW_ID_in_value1774); 
					 //Cerco la dichiarazione (pallina) da attaccare alla foglia dell'albero
						    int j=nestingLevel;
						    STentry entry=null; //Qu� metter� la mia pallina se la trovo
						    while(j>=0 && entry == null)//Qu� scorro tutte le tabelle fino a che non lo trovo o che arrivo alla fine della lista di tabelle
						      entry = symTable.get(j--).get((i!=null?i.getText():null)); //Qu� ottengo la hashmap e vado a controllare che ci sia l'id che cerco
						    
						    if(entry==null){//Se anche dopo il ciclo while � null allora non esiste
						      System.out.println("Id: " + (i!=null?i.getText():null) + " at line: " + (i!=null?i.getLine():0) + " not declared!"); 
					        System.exit(0);
					      }
						    
						    //Se arriviamo qu� invece entry � diverso da null, quindi abbiamo trovato il valore (pallina) da attaccare, quindi idnode ha un altro figlio 
						    //Quindi gli metto il nome, la pallina con il valore, nestinglevel
						    ast = new IdNode((i!=null?i.getText():null), entry, nestingLevel);
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:467:5: ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR | DOT mid= ID LPAR (fe= exp ( COMMA ne= exp )* )? RPAR )?
					int alt25=3;
					int LA25_0 = input.LA(1);
					if ( (LA25_0==LPAR) ) {
						alt25=1;
					}
					else if ( (LA25_0==DOT) ) {
						alt25=2;
					}
					switch (alt25) {
						case 1 :
							// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:467:7: LPAR (fa= exp ( COMMA a= exp )* )? RPAR
							{
							match(input,LPAR,FOLLOW_LPAR_in_value1837); 
							ArrayList<Node> argList = new ArrayList<Node>();
							// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:468:9: (fa= exp ( COMMA a= exp )* )?
							int alt22=2;
							int LA22_0 = input.LA(1);
							if ( (LA22_0==FALSE||(LA22_0 >= ID && LA22_0 <= IF)||LA22_0==INTEGER||LA22_0==LPAR||(LA22_0 >= NEW && LA22_0 <= NULL)||LA22_0==PRINT||LA22_0==TRUE) ) {
								alt22=1;
							}
							switch (alt22) {
								case 1 :
									// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:468:10: fa= exp ( COMMA a= exp )*
									{
									pushFollow(FOLLOW_exp_in_value1852);
									fa=exp();
									state._fsp--;

									argList.add(fa);
									// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:468:41: ( COMMA a= exp )*
									loop21:
									while (true) {
										int alt21=2;
										int LA21_0 = input.LA(1);
										if ( (LA21_0==COMMA) ) {
											alt21=1;
										}

										switch (alt21) {
										case 1 :
											// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:468:42: COMMA a= exp
											{
											match(input,COMMA,FOLLOW_COMMA_in_value1857); 
											pushFollow(FOLLOW_exp_in_value1861);
											a=exp();
											state._fsp--;

											argList.add(a);
											}
											break;

										default :
											break loop21;
										}
									}

									}
									break;

							}

							match(input,RPAR,FOLLOW_RPAR_in_value1877); 
							ast = new CallNode((i!=null?i.getText():null), entry, argList, nestingLevel);
							}
							break;
						case 2 :
							// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:473:7: DOT mid= ID LPAR (fe= exp ( COMMA ne= exp )* )? RPAR
							{
							match(input,DOT,FOLLOW_DOT_in_value1907); 
							mid=(Token)match(input,ID,FOLLOW_ID_in_value1911); 

							         // la variabile che punta all'oggetto � dichiarata nello stack
							         // l'id dell'oggetto della classe � catturato da i settato sopra
							              
							         // Verifico se classe e metodo esistono
							              
							         // ricavo il tipo dell'oggetto
							         ClassTypeNode classType = (ClassTypeNode)entry.getType();
							         
							         // cerco nella class table la classe
							         CTentry classEntry = classTable.get(classType.getType());
							         
							         if(classEntry == null){
							            System.out.println("Error: class "+classType.getType()+" at line " +(i!=null?i.getLine():0)+ " not declared!");
							            System.exit(0);
							         } 
							         
							         // cerco il metodo nella virtual table relativa all'oggetto in esame
							         STentry methodEntry = classEntry.getVirtualTable().get((mid!=null?mid.getText():null));
							         if(methodEntry == null){
							           System.out.println("Error: method "+(mid!=null?mid.getText():null)+" at line " +(mid!=null?mid.getLine():0)+ " not defined!");
							           System.exit(0);
							         }
							    
							 // creo una lista di parametri
							      ArrayList<Node> parList = new ArrayList<Node>();
							    
							match(input,LPAR,FOLLOW_LPAR_in_value1939); 
							// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:503:10: (fe= exp ( COMMA ne= exp )* )?
							int alt24=2;
							int LA24_0 = input.LA(1);
							if ( (LA24_0==FALSE||(LA24_0 >= ID && LA24_0 <= IF)||LA24_0==INTEGER||LA24_0==LPAR||(LA24_0 >= NEW && LA24_0 <= NULL)||LA24_0==PRINT||LA24_0==TRUE) ) {
								alt24=1;
							}
							switch (alt24) {
								case 1 :
									// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:503:11: fe= exp ( COMMA ne= exp )*
									{
									pushFollow(FOLLOW_exp_in_value1946);
									fe=exp();
									state._fsp--;

									parList.add(fe);
									// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:503:44: ( COMMA ne= exp )*
									loop23:
									while (true) {
										int alt23=2;
										int LA23_0 = input.LA(1);
										if ( (LA23_0==COMMA) ) {
											alt23=1;
										}

										switch (alt23) {
										case 1 :
											// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:503:45: COMMA ne= exp
											{
											match(input,COMMA,FOLLOW_COMMA_in_value1951); 
											pushFollow(FOLLOW_exp_in_value1955);
											ne=exp();
											state._fsp--;

											parList.add(ne);
											}
											break;

										default :
											break loop23;
										}
									}

									}
									break;

							}

							match(input,RPAR,FOLLOW_RPAR_in_value1964); 
							// creo il nodo che verr� restituito
							     ast = new ClassCallNode((i!=null?i.getText():null), entry, (mid!=null?mid.getText():null), methodEntry, parList, nestingLevel);
							    
							}
							break;

					}

					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "value"



	// $ANTLR start "type"
	// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:510:1: type returns [Node ast] : (b= basic |a= arrow );
	public final Node type() throws RecognitionException {
		Node ast = null;


		ParserRuleReturnScope b =null;
		Node a =null;

		try {
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:510:27: (b= basic |a= arrow )
			int alt27=2;
			int LA27_0 = input.LA(1);
			if ( (LA27_0==BOOL||LA27_0==ID||LA27_0==INT) ) {
				alt27=1;
			}
			else if ( (LA27_0==LPAR) ) {
				alt27=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 27, 0, input);
				throw nvae;
			}

			switch (alt27) {
				case 1 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:511:10: b= basic
					{
					pushFollow(FOLLOW_basic_in_type2004);
					b=basic();
					state._fsp--;

					ast = (b!=null?((FOOLParser.basic_return)b).ast:null);
					}
					break;
				case 2 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:512:10: a= arrow
					{
					pushFollow(FOLLOW_arrow_in_type2022);
					a=arrow();
					state._fsp--;

					ast = a;
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "type"


	public static class basic_return extends ParserRuleReturnScope {
		public Node ast;
	};


	// $ANTLR start "basic"
	// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:517:1: basic returns [Node ast] : ( INT | BOOL |i= ID );
	public final FOOLParser.basic_return basic() throws RecognitionException {
		FOOLParser.basic_return retval = new FOOLParser.basic_return();
		retval.start = input.LT(1);

		Token i=null;

		try {
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:517:27: ( INT | BOOL |i= ID )
			int alt28=3;
			switch ( input.LA(1) ) {
			case INT:
				{
				alt28=1;
				}
				break;
			case BOOL:
				{
				alt28=2;
				}
				break;
			case ID:
				{
				alt28=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 28, 0, input);
				throw nvae;
			}
			switch (alt28) {
				case 1 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:518:5: INT
					{
					match(input,INT,FOLLOW_INT_in_basic2061); 
					retval.ast = new IntTypeNode();
					}
					break;
				case 2 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:519:5: BOOL
					{
					match(input,BOOL,FOLLOW_BOOL_in_basic2072); 
					retval.ast = new BoolTypeNode();
					}
					break;
				case 3 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:520:5: i= ID
					{
					i=(Token)match(input,ID,FOLLOW_ID_in_basic2086); 
					retval.ast = new ClassTypeNode((i!=null?i.getText():null));
					}
					break;

			}
			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "basic"



	// $ANTLR start "arrow"
	// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:524:2: arrow returns [Node ast] : LPAR (t= type ( COMMA t= type )* )? RPAR ARROW b= basic ;
	public final Node arrow() throws RecognitionException {
		Node ast = null;


		Node t =null;
		ParserRuleReturnScope b =null;

		try {
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:524:28: ( LPAR (t= type ( COMMA t= type )* )? RPAR ARROW b= basic )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:525:3: LPAR (t= type ( COMMA t= type )* )? RPAR ARROW b= basic
			{

			    //lista dei parametri
			    ArrayList<Node> parList = new ArrayList<Node>();
			  
			match(input,LPAR,FOLLOW_LPAR_in_arrow2154); 
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:529:8: (t= type ( COMMA t= type )* )?
			int alt30=2;
			int LA30_0 = input.LA(1);
			if ( (LA30_0==BOOL||LA30_0==ID||LA30_0==INT||LA30_0==LPAR) ) {
				alt30=1;
			}
			switch (alt30) {
				case 1 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:529:9: t= type ( COMMA t= type )*
					{
					pushFollow(FOLLOW_type_in_arrow2159);
					t=type();
					state._fsp--;

					parList.add(t);
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:530:3: ( COMMA t= type )*
					loop29:
					while (true) {
						int alt29=2;
						int LA29_0 = input.LA(1);
						if ( (LA29_0==COMMA) ) {
							alt29=1;
						}

						switch (alt29) {
						case 1 :
							// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:530:4: COMMA t= type
							{
							match(input,COMMA,FOLLOW_COMMA_in_arrow2167); 
							pushFollow(FOLLOW_type_in_arrow2171);
							t=type();
							state._fsp--;

							parList.add(t);
							}
							break;

						default :
							break loop29;
						}
					}

					}
					break;

			}

			match(input,RPAR,FOLLOW_RPAR_in_arrow2183); 
			match(input,ARROW,FOLLOW_ARROW_in_arrow2185); 
			pushFollow(FOLLOW_basic_in_arrow2189);
			b=basic();
			state._fsp--;

			ast = new ArrowTypeNode(parList,(b!=null?((FOOLParser.basic_return)b).ast:null));
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "arrow"

	// Delegated rules



	public static final BitSet FOLLOW_exp_in_prog42 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_SEMIC_in_prog44 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LET_in_prog52 = new BitSet(new long[]{0x0000080000200100L});
	public static final BitSet FOLLOW_cllist_in_prog93 = new BitSet(new long[]{0x0000080000200000L});
	public static final BitSet FOLLOW_declist_in_prog129 = new BitSet(new long[]{0x0000000002000000L});
	public static final BitSet FOLLOW_IN_in_prog153 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_prog190 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_SEMIC_in_prog192 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_CLASS_in_cllist246 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_cllist250 = new BitSet(new long[]{0x0000000040080000L});
	public static final BitSet FOLLOW_EXTENDS_in_cllist264 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_cllist268 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_cllist322 = new BitSet(new long[]{0x0000004000800000L});
	public static final BitSet FOLLOW_ID_in_cllist328 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_cllist330 = new BitSet(new long[]{0x0000000004800080L});
	public static final BitSet FOLLOW_basic_in_cllist334 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_cllist365 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_cllist369 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_cllist371 = new BitSet(new long[]{0x0000000004800080L});
	public static final BitSet FOLLOW_basic_in_cllist375 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_cllist398 = new BitSet(new long[]{0x0000000000000200L});
	public static final BitSet FOLLOW_CLPAR_in_cllist418 = new BitSet(new long[]{0x0000000000202000L});
	public static final BitSet FOLLOW_FUN_in_cllist428 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_cllist432 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_cllist434 = new BitSet(new long[]{0x0000000004800080L});
	public static final BitSet FOLLOW_basic_in_cllist438 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_cllist463 = new BitSet(new long[]{0x0000004000800000L});
	public static final BitSet FOLLOW_ID_in_cllist502 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_cllist504 = new BitSet(new long[]{0x0000000044800080L});
	public static final BitSet FOLLOW_type_in_cllist508 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_cllist536 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_cllist540 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_cllist542 = new BitSet(new long[]{0x0000000044800080L});
	public static final BitSet FOLLOW_type_in_cllist546 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_cllist569 = new BitSet(new long[]{0x0000042769900000L});
	public static final BitSet FOLLOW_LET_in_cllist585 = new BitSet(new long[]{0x0000080002000000L});
	public static final BitSet FOLLOW_VAR_in_cllist606 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_cllist610 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_cllist612 = new BitSet(new long[]{0x0000000004800080L});
	public static final BitSet FOLLOW_basic_in_cllist616 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_ASS_in_cllist618 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_cllist622 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_SEMIC_in_cllist624 = new BitSet(new long[]{0x0000080002000000L});
	public static final BitSet FOLLOW_IN_in_cllist661 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_cllist703 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_SEMIC_in_cllist727 = new BitSet(new long[]{0x0000000000202000L});
	public static final BitSet FOLLOW_CRPAR_in_cllist760 = new BitSet(new long[]{0x0000000000000102L});
	public static final BitSet FOLLOW_VAR_in_declist834 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_declist838 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_declist840 = new BitSet(new long[]{0x0000000044800080L});
	public static final BitSet FOLLOW_type_in_declist844 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_ASS_in_declist846 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_declist850 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_FUN_in_declist886 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_declist890 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_declist892 = new BitSet(new long[]{0x0000000044800080L});
	public static final BitSet FOLLOW_type_in_declist896 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_declist909 = new BitSet(new long[]{0x0000004000800000L});
	public static final BitSet FOLLOW_ID_in_declist945 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_declist947 = new BitSet(new long[]{0x0000000044800080L});
	public static final BitSet FOLLOW_type_in_declist951 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_declist1025 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_declist1029 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_declist1031 = new BitSet(new long[]{0x0000000044800080L});
	public static final BitSet FOLLOW_type_in_declist1035 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_declist1101 = new BitSet(new long[]{0x0000042769900000L});
	public static final BitSet FOLLOW_LET_in_declist1126 = new BitSet(new long[]{0x0000080000200000L});
	public static final BitSet FOLLOW_declist_in_declist1130 = new BitSet(new long[]{0x0000000002000000L});
	public static final BitSet FOLLOW_IN_in_declist1132 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_declist1139 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_SEMIC_in_declist1164 = new BitSet(new long[]{0x0000080000200002L});
	public static final BitSet FOLLOW_term_in_exp1236 = new BitSet(new long[]{0x0000001880000002L});
	public static final BitSet FOLLOW_PLUS_in_exp1261 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_term_in_exp1267 = new BitSet(new long[]{0x0000001880000002L});
	public static final BitSet FOLLOW_MINUS_in_exp1282 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_term_in_exp1288 = new BitSet(new long[]{0x0000001880000002L});
	public static final BitSet FOLLOW_OR_in_exp1303 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_term_in_exp1310 = new BitSet(new long[]{0x0000001880000002L});
	public static final BitSet FOLLOW_factor_in_term1354 = new BitSet(new long[]{0x0000020000004012L});
	public static final BitSet FOLLOW_TIMES_in_term1373 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_factor_in_term1379 = new BitSet(new long[]{0x0000020000004012L});
	public static final BitSet FOLLOW_DIV_in_term1391 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_factor_in_term1398 = new BitSet(new long[]{0x0000020000004012L});
	public static final BitSet FOLLOW_AND_in_term1410 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_factor_in_term1417 = new BitSet(new long[]{0x0000020000004012L});
	public static final BitSet FOLLOW_value_in_factor1451 = new BitSet(new long[]{0x0000000010420002L});
	public static final BitSet FOLLOW_EQ_in_factor1470 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_value_in_factor1476 = new BitSet(new long[]{0x0000000010420002L});
	public static final BitSet FOLLOW_GE_in_factor1488 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_value_in_factor1494 = new BitSet(new long[]{0x0000000010420002L});
	public static final BitSet FOLLOW_LE_in_factor1507 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_value_in_factor1513 = new BitSet(new long[]{0x0000000010420002L});
	public static final BitSet FOLLOW_INTEGER_in_value1563 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TRUE_in_value1574 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FALSE_in_value1585 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NULL_in_value1596 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAR_in_value1605 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value1609 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_RPAR_in_value1611 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IF_in_value1622 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value1626 = new BitSet(new long[]{0x0000010000000000L});
	public static final BitSet FOLLOW_THEN_in_value1628 = new BitSet(new long[]{0x0000000000000200L});
	public static final BitSet FOLLOW_CLPAR_in_value1630 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value1634 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_CRPAR_in_value1636 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_ELSE_in_value1638 = new BitSet(new long[]{0x0000000000000200L});
	public static final BitSet FOLLOW_CLPAR_in_value1640 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value1644 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_CRPAR_in_value1646 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_PRINT_in_value1657 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_value1659 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value1663 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_RPAR_in_value1665 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOT_in_value1678 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_value1680 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value1684 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_RPAR_in_value1688 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NEW_in_value1697 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_value1701 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_value1703 = new BitSet(new long[]{0x0000046749900000L});
	public static final BitSet FOLLOW_exp_in_value1726 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_value1731 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value1735 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_value1763 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_value1774 = new BitSet(new long[]{0x0000000040008002L});
	public static final BitSet FOLLOW_LPAR_in_value1837 = new BitSet(new long[]{0x0000046749900000L});
	public static final BitSet FOLLOW_exp_in_value1852 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_value1857 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value1861 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_value1877 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DOT_in_value1907 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_value1911 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_value1939 = new BitSet(new long[]{0x0000046749900000L});
	public static final BitSet FOLLOW_exp_in_value1946 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_value1951 = new BitSet(new long[]{0x0000042749900000L});
	public static final BitSet FOLLOW_exp_in_value1955 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_value1964 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_basic_in_type2004 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_arrow_in_type2022 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_INT_in_basic2061 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BOOL_in_basic2072 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_basic2086 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAR_in_arrow2154 = new BitSet(new long[]{0x0000004044800080L});
	public static final BitSet FOLLOW_type_in_arrow2159 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_arrow2167 = new BitSet(new long[]{0x0000000044800080L});
	public static final BitSet FOLLOW_type_in_arrow2171 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_arrow2183 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ARROW_in_arrow2185 = new BitSet(new long[]{0x0000000004800080L});
	public static final BitSet FOLLOW_basic_in_arrow2189 = new BitSet(new long[]{0x0000000000000002L});
}
