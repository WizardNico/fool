package ast;

import interfaces.Node;
import lib.FOOLlib;

public class MinusNode implements Node {

	private Node left;
	private Node right;

	public MinusNode(Node left, Node right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "Minus\n" + this.left.toPrint(indent + "  ") + this.right.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() {
		//per fare la sottrazione � sufficiente controllare che le le espressioni a dx e sx restituiscano un intero
		if (!(FOOLlib.isSubType(this.left.typeCheck(), new IntTypeNode()) && FOOLlib.isSubType(this.right.typeCheck(), new IntTypeNode()))) {
			System.out.println("Non integers in subtraction!");
			System.exit(0);
		}

		return new IntTypeNode();
	}

	@Override
	public String codeGeneration() {
		return this.left.codeGeneration() + this.right.codeGeneration() + "sub\n";
	}
}