package ast;

import java.util.ArrayList;

import ct.CTentry;
import interfaces.DecNode;
import interfaces.Node;
import lib.FOOLlib;

public class NewNode implements Node {

	// id della classe che sto istanziando
	private String classId;
	
	// class entry che la descrive
	private CTentry entry;
	
	// argomenti passati alla new
	private ArrayList<Node> argList;

	public NewNode(String classId, CTentry entry, ArrayList<Node> parList) {
		this.classId = classId;
		this.entry = entry;
		this.argList = parList;
	}

	private String printArgList(String s) {
		String argstr = "";
		if (this.argList != null) {
			for (Node dec : this.argList) {
				argstr += dec.toPrint(s + "  ");
			}
		}
		return argstr;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "NewNode " + this.classId + "\n" + this.entry.toPrint(indent + "  ") + printArgList(indent);
	}

	@Override
	public Node typeCheck() {
		// controllo che il numero degli argomenti passati sia quello richiesto
		if (this.entry.getAllFields().size() != this.argList.size()) {
			System.out.println("Wrong number of parameters in the creation of an object of class " + this.classId);
			System.exit(0);
		}

		// recupero i tipi dei campi tramite campo allFields della CTentry e controllo che matchino i tipi
		for (int i = 0; i < this.entry.getAllFields().size(); i++) {
			DecNode d = (DecNode) this.entry.getAllFields().get(i);
			if (!FOOLlib.isSubType(this.argList.get(i).typeCheck(), d.getSymType())) {
				System.out.println("Wrong type for " + i + "-th parameter in the creation of an object of class " + this.classId);
				System.exit(0);
			}
		}

		// Ritorno il nodo del tipo desiderato
		return new ClassTypeNode(classId);
	}

	private String generateParametersList() {
		// Richiamo la generazione di codice su tutti i parametri
		String parCode = "";

		for (int i = 0; i < this.argList.size(); i++)
			parCode += this.argList.get(i).codeGeneration();

		return parCode;
	}
	
	private String putReverseParametersOnHeap() {
		String parCode = "";

		// alloco un nuovo oggetto nello heap, come prima cosa allora
		// pusho i parametri mettendoli nello heap dall'ultimo al primo come da figura

		for (int i = 0; i < this.argList.size(); i++) {
			/* Parto salvando sullo heap il (argList.size()-1)-i = (n-1-i)-esimo campo */
			parCode += "lhp\n";//pusho l'hp sullo stack		
			parCode += "sw\n";//salvo l'indirizzo del metodo a dove punta hp
			
			// ora ho il parametro nell'heap, devo incrementare l'heap pointer, stessa procedura: Lo carico e lo sommo ad uno
			parCode += "lhp\n";
			parCode += "push 1\n";
			parCode += "add\n";
			
			// salvo il valore
			parCode += "shp\n";
		}

		return parCode;
	}
	
	private String putMethodsOnHeap() {
		String metCode = "";
		
		//Itero su tutti i metodi
		for (int i = 0; i < this.entry.getAllMethods().size(); i++) {
			//Prendo l'i-esimo metodo, partiamo dall'i-esimo
			MethodNode m = (MethodNode) this.entry.getAllMethods().get(i);
			
			//Prendiamo la relativa label
			String label = m.getLabel();
			
			//Adesso devo mettere l'indirizzo dell i-esimo metodo nello heap		
			metCode += "push " + label + " \n"; //pusho la label		
			metCode += "lhp\n"; //pusho l'hp sullo stack
			metCode += "sw\n";//salvo l'indirizzo del metodo a dove punta hp
			
			//Incremento hp nel solito modo
			metCode += "lhp\n";
			metCode += "push 1\n";
			metCode += "add\n";
			
			// salvo hp
			metCode += "shp\n";
		}

		return metCode;
	}

	@Override
	public String codeGeneration() {
		/*
		 * In questo caso l'oggetto non verr� genarato codice nello stack,
		 * l'oggetto viene allocato nello heap.
		 */

		// caso particolare: classe senza campi ne metodi
		if (this.entry.getAllFields().size() == 0 && this.entry.getAllMethods().size() == 0) {
			return "lhp\n"   //occupo uno spazietto incrementando hp
				 + "push 1\n"//Pusho 1 per poi fare add e spostarmici
				 + "add\n"   //sommo e mi ci posiziono sopra
				 + "shp\n";  //salvo il valore hp
		} else {//Caso pi� reale quando ho una classe con parametri e metodi
			return generateParametersList()//Generazione di codice per i parametri
			
			/*----ALLOCO UN NUOVO OGGETTO NELLO HEAP----*/ 
			
			//Aggiungo i parametri
			+ putReverseParametersOnHeap() //Come prima cosa allora pusho i parametri mettendoli nello heap dall'ultimo al primo come da figura
			
			//Aggiugno Object Pointer
			+ "lhp\n"//metto hp sullo heap (object pointer, subito dopo i parametri, in modo che punti al primo metodo, vedi layout)

			//Aggiungo i metodi (prendendoli dalla dispatch table)
			+ putMethodsOnHeap();
		}
	}

}
