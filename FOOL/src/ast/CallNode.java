package ast;

import java.util.ArrayList;

import interfaces.Node;
import lib.FOOLlib;
import st.STentry;

public class CallNode implements Node {

	// nome della funzione chiamata
	private String id;
	// entry contenente le info riguardanti la dichiarazione della funzione
	private STentry entry;
	// argomenti passati alla chiamata (espressioni)
	private ArrayList<Node> argList;
	private int nestingLevel;

	/* Nodo chiamata a funzione dentro il coropo del programma */
	public CallNode(String i, STentry e, ArrayList<Node> p, int nl) {
		this.id = i;
		this.entry = e;
		this.argList = p;
		this.nestingLevel = nl;
	}

	private String printParList(String s) {
		String parstr = "";
		for (Node dec : this.argList) {
			parstr += dec.toPrint(s + "  ");
		}
		return parstr;
	}

	public String toPrint(String indent) {
		return indent + "Call:" + this.id + " at nestlev " + this.nestingLevel + "\n"
				+ this.entry.toPrint(indent + "  ") + printParList(indent);
	}

	@Override
	public Node typeCheck() {

		/*
		 * Controllo che entry sia di tipo arrowtype altrimenti significa che
		 * qualcuno ha usato il nome della funzione da qualche altra parte in
		 * maniera scorretta
		 */
		if (!(this.entry.getType() instanceof ArrowTypeNode)) {
			System.out.println("Wrong usage of identifier: " + this.id + ", is a function identifier");
			System.exit(0);
		}

		// tipo di ritorno della funzione definito nella dichiarazione
		ArrowTypeNode type = (ArrowTypeNode) this.entry.getType();
		Node retType = type.getRet();

		// lista dei tipi dei parametri formali definiti nella dichiarazione
		ArrayList<Node> parList = type.getParList();

		if (parList.size() != this.argList.size()) {
			System.out.println("Wrong number of parameters in the invocation of " + this.id);
			System.exit(0);
		}

		for (int i = 0; i < parList.size(); i++) {
			/*
			 * il tipo degli argomenti deve essere <= del tipo dei parametri
			 * formali argList sono espressioni => devo valutarle e ottenere il
			 * tipo con il typecheck
			 */
			if (!FOOLlib.isSubType(this.argList.get(i).typeCheck(), parList.get(i))) {
				System.out.println("Wrong type for " + i + "-th parameter in the invocation of " + this.id);
				System.exit(0);

			}
		}

		// ritorno il tipo dichiarato dentro arrowTypeNode
		return retType;
	}
	
	/*
	 * devo vedere dove � dichiarato, quindi qu� mi calcolo la differenza di
	 * nesting level per risalire la catena, dovr� poi generare tanti lw per
	 * quanti passi di nesting level devo fare. Allora genero una serie di "lw"
	 * per tanti quanti sono i passi che devo fare.
	 */
	private String getAR() {
		String getAR = "";
		// carica sullo stack il valore a quell'indirizzo, quindi lo uso per salire la catena, un lw un passo
		for (int i = 0; i < this.nestingLevel - this.entry.getNestingLevel(); i++)
			getAR += "lw\n";
		
		return getAR;
	}

	/*
	 * Generazione di codice di tutte le dichiarazioni in lista, crea
	 * l'activation record AR e ritorna la stringa con il codice generato,
	 * lascia uno spaziettino per ogni variabile/funzione che contiene all'
	 * interno: se � una funzione il suo indirizzo altrimenti se � una variabile 
	 * invece il punto in cui � dichiarata
	 */
	private String generateReverseDeclList() {
		/*stessa cosa di prima, chiamo la generazione del codice
		 * per ogni elmeneto della lista dei parametri e lascio uno
		 * spazietto per ogni elemento della lista*/
		String parCode = "";
		for (int i = this.argList.size() - 1; i >= 0; i--)
			parCode += argList.get(i).codeGeneration();
		
		return parCode;
	}
	
	/*
	 * Mettiamo sullo stack il ControlLink, e lo troviamo in fp.
	 * 
	 * Per ogni nodo di par list chiamiamo la generazione del codice, in ordine
	 * inverso, dall'ultimo al primo.
	 */
	@Override
	public String codeGeneration() {
		/*
		 * Chiamata di una funzione: devo costruire la prima parte
		 * dell'Activation Record della funzione che sto invocando. Devo settare
		 * (in ordine): 
		 * - CL: indirizzo dell'AR chiamante (che � questo in cui sono) 
		 * - parametri: dall'ultimo al primo 
		 * - AL: indirizzo dell'AR dove � dichiarata la funzione, per poter recuperare il "contesto"
		 * 
		 * 
		 * In questo momento sono ancora nell'AR prima della chiamata quindi FP
		 * contiene il puntatore a questo AR che quindi metto sullo stack nella
		 * posizione di CL
		 * 
		 * Le dichiarazioni di funzione (dopo l'high order) occupano due offset
		 * nello stack: 
		 * - ad offset definito nella sym table � presente l'FP al loro AR 
		 * - ad (offset definito nella sym table)-1 � presente l'indirizzo della funzione
		 */
		
		// caso di chiamata di un metodo internamente alla classe
		if (this.entry.isMethod()) {
			
			// Tramite l'object pointer arrivo nell'heap e con l'offset recupero l'indirizzo del metodo
			
			return "lfp\n"  //Metto sullo stack il control link
					+ generateReverseDeclList() //Metto sullo stack i parametri in ordine inverso.
					+ "lfp\n" //$fp punta ad AL
					+ getAR()  //Prendo il valore all'indirizzo indicato e lo mette in cima allo stack, fatto tante volte (comando lw) quanto � la differenza di nesting-level, quindi setto AL risalendo la catena statica
					   
					//Fatto questo abbiamo settato l'access link, adesso dobbiamo saltare alla funzione, dobbiamo rimediare l'indirizzo della funzione, preso dalla sua dichiarazione
					//Rimedio quindi l'indirizzo a cui saltare e lo metto sullo stack:
					   
					+ "push " + this.entry.getOffset() + "\n" //metto indirizzo di offset sullo stack
					+ "lfp\n"
					+ getAR() //Salto all'AR selezionato, se � locale allora � zero e sono gi� qua, altrimenti se � dichiarata fuori la stringa non sar�  vuota e risalgo la catena statica
					+ "add\n" // sommando mi posiziono sull'FP della funzione	
					+ "lw\n" //carica sullo stack il valore che c'� a quell'indirizzo ottenuto	
					+ "js\n"; //salto all'indirizzo che c'� sulla cima dello stack (e se lo mangia)	
			
		} else {
			/*
			 * Risalgo la catena statica degli access link e ad offset di sym
			 * table arrivo direttamente sull'FP mentre a quello di sym table -1
			 * trovo l'indirizzo della funzione
			 */
			return "lfp\n"  // Metto sullo stack il control link, copiando la posizione in cui mi trovo attualmente
					+ generateReverseDeclList() //Metto sullo stack i parametri in ordine inverso.
					
					//Setto Access Link
					+ "push " + (this.entry.getOffset()) + "\n" // pusho l'offset della dichiarazione della f nel suo AR
					+ "lfp\n" // pusho FP (che punta all'AL)
					+ getAR() // mi permette di risalire la catena statica e raggiungere AR di dichiarazione della funzione
					+ "add\n" // sommando mi posiziono sull'FP della funzione					
					+ "lw\n" // ne vado a prendere il valore e lo metto sullo stack, in questo modo setto l'AL per il chiamato cio� vado a dirgli che si riferisce a questo AR 	
					
					/*----Fino a qu� ho settato la prima parte dell AR fino all' AL----*/
													
					// ora vado a recuperare l'indirizzo della funzione a cui saltare nello stesso modo (spostandomi di -1)
					+ "push " + (this.entry.getOffset() - 1) + "\n" 
					+ "lfp\n" 
					+ getAR() 
					+ "add\n" 
					+ "lw\n"
					+ "js\n"; //salto all'indirizzo che c'� sulla cima dello stack (e se lo mangia)
		}					   
	}
}