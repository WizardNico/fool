package ast;

import interfaces.DecNode;
import interfaces.Node;

public class ParNode implements Node, DecNode {

	private String id;
	private Node type;

	/*
	 * Nodo relativo ai parametri di funzione, ogni nodo par � un parametro
	 * dentro parentesi.
	 */
	public ParNode(String id, Node type) {
		this.id = id;
		this.type = type;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "Par:" + this.id + "\n" + this.type.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() {
		return null;
	}

	@Override
	public String codeGeneration() {
		return null;
	}

	@Override
	public Node getSymType() {
		return this.type;
	}
}