package ast;

import interfaces.Node;

public class IntTypeNode implements Node {

	public IntTypeNode() {
	}

	@Override
	public String toPrint(String indent) {
		return indent + "IntType\n";
	}

	@Override
	public Node typeCheck() {
		/* Non verr� mai chiamato, sono sulla foglia */
		return null;
	}

	@Override
	public String codeGeneration() {
		// mai usato
		return null;
	}
}