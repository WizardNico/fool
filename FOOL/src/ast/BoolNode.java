package ast;

import interfaces.Node;

public class BoolNode implements Node {

	private Boolean val;

	public BoolNode(Boolean val) {
		this.val = val;
	}

	@Override
	public String toPrint(String indent) {
		if (this.val)
			return indent + "Bool:true\n";
		else
			return indent + "Bool:false\n";
	}

	@Override
	public Node typeCheck() {
		/* Torna se stesso un booltype node */
		return new BoolTypeNode();
	}

	@Override
	public String codeGeneration() {
		// Ritorno una push della variabile booleana sullo stack
		// Se � true 1, altrimenti 0.
		return "push " + (this.val ? 1 : 0) + "\n";
	}
}