package ast;

import interfaces.Node;
import lib.FOOLlib;

public class LowerEqualNode implements Node {

	private Node left;
	private Node right;

	public LowerEqualNode(Node left, Node right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "LowerOrEqual\n" + this.left.toPrint(indent + "  ") + this.right.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() {
		Node l = left.typeCheck();
		Node r = right.typeCheck();

		if (l instanceof ArrowTypeNode || r instanceof ArrowTypeNode) {
			System.out.println("Incompatible types in '<=' operator, functions not allowed");
			System.exit(0);
		}

		if (FOOLlib.lowestCommonAncestor(l, r) == null) {
			System.out.println("Incompatible types in '<=' operator");
			System.exit(0);
		}

		return new BoolTypeNode();
	}

	@Override
	public String codeGeneration() {
		String labelA = FOOLlib.freshVariableLabel();
		String labelB = FOOLlib.freshVariableLabel();
		
		return 	this.left.codeGeneration()+//metto sullo stack il risultato della espressione a sx
				this.right.codeGeneration()+//metto sullo stack il risultato della espressione a dx				
				"bleq " + labelA + "\n" +// se left < di right salto a labelA
				
				this.left.codeGeneration()+//metto sullo stack il risultato della espressione a sx
				this.right.codeGeneration()+//metto sullo stack il risultato della espressione a dx	
				"beq " + labelA + "\n" +// se left = right salto a labelA

				
				"push 0\n" +//se nessuna delle due => metto false			
				"b " + labelB + "\n" +// salto via cio� non faccio il push true
				
				labelA + ":  \n" +
				"push 1 \n" +// se sono <=
				labelB + ":\n";
	}
}
