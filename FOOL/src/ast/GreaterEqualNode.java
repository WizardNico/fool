package ast;

import interfaces.Node;
import lib.FOOLlib;

public class GreaterEqualNode implements Node {

	private Node left;
	private Node right;

	public GreaterEqualNode(Node left, Node right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "GreaterOrEqual\n" + this.left.toPrint(indent + "  ") + this.right.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() {
		Node l = this.left.typeCheck();
		Node r = this.right.typeCheck();

		if (l instanceof ArrowTypeNode || r instanceof ArrowTypeNode) {
			System.out.println("Type error: function in '>=' operator");
			System.exit(0);
		}

		if (FOOLlib.lowestCommonAncestor(l, r) == null) {
			System.out.println("Incompatible types in '>=' operator");
			System.exit(0);
		}

		return new BoolTypeNode();
	}

	@Override
	public String codeGeneration() {
		String labelA = FOOLlib.freshVariableLabel();
		String labelB = FOOLlib.freshVariableLabel();
		
		return 	this.right.codeGeneration()+//metto sullo stack il risultato delle due espressioni al contrario partendo da dx
				this.left.codeGeneration()+//metto sullo stack il risultato delle due espressioni al contrario partendo da sx
				"bleq " + labelA + "\n" +// se right < di left => left > right =>  salto a labelA
				
				this.left.codeGeneration() +//Valuto sx
				this.right.codeGeneration() +//Valuto dx
				"beq " + labelA + "\n" +//se sono uguali salto a labelA
				
				
				"push 0\n" +//se nessuna delle due => metto false				
				"b " + labelB + "\n" +// salto via cio� non faccio il push true
				
				labelA+":  \n" +
				"push 1 \n" +// se sono <= di conseguenza >=
				labelB + ":\n";
	}
}