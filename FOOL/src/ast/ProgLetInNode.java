package ast;

import java.util.ArrayList;

import interfaces.Node;
import lib.FOOLlib;

public class ProgLetInNode implements Node {

	// Tutte le dichiarazioni fatte in principio
	private ArrayList<Node> declarations;
	
	// Espresione aggangiata
	private Node exp;
	
	//Lista di classi
	private ArrayList<Node> classes;

	/*
	 * Programma pu� essere o un prognode o un progletinnode, questo � il caso
	 * in cui nel let iniziale ci sono delle dichiarazioni.
	 */
	public ProgLetInNode(ArrayList<Node> classes, ArrayList<Node> d , Node e) {
		this.declarations = d;
		this.exp = e;
		this.classes = classes;
	}

	private String printClassList(String s) {
		String clstr = "";
		if (this.classes != null) {
			for (Node cl : this.classes) {
				clstr += cl.toPrint(s + "  ");
			}
		}
		return clstr;
	}
	
	private String printDeclList(String s) {
		String declstr = "";
		if (this.declarations != null) {
			for (Node dec : this.declarations) {
				declstr += dec.toPrint(s + "  ");
			}
		}
		return declstr;
	}

	// Check di tutte le dichiarazioni agganciate a questo nodo
	private void checkDeclarations() {
		if (this.declarations != null) {
			for (Node dec : this.declarations)
				dec.typeCheck();
		}
	}
	
	// Check di tutte le classi agganciate a questo nodo
	private void checkClasses() {
		if (this.classes != null) {
			for (Node cl : this.classes)
				cl.typeCheck();
		}
	}

	/*
	 * Generazione di codice di tutte le dichiarazioni in lista, crea
	 * l'activation record AR e ritorna la stringa con il codice generato,
	 * lascia uno spaziettino per ogni variabile/funzione che contiene all'
	 * interno: se � una funzione il suo indirizzo altrimenti se � una variabile 
	 * invece il punto in cui � dichiarata
	 */
	private String generateDeclList() {
		String declCode = "";
		if (this.declarations != null) {
			for (Node dec : this.declarations) {
				declCode += dec.codeGeneration();
			}
		}
		return declCode;
	}
	
	/*
	 * Generazione di codice di tutte le classi dichiarate
	 */
	private String generateClassList() {
		String clCode = "";
		if (this.classes != null) {
			for (Node cl : this.classes) {
				clCode += cl.codeGeneration();
			}
		}
		return clCode;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "ProgLetIn\n" + printClassList(indent) + printDeclList(indent) + this.exp.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() {
		// Typecheck di tutte le dichiarazioni (var e funzioni)
		checkDeclarations();
		
		// Typecheck di tutte le classi
		checkClasses();
		
		// Ritorna il typechek dell'espressione aggangiata dopo
		return this.exp.typeCheck();
	}

	@Override
	public String codeGeneration() {
		/*
		 * questo non cambia molto da prognode, per� abbiamo anche delle
		 * dichiarazioni da riportare sullo stack, prima di cominciare a
		 * generare il codice del corpo, del main/in
		 */
		return "push 0\n" + 		 	    
		 	    generateClassList()+
		 	    generateDeclList() + 
		 	    "\n" +
		 	    this.exp.codeGeneration() + 
		 	    "halt\n" + 
		 	    FOOLlib.getCode();//Dopo aver generato il codice del corpo, poniamo le etichette seguite dal corpo delle funzioni dichiarate in principio
	}
}