package ast;

import interfaces.Node;
import lib.FOOLlib;

public class DivNode implements Node {

	private Node left;
	private Node right;

	public DivNode(Node left, Node right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "Div\n" + this.left.toPrint(indent + "  ") + this.right.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() {

		if (!(FOOLlib.isSubType(this.left.typeCheck(), new IntTypeNode()) && FOOLlib.isSubType(this.right.typeCheck(), new IntTypeNode()))) {
			System.out.println("Non integers in division!");
			System.exit(0);
		}

		return new IntTypeNode();
	}

	@Override
	public String codeGeneration() {
		return this.left.codeGeneration() + this.right.codeGeneration() + "div\n";
	}

}
