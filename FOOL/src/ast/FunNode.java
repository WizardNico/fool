package ast;

import java.util.ArrayList;

import interfaces.DecNode;
import interfaces.Node;
import lib.FOOLlib;

public class FunNode implements Node, DecNode {

	private String id;
	private Node returnType;
	private ArrayList<Node> declarations;
	private ArrayList<Node> parameters;
	private Node exp;
	
	// tipo messo in Symbol Table
	private Node symType;

	/*
	 * Nel nodo funzione avremo attaccato ad esso:
	 * 
	 * -Un nodo per ogni paramentro dichiarato;
	 * 
	 * -Un nodo per ogni variabile dichiarata nel let, quindi globalmente alla
	 * funzione(al suo interno);
	 * 
	 * -Un nodo che riguarda le operazioni, quindi l'espressione, che la
	 * funzione andr� a fare al suo interno;
	 */
	public FunNode(String id, Node type) {
		this.id = id;
		this.returnType = type;
		this.parameters = new ArrayList<Node>();
	}

	/* Aggiungo il corpo della funzione: Espressione */
	public void addBody(Node exp) {
		this.exp = exp;
	}

	/* Aggiungo i parametri */
	public void addParameter(Node param) {
		this.parameters.add(param);
	}
	
	public void addDeclarations(ArrayList<Node> declarations){
		this.declarations = declarations;
	}
	
	/*setto il type della funzione dalla symbletable*/
	public void addSymType(Node functionType){
		this.symType = functionType;
	}

	/* retiturisce la stringa con la lista delle dichiarazioni */
	private String printDeclList(String s) {
		String declstr = "";
		if (this.declarations != null) {
			for (Node dec : this.declarations) {
				declstr += dec.toPrint(s + "  ");
			}
		}
		return declstr;
	}

	/* retiturisce la stringa con la lista dei parametri */
	private String printParamList(String s) {
		String paramstr = "";
		if (this.parameters != null) {
			for (Node parm : this.parameters) {
				paramstr += parm.toPrint(s + "  ");
			}
		}
		return paramstr;
	}

	/* Typechecking di tutte le dichiarazioni */
	private void checkDeclarations() {
		if (this.declarations != null) {
			for (Node dec : this.declarations)
				dec.typeCheck();
		}
	}
	
	/*
	 * Generazione di codice di tutte le dichiarazioni in lista, crea
	 * l'activation record AR e ritorna la stringa con il codice generato,
	 * lascia uno spaziettino per ogni variabile/funzione che contiene all'
	 * interno: se � una funzione il suo indirizzo altrimenti se � una variabile
	 * il punto in cui � dichiarata
	 */
	private String generateDeclList() {
		String declCode = "";
		if (this.declarations != null) {
			for (Node dec : this.declarations) {
				declCode += dec.codeGeneration();
			}
		}
		return declCode;
	}

	private String popDeclarationsList() {
		String popDecl = "";
		if (this.declarations != null) {
			for (int i = 0; i < this.declarations.size(); i++) {
				popDecl += "pop\n";// Una pop per ogni dichiarazione

				// se la dichiarazione ha tipo funzionale devo eliminare sia
				// l'indirizzo che il suo FP, con due pop, qu� aggiungo la seconda
				if (((DecNode) this.declarations.get(i)).getSymType() instanceof ArrowTypeNode)
					popDecl += "pop\n";
			}
		}
		return popDecl;
	}

	private String popParametersList() {
		String popPars = "";
		if (this.parameters != null) {
			for (int i = 0; i < this.parameters.size(); i++) {
				popPars += "pop\n";// Una pop per ogni parametro

				// se il parametro ha tipo funzionale devo eliminare sia
				// l'indirizzo che il suo FP, con due pop, qu� aggiungo la seconda
				if (((DecNode) this.parameters.get(i)).getSymType() instanceof ArrowTypeNode)
					popPars += "pop\n";
			}
		}
		return popPars;
	}
	
	@Override
	public Node getSymType() {
		return this.symType;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "Fun:" + this.id + "\n" + this.returnType.toPrint(indent + "  ") + printParamList(indent)
				+ printDeclList(indent) + this.exp.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() {
		/*
		 * Faccio prima un check su ogni dichiarazione, le quali fanno un
		 * autocheck al loro interno.
		 */
		checkDeclarations();

		/*
		 * Controllo che l'espressione ritorni un tipo congruente a quello
		 * dichiarato dalla funzione, ovvero: se la funzione � di tipo bool
		 * allora questa operazione deve ridarmi per forza un bool.
		 */
		if (!FOOLlib.isSubType(this.exp.typeCheck(), this.returnType)) {
			System.out.println("Wrong return type for function " + this.id);
			System.exit(0);
		}
		return this.returnType;
	}

	@Override
	public String codeGeneration() {
		/*
		 * Creo un etichetta/indirizzo a cui far tornare il codice una volta
		 * svolta la funzione
		 */
		String address = FOOLlib.freshFunctionLabel();

		/*
		 * Creiamo una specie di repository con tutti i codici delle funzioni.
		 * successivamente nel progletinnode, dopo l'alt ci mettiamo tutta sta
		 * stringona con i codici delle funzioni
		 */
		FOOLlib.putCode(
						address + ":\n" 
						+ "cfp\n" //setta $fp a $sp
						+ "lra\n" //inserimento return-address
						+ generateDeclList() //Inserimento dichiarazioni locali
						+ this.exp.codeGeneration() //chiamo la generazione sul corpo della funzione
						+ "srv\n" //salvo sullo stack il return value della funzione
						
						//A sto punto dobbiamo iniziare a distruggere lo stack e tutte le robe che abbiamo creato, quindi tante pop, per ogni dichiarazione che abbiamo
						
						+ popDeclarationsList() //Poppo tutte le dichiarazioni
						+ "sra\n" //Pop del return address e lo salvo
						+ "pop\n" //Elimina l'access link
						+ popParametersList() //pop dei parametri
						+ "sfp\n" //Setto il $fp al valore del control link
						+ "lrv\n" //Carico il risultato della funzione sullo stack
						+ "lra\n" //Carico l'indirizzo per il salto (return address), dallo stack, quindi carica $ra
						+ "js\n" //Salto all'indirizzo sopra definito, salta a $ra
					   );
		
		//Push dell'indirizzo della funzione, e a quell'indirizzo ci metto il codice sopra definito
		return "lfp\n" // pusho l'FP a questo AR, lo carico a questo Activation Record, per poter successivamente recuperare il contesto (QUI PRENDO L'INDIRIZZO DELLA FUNZIONE).
			 + "push " + address + "\n"; //Metto il codice della funzione, che ho creato qua sopra, all'indirizzo sopra caricato.
	}

}