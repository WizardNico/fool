package ast;

import java.util.ArrayList;

import interfaces.*;
import lib.FOOLlib;

public class MethodNode implements Node, DecNode {

	// nome del metodo
	private String id;
	
	// tipo di ritorno
	private Node retType;
	
	// parametri
	private ArrayList<Node> parameters;
	
	// dichiarazioni
	private ArrayList<Node> declarations;
	
	// corpo del metodo
	private Node body;
	
	// tipo messo in Virtual Table
	private Node symType;
	
	// label utilizzata durante la generazione di codice
	private String label;

	public MethodNode(String id, Node type) {
		this.id = id;
		this.retType = type;
		this.parameters = new ArrayList<Node>();
		this.declarations = new ArrayList<Node>();
	}

	public void addParameter(Node parameter) {
		parameters.add(parameter);
	}

	public void addDec(ArrayList<Node> declarations) {
		this.declarations = declarations;
	}

	public void addBody(Node body) {
		this.body = body;
	}

	public void addSymType(Node functionType) {
		this.symType = functionType;
	}

	/* retiturisce la stringa con la lista delle dichiarazioni */
	private String printDeclList(String s) {
		String declstr = "";
		if (this.declarations != null) {
			for (Node dec : this.declarations) {
				declstr += dec.toPrint(s + "  ");
			}
		}
		return declstr;
	}

	/* retiturisce la stringa con la lista dei parametri */
	private String printParamList(String s) {
		String paramstr = "";
		if (this.parameters != null) {
			for (Node parm : this.parameters) {
				paramstr += parm.toPrint(s + "  ");
			}
		}
		return paramstr;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "Method: " + this.id + "\n" + this.retType.toPrint(indent + "  ") + printParamList(indent)
				+ printDeclList(indent) + this.body.toPrint(indent + "  ");
	}

	@Override
	public Node getSymType() {
		return this.symType;
	}

	// setto la label (definita dalla classe)
	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public Node typeCheck() {
		// verifico che il corpo della funzione ritorni il tipo dichiarato dal metodo
		if (!FOOLlib.isSubType(this.body.typeCheck(), this.retType)) {
			System.out.println("Wrong return type for method " + this.id);
			System.exit(0);

		}
		
		// controllo le dichiarazioni
		for (Node node : this.declarations) 
			node.typeCheck();
		
		return this.retType;
	}

	/*
	 * Generazione di codice di tutte le dichiarazioni in lista, crea
	 * l'activation record AR e ritorna la stringa con il codice generato,
	 * lascia uno spaziettino per ogni variabile/funzione che contiene all'
	 * interno: se � una funzione il suo indirizzo altrimenti se � una variabile
	 * invece il punto in cui � dichiarata
	 */
	private String generateDeclList() {
		String declCode = "";
		if (this.declarations != null) {
			for (Node dec : this.declarations) {
				declCode += dec.codeGeneration();
			}
		}
		return declCode;
	}

	private String popDeclarationsList() {
		String popDecl = "";
		if (this.declarations != null) {
			for (int i = 0; i < this.declarations.size(); i++) {
				popDecl += "pop\n";// Una pop per ogni dichiarazione

				// se la dichiarazione ha tipo funzionale devo eliminare sia
				// l'indirizzo che il suo FP, con due pop, qu� aggiungo la seconda
				if (((DecNode) this.declarations.get(i)).getSymType() instanceof ArrowTypeNode)
					popDecl += "pop\n";
			}
		}
		return popDecl;
	}

	private String popParametersList() {
		String popPars = "";
		if (this.parameters != null) {
			for (int i = 0; i < this.parameters.size(); i++) {
				popPars += "pop\n";// Una pop per ogni parametro

				// se il parametro ha tipo funzionale devo eliminare sia
				// l'indirizzo che il suo FP, con due pop, qu� aggiungo la seconda
				if (((DecNode) this.parameters.get(i)).getSymType() instanceof ArrowTypeNode)
					popPars += "pop\n";
			}
		}
		return popPars;
	}
	
	@Override
	public String codeGeneration() {
		//Prendiamo l'indirizzo relativo al metodo
		String address = this.label;

		/*
		 * Creiamo una specie di repository con tutti i codici delle funzioni.
		 * successivamente nel progletinnode, dopo l'alt ci mettiamo tutta sta
		 * stringona con i codici delle funzioni
		 */
		FOOLlib.putCode(
						address + ":\n" 
						+ "cfp\n" //setta $fp a $sp
						+ "lra\n" //inserimento return-address
						+ generateDeclList() //Inserimento dichiarazioni locali
						+ this.body.codeGeneration() //chiamo la generazione sul corpo della funzione
						+ "srv\n" //Estrae dallo stack il return value/fa il pop del retrun value
						
						//A sto punto dobbiamo iniziare a distruggere lo stack e tutte le robe che abbiamo creato, quindi tante pop, per ogni dichiarazione che abbiamo
						
						+ popDeclarationsList() //Poppo tutte le dichiarazioni
						+ "sra\n" //Pop del return address e lo mette nel registro ra
						+ "pop\n" //Elimina l'access link
						+ popParametersList() //pop dei parametri
						+ "sfp\n" //Setto il $fp al valore del control link
						+ "lrv\n" //Carico il risultato della funzione sullo stack
						+ "lra\n" //Carico l'indirizzo per il salto (return address), dallo stack, quindi carica $ra
						+ "js\n" //Salto all'indirizzo sopra definito, salta a $ra
					   );
		
		// non viene allocato niente sullo stack nel new node verr� creata la dispatch table
		return  "";	
	}

	public String getLabel() {
		return this.label;
	}
}
