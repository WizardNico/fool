package ast;

import interfaces.Node;

public class BoolTypeNode implements Node {

	public BoolTypeNode() {
	}

	@Override
	public String toPrint(String indent) {
		return indent + "BoolType\n";
	}

	@Override
	public Node typeCheck() {
		/* Non verr� mai chiamato, sono sulla foglia */
		return null;
	}

	@Override
	public String codeGeneration() {
		/*anche quà la generazione non verrà mai chiamato*/
		return null;
	}
}