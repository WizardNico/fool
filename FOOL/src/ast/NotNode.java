package ast;

import interfaces.Node;
import lib.FOOLlib;

public class NotNode implements Node {

	private Node exp;

	public NotNode(Node exp) {
		this.exp = exp;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "Not: \n" + this.exp.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() {
		if (!FOOLlib.isSubType(exp.typeCheck(), new BoolTypeNode())) {
			System.out.println("Non boolean term in not!");
			System.exit(0);
		}
		return new BoolTypeNode();
	}

	@Override
	public String codeGeneration() {
		String labelA = FOOLlib.freshVariableLabel();
		String labelB = FOOLlib.freshVariableLabel();

		return this.exp.codeGeneration() +//Pusho sullo stack il codice della exp
				"push 1\n" + //pusho 1
				"beq " + labelA + "\n" + //Se � true allora salta a labelA

				"push 1\n" +//se � false quindi pusho true		
				"b " + labelB + "\n" +// salto via cio� non faccio il push true
				
				labelA + ":  \n" +				
				"push 0 \n" +// se � true pusho false	
				labelB + ":\n";
	}
}
