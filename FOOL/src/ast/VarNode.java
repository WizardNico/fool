package ast;

import interfaces.DecNode;
import interfaces.Node;
import lib.FOOLlib;

public class VarNode implements Node, DecNode {

	private String id;
	private Node type;
	private Node exp;

	public VarNode(String id, Node type, Node exp) {
		this.id = id;
		this.type = type;
		this.exp = exp;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "Var:" + this.id + "\n" + this.type.toPrint(indent + "  ") + this.exp.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() {
		/*
		 * Qu� siamo nel caso di una dichiarazione fatta dentro al
		 * progletinnode. Dovr� controllare che l'espessione dopo l'uguale
		 * torni un tipo come quello dichiarato, ovvero se dico
		 * "var x:int = 3+6-5;" allora l'espressione a DX dell'uguale deve
		 * ridarmi un intero per forza
		 */
		if (!FOOLlib.isSubType(this.exp.typeCheck(), this.type)) {
			System.out.println("Incompatible value for variable " + this.id);
			System.exit(0);
		}
		return null;
	}

	@Override
	public String codeGeneration() {
		/*
		 * pushamo sullo stack solo il risultato dell'espressione di
		 * inizializzazione se faccio var x:int = 5+2/3, butto il risultato
		 * direttamente, non tutta quella roba
		 */
		return this.exp.codeGeneration();
	}

	@Override
	public Node getSymType() {
		return this.type;
	}
}