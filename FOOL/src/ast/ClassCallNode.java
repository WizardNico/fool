package ast;

import java.util.ArrayList;

import interfaces.Node;
import lib.FOOLlib;
import st.STentry;

public class ClassCallNode implements Node {

	// entry dell'oggetto
	private STentry entry;
	
	// nome dell'oggetto
	private String objId;
	
	// entry del metodo chiamato
	private STentry methodEntry;
	
	// nome del metodo
	private String methodId;
	
	// parametri passati al metodo
	private ArrayList<Node> argList;
	
	// livello di dove � la chiamata del metodo
	private int nestingLevel;

	public ClassCallNode(String objId, STentry classEntry, String methodId, STentry methodEntry, ArrayList<Node> parList, int nestingLevel) {
		this.objId = objId;
		this.entry = classEntry;
		this.methodEntry = methodEntry;
		this.methodId = methodId;
		this.argList = parList;
		this.nestingLevel = nestingLevel;
	}

	/* retiturisce la stringa con la lista delle dichiarazioni */
	private String printArgList(String s) {
		String declstr = "";
		if (this.argList != null) {
			for (Node dec : this.argList) {
				declstr += dec.toPrint(s + "  ");
			}
		}
		return declstr;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "ClassCall " + this.objId + "." + this.methodId + "\n" + indent + "Object entry (" + this.objId
				+ ")\n" + this.entry.toPrint(indent + "  ") + indent + "Method entry (" + this.methodId + ")\n"
				+ this.methodEntry.toPrint(indent + "  ") + printArgList(indent);
	}

	@Override
	public Node typeCheck() {
		//Controllo che non sia stata usata la chiamata su ci� che non � un oggetto
		if (!(this.entry.getType() instanceof ClassTypeNode)) {
			System.out.println("Object usage of a non object identifier " + this.objId);
			System.exit(0);
		}

		//Tipo di ritorno del metodo definito nella dichiarazione
		ArrowTypeNode type = (ArrowTypeNode) this.methodEntry.getType();
		Node retType = type.getRet();

		//Lista dei tipi dei parametri formali definiti nella dichiarazione (lista dei parametri previsti dalla classe)
		ArrayList<Node> parList = type.getParList();

		//Controllo che ci sia lo stesso numero di parametri previsti dalla classe
		if (parList.size() != argList.size()) {
			System.out.println("Wrong number of parameters in the invocation of " + this.objId + "." + this.methodId);
			System.exit(0);
		}

		
		for (int i = 0; i < parList.size(); i++) {
			// il tipo degli argomenti deve essere <= del tipo dei parametri formali(quelli previsti) argList sono espressioni quindi devo valutarle e ottenere il tipo con il typecheck
			if (!FOOLlib.isSubType(argList.get(i).typeCheck(), parList.get(i))) {
				System.out.println("Wrong type for " + i + "-th parameter in the invocation of " + this.objId + "." + this.methodId);
				System.exit(0);
			}
		}

		// ritorno il tipo dichiarato dentro arrowTypeNode
		return retType;
	}
	
	private String generateReverseDeclList() {
		String parCode = "";
		for (int i = this.argList.size() - 1; i >= 0; i--)
			parCode += argList.get(i).codeGeneration();
		
		return parCode;
	}

	private String getAR() {
		String getAR = "";
		// carica sullo stack il valore a quell'indirizzo, quindi lo uso per salire la catena, un lw un passo
		for (int i = 0; i < this.nestingLevel - this.entry.getNestingLevel(); i++)
			getAR += "lw\n";
		
		return getAR;
	}
	
	@Override
	public String codeGeneration() {
		return "lfp\n" //Setto il Control link (CL) settando il fp ad esso, praticamente dove punta attualmente sp
				+ generateReverseDeclList() //Metto sullo stack i parametri in ordine inverso
				
				/*------------Ora devo settare AL recuperando prima Obj-pointer---------------*/
				+ "push " + (this.entry.getOffset()) + "\n"//pusho l'offset della dichiarazione dell'oggetto nel suo AR
				+ "lfp\n" // pusho FP (che punta all'AL)
				+ getAR() // mi permette di risalire la catena statica
				+ "add\n" // sommando mi posiziono sull'OB della classe				
				+ "lw\n" // vado a prendere il valore e lo metto sullo stack, in questo modo setto l'AL per il chiamato cio� vado a dirgli che si riferisce a questo AR 	
				
				/*----Fino a qu� ho settato la prima parte dell AR fino all' AL----*/
													
				//ora vado a recuperare l'indirizzo del metodo nella dispatch table a cui saltare
				
				//Recupero prima l'object-pointer
				+ "push " + (this.entry.getOffset()) + "\n" //pusho l'offset della dichiarazione dell'oggetto nel suo AR
				+ "lfp\n" //Pusho fp
				+ getAR() // risalgo la catena statica e raggiungo l'oggetto
				+ "add\n" //Sommando mi ci posiziono sopra con l'object pointer
				+ "lw\n" //Prendo il valore e lo metto sullo stack
	
				//Poi pusho l'offset del metodo
				+ "push " +(this.methodEntry.getOffset())+"\n"
				+ "add\n" //Mi ci posiziono
				+ "lw\n"
				+ "js\n"; //salto all'indirizzo che c'� sulla cima dello stack (e se lo mangia)
	}
}
