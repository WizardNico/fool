package ast;

import interfaces.Node;
import lib.FOOLlib;

public class OrNode implements Node {

	private Node left;
	private Node right;

	public OrNode(Node left, Node right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "Or\n" + this.left.toPrint(indent + "  ") + this.right.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() {
		// ci va bene un qualsiasi sottotipo di bool
		if (!(FOOLlib.isSubType(left.typeCheck(), new BoolTypeNode()) && FOOLlib.isSubType(right.typeCheck(), new BoolTypeNode()))) {
			System.out.println("Non boolean in or!");
			System.exit(0);
		}
		return new BoolTypeNode();
	}

	@Override
	public String codeGeneration() {

		/*
		 * Quando ho OR tra due elementi:
		 * 
		 * e1 || e2 allora:
		 * 
		 * salto se almeno una delle due espressioni valuta 1:
		 * 
		 * controllo e1, pusho 1 sullo stack e salto se sono uguali se non sono
		 * uguali controllo e2, pusho 1 sullo stack e salto se sono uguali se
		 * non sono uguali salto via 
		 */

		String labelA = FOOLlib.freshVariableLabel();
		String labelB = FOOLlib.freshVariableLabel();

		return //valuto l'espressione sinistra e salto a labelA se valuta true
				left.codeGeneration()+
				"push 1\n"+
				"beq "+labelA+ "\n" +
				
				//valuto l'espessione destra e salto a labelA se valuta true
				right.codeGeneration()+
				"push 1\n"+
				"beq "+labelA+ "\n" +
				
				//se arrivo qui non ho saltato => nessuno dei due era true => metto 0
				"push 0\n"+				
				"b "+labelB+"\n"+//salto a labelB che mi evita il push 1
				
				labelA+":  \n"+
				"push 1 \n"+
				labelB+":\n";
	}

}
