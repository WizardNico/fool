package ast;

import interfaces.Node;

public class PrintNode implements Node {

	private Node exp;

	public PrintNode(Node print) {
		this.exp = print;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "Print\n" + this.exp.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() {
		/* Fa typecheck del nodo agganciato al printnode */
		return this.exp.typeCheck();
	}

	@Override
	public String codeGeneration() {
		//stampa la cima dello stack
		return this.exp.codeGeneration() + "print\n";
	}
}
