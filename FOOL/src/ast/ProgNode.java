package ast;

import interfaces.Node;

public class ProgNode implements Node {

	private Node exp;

	/*
	 * Programma pu� essere o un prognode o un progletinnode, questo � il caso
	 * in cui nel let iniziale non ci sia alcuna dichiarazione, caso base
	 */
	public ProgNode(Node e) {
		this.exp = e;
	}

	@Override
	public String toPrint(String indent) {
		return "Prog\n" + this.exp.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() {
		return this.exp.typeCheck();
	}

	@Override
	public String codeGeneration() {
		/*
		 * crea il codice della espressione che contiene nell'in, dobbiamo per�
		 * mettere halt alla fine per farlo fermare, senno questo va
		 * all'infinito generando cose vuote
		 */
		return this.exp.codeGeneration() + "halt\n";
	}
}
