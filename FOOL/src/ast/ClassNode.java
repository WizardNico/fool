package ast;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import ct.CTentry;
import interfaces.*;
import lib.FOOLlib;

public class ClassNode implements Node, DecNode {

	//Tipo
	private ClassTypeNode type;
	
	// definizione dei metodi definiti dalla classe (non da eventuali super)
	private ArrayList<Node> methods;
	
	// definizione dei campi definiti dalla classe (non da eventuali super)
	private ArrayList<Node> fields;
	
	//Entry della classe che vado a creare
	private CTentry classEntry;
	
	//Da usare nel caso eredito, questa non varr� null
	private CTentry superEntry;

	public ClassNode(ClassTypeNode type, ArrayList<Node> fields, ArrayList<Node> methods, CTentry classEntry, CTentry superEntry) {
		this.fields = fields;
		this.methods = methods;
		this.type = type;
		this.classEntry = classEntry;
		this.superEntry = superEntry;
	}

	/* restiturisce la stringa con la lista dei campi */
	private String printFieldsList(String s) {
		String fieldsStr = "";
		if (this.fields != null) {
			for (Node parm : this.fields) {
				fieldsStr += parm.toPrint(s + "  ");
			}
		}
		return fieldsStr;
	}

	/* retiturisce la stringa con la lista dei metodi */
	private String printMethodsList(String s) {
		String methodsStr = "";
		if (this.methods != null) {
			for (Node parm : this.methods) {
				methodsStr += parm.toPrint(s + "  ");
			}
		}
		return methodsStr;
	}

	@Override
	public String toPrint(String indent) {
		//Caso in cui eredito
		if (superEntry != null) {
			return indent + "Class: " + type.getType() + "\n" + printFieldsList(indent) + printMethodsList(indent)
					+ indent + "Class entry:\n" + this.classEntry.toPrint(indent) + indent + "Super class entry:\n"
					+ this.superEntry.toPrint(indent);
		} else {
			return indent + "Class: " + type.getType() + "\n" + printFieldsList(indent) + printMethodsList(indent)
					+ indent + "Class entry:\n" + this.classEntry.toPrint(indent);
		}
	}

	@Override
	public Node typeCheck() {
		// Typechecking sui metodi della classe
		for (int i = 0; i < this.methods.size(); i++) 
			this.methods.get(i).typeCheck();

		// in caso di eredetariet� verifico che l'overriding sia corretto (per campi e metodi)
		if (this.superEntry != null) {
			// recupero il tipo dei campi dalla CTentry che descrive la classe
			ArrayList<Node> classFields = this.classEntry.getAllFields();	
			// recupero il tipo dei campi dalla CTentry che descrive la super-classe
			ArrayList<Node> superFields = this.superEntry.getAllFields();

			// recupero il tipo dei metodi dalla CTentry che descrive la classe
			ArrayList<Node> classMethods = this.classEntry.getAllMethods();
			// recupero il tipo dei metodi dalla CTentry che descrive la super-classe
			ArrayList<Node> superMethods = this.superEntry.getAllMethods();

			// recupero gli offset di campi e metodi definiti nella classe normale e quella superiore
			HashSet<Integer> locals = this.classEntry.getLocals();
			HashSet<Integer> superLocals = this.superEntry.getLocals();

			// itero sugli offset dei locals, mi limito a controllare che ci sia overriding dei metodi/campi giusti
			Iterator<Integer> it = locals.iterator();
			while (it.hasNext()) {
				int offset = it.next();
				if (!superLocals.contains(offset)) {
					// se la superclasse non contiene questo offset allora significa
					// che ho qualcosa di nuovo quindi vado avanti, niente overriding
					continue;
				}
				if (offset < 0) {
					//Se sono negativi si tratta di CAMPI
					int i = -(offset) - 1;
					// estraggo i tipi (altrimenti ho FieldNode) e controllo che siano dello stesso tipo, overriding che va a modificare anche il tipo non verr� ammesso!
					DecNode classField = (DecNode) classFields.get(i);
					DecNode superField = (DecNode) superFields.get(i);
					if (FOOLlib.isSubType(classField.getSymType(), superField.getSymType()) == false) {
						System.out.println("Wrong type for " + i + "-th overridden field in " + this.type.getType());
						System.exit(0);
					}
				} else {
					int i = offset;
					// ... dei METODI se sono positivi o zero
					
					// estraggo i tipi (altrimenti ho MethodNode) e controllo anche qu� che l'overriding non vada a modificarne il tipo
					DecNode classMethod = (DecNode) classMethods.get(i);
					DecNode superMethod = (DecNode) superMethods.get(i);

					if (FOOLlib.isSubType(classMethod.getSymType(), superMethod.getSymType()) == false) {
						System.out.println("Wrong type for " + i + "-th overridden method in " + this.type.getType());
						System.exit(0);

					}
				}
			}

		}
		// ritorno null perch� � una dichiarazione
		return null;
	}

	@Override
	public String codeGeneration() {
		// genero le etichette per tutti i metodo contenuti in methods
		for (int i = 0; i < this.methods.size(); i++){
			String label = FOOLlib.freshMethodLabel();
			((MethodNode)this.methods.get(i)).setLabel(label);
		}

		// mi richiamo su ciascun metodo
		for (int i = 0; i < this.methods.size(); i++)
			this.methods.get(i).codeGeneration();

		// sullo stack non metto nulla
		return "";
	}

	@Override
	public Node getSymType() {
		return this.type;
	}
}