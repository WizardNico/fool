package ast;

import interfaces.Node;
import lib.FOOLlib;

public class PlusNode implements Node {

	private Node left;
	private Node right;

	public PlusNode(Node l, Node r) {
		this.left = l;
		this.right = r;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "Plus\n" + this.left.toPrint(indent + "  ") + this.right.toPrint(indent + "  ");
	}

	/* Devo controllare che la somma avvenga appunto tra due interi */
	@Override
	public Node typeCheck() {
		/*
		 * prendo il figlio di sinistra e controllo che sia un sottotipo di
		 * IntNode perch� appunto la somma pu� avvenire solo tra interi, e
		 * stessa cosa per il figlio di destra. In sostanza volgiamo avere la
		 * somma SOLO tra due nodi di tipo IntNode.
		 * 
		 * Nel caso non sia cos� ritorno errore.
		 * 
		 * Altrimenti ritorno il risultato di una somma che � appunto un intero
		 * ancora.
		 */
		if (!((FOOLlib.isSubType(this.left.typeCheck(), new IntTypeNode())) && (FOOLlib.isSubType(this.right.typeCheck(), new IntTypeNode())))) {
			System.out.println("Non integers in sum");
			System.exit(0);
		}

		return new IntTypeNode();
	}

	@Override
	public String codeGeneration() {
		/*
		 * Calcolo le due sotto espressioni: SX e DX che poi verranno pushati
		 * sullo stack, una volta pushati sullo stack faccio add, che ha effetto
		 * sui primi due elementi dello stack appunto
		 */
		return this.left.codeGeneration() + this.right.codeGeneration() + "add\n";
	}
}
