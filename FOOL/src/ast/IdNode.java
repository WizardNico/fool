package ast;

import interfaces.Node;
import st.STentry;

public class IdNode implements Node {

	private String id;
	private STentry entry;
	private int nestingLevel;

	public IdNode(String id, STentry entry, int nl) {
		this.id = id;
		this.entry = entry;
		this.nestingLevel = nl;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "Id:" + this.id + " at nestlev " + this.nestingLevel + "\n" + this.entry.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() {
		// controllo che non sia un metodo
		if(this.entry.isMethod()){
			System.out.println("Wrong usage of method identifier");
			System.exit(0);
		}
		
		// controllo che non sia null, ovvero il nome di una classe
		if(this.id.equals("null")){
			System.out.println("Wrong usage of class identifier");
			System.exit(0);
		}
		return this.entry.getType();
	}

	/*
	 * devo vedere dove � dichiarato, quindi qu� mi calcolo la differenza di
	 * nesting level per risalire la catena, dovr� poi generare tanti lw per
	 * quanti passi di nesting level devo fare. Allora genero una serie di "lw"
	 * per tanti quanti sono i passi che devo fare.
	 */
	private String getAR() {
		String getAR = "";
		// carica sullo stack il valore a quell'indirizzo, quindi lo uso per salire la catena, un lw un passo
		for (int i = 0; i < this.nestingLevel - this.entry.getNestingLevel(); i++)
			getAR += "lw\n";
		
		return getAR;
	}
	
	@Override
	public String codeGeneration() {		
		/*
		 * IdNode rappresenta un qualsiasi ID -> variabile o funzione.
		 * 
		 * qua incontro l'uso di una varaibile, quindi pusho il suo valore preso
		 * all'indirizzo del suo offset che lo abbiamo appunto nella "pallina",
		 * ovvero nella STentry, nella symbletable.
		 * 
		 * Se dichiaro l'uso di una variabile all'interno di una funzione, devo risalire 
		 * usando l'access link, scorrendo tutta la catena, andando per differenza di nesting level
		 * 
		 * Per recuperare il valore della variabile o della funzione, utilizzo il frame pointer FP (punto fisso) 
		 * che sar� stato settato nella chiamata a funzione.
		 * 
		 * Se l'ID � dichiarato localmente allora semplicemente lo raggiungo tramite il
		 * FP e l'offset. 
		 * Nel caso in cui non lo sia devo risalire la catena statica e andare a recuperare i valori/e nell'AR della dichiarazione.
		 * Per risalire la catena statica si fa nel solito modo:
		 * FP mi punta all'AL (che � a offset 0) allora pusho FP sullo stack
		 * faccio delle lw tante volte quanto � lvl_nesting_utilizzo - lvl_nesting_dichiarazione
		 * 
		 */
		
		//Se ho un id FUNZIONALE
		if(this.entry.getType() instanceof ArrowTypeNode){
			/*---PRIMA RECUPERO IL VALORE DEL FRAME POINTER---*/
			return "push " + this.entry.getOffset() + "\n"//Carico il valore del FP (a offset 0 ho il FP e a -1 l'indirizzo)
					+ "lfp\n" //Carico frame pointer (fp) sullo stack
					+ getAR() //Salto all'AR selezionato, se � locale allora � zero e sono gi�qua, altrimenti se � dichiarata fuori la stringa non sar� vuota e risalgo la catena statica
					+ "add\n" // sommando mi posiziono sull'FP dell' AR	della funzione
					+ "lw\n" //recupero il valore del FP e lo metto sullo stack all'indirizzo specificato
			
					/*
					 * Quindi qu� sopra ho recuperato il valore del frame pointer dove la funzione (che stiamo usando come id), � stata dichiarata.
					 * FP punta all'AR (activation record) della funzione, manca solo da pushare l'indirizzo subito sotto.
					 */			
					
					/*---ORA RECUPERO L'INDIRIZZO DELLA FUNZIONE---*/			
					+ "push " + (this.entry.getOffset() - 1) + "\n" //Carico l'indirizzo della funzione all'(offset-1), perch� all'offset 0 ho il FP come da layout higher order
					+ "lfp\n" //Carico frame pointer (fp) sullo stack
					+ getAR() //Salto all' AR dove � definita la funzione
					+ "add\n" //sommando mi posiziono sull'FP della funzione	
					+ "lw\n"; //recupero l'indirizzo della funzione, e lo metto sullo stack all'indirizzo specificato
			
		} else {//Se ho un id di VARIABILE
			return "push " + this.entry.getOffset() + "\n"//carico l'indirizzo della variabile
					+"lfp\n" //Carico frame pointer (fp) sullo stack
					+ getAR() //Salto all'AR selezionato, se � locale allora � zero e sono gi� qua, altrimenti se � dichiarata fuori la stringa non sar� vuota e risalgo la catena statica
					+"add\n" //sommando mi posiziono sull'FP della variabile
					+"lw\n";//carica sullo stack il valore a quell'indirizzo
		}
	}
}