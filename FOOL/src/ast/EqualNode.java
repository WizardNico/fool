package ast;

import interfaces.Node;
import lib.FOOLlib;

public class EqualNode implements Node {

	private Node left;
	private Node right;

	public EqualNode(Node left, Node right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "Equals\n" + this.left.toPrint(indent + "  ") + this.right.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() {
		/* Prenod il tipo dei due nodi agganciati al nodo Equals(==) */
		Node l = this.left.typeCheck();
		Node r = this.right.typeCheck();

		/*
		 * Controllo che i due nodi da confrontare non siano di tipo funzionale.
		 * Nel caso lo fossero, per semplicit�, vietiamo il confronto 
		 */
		if(l instanceof ArrowTypeNode || r instanceof ArrowTypeNode){
			System.out.println("Type error: function in == !");
			System.exit(0);
		}
		
		/*
		 * Controllo che siano l'uno il sottotipo dell'altro, ovvero booleani
		 * con booleani e interi con interi, mediante il metodo di lowestcommon ancestor,
		 * due scenari allora:
		 * 
		 * -CASO POSITIVO: torno un Booleano per confermare il risultato della
		 * comparazione: vera o falsa.
		 * 
		 * -CASO NEGATIVO torno un errore di tipo sull'uguale
		 */
		if (FOOLlib.lowestCommonAncestor(l, r) == null) {
			System.out.println("Incompatible types in equal!");
			System.exit(0);
		}

		return new BoolTypeNode();
	}

	@Override
	public String codeGeneration() {
		//Mi genero due nuove label
		String l1= FOOLlib.freshVariableLabel();
		String l2= FOOLlib.freshVariableLabel();
		
		/*
		 * Prende l'elemento sx, poi prende l'elemento dx, li confronta e torna
		 * true, quindi 1 se sono uguali, altrimenti false, che � 0. Ci si
		 * aspetta quindi che i due numeri siano sulla cima dello stack, li
		 * poppa, e quindi dobbiamo pushare o 1 o 0 a seconda del risultato
		 * della equivalenza
		 */
		return this.left.codeGeneration() 
				//Valuto l'espressione a destra
				+ this.right.codeGeneration() 
				+ "beq "+ l1 +"\n"//Se � uguale salto a l1
				+ "push 0\n"//Se non � uguale pusho 0
				+ "b "+ l2 +"\n"//Se ho gi� messo 0 allora salto via senno mette anche 1
			
				+ l1 + ":\n"//Se il controllo � vero salt� qua
				+ "push 1\n"//Pusho 1
				+ l2 + ":\n";//Se sono diversi salto qu� ed esco mettendo solo 0
	}
}
