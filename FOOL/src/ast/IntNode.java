package ast;

import interfaces.Node;

public class IntNode implements Node {

	private Integer val;

	public IntNode(Integer val) {
		this.val = val;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "Int:" + Integer.toString(this.val) + "\n";
	}

	@Override
	public Node typeCheck() {
		/* E' un semplcie int node quindi ritorna se stesso */
		return new IntTypeNode();
	}

	@Override
	public String codeGeneration() {
		// Ritorno una push del numero sullo stack
		return "push " + this.val + "\n";
	}
}
