package ast;

import interfaces.Node;
import lib.FOOLlib;

public class IfNode implements Node {

	private Node cond;
	private Node th;
	private Node el;

	public IfNode(Node cond, Node th, Node el) {
		this.cond = cond;
		this.th = th;
		this.el = el;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "If\n" + this.cond.toPrint(indent + "  ") + this.th.toPrint(indent + "  ")
				+ this.el.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() {
		/*
		 * Prendo il tipo della condizione, ovvero dove abbiamo l'equal e vedo
		 * che sia di tipo Booleano, ovvero che l'operazione torni un bool,
		 * altrimenti ritorno un errore di tipo nella condizione if
		 */
		if (!FOOLlib.isSubType(this.cond.typeCheck(), new BoolTypeNode())) {
			System.out.println("Non boolean condition in if");
			System.exit(0);
		}

		// Prendo i tipi di: then e else
		Node t = this.th.typeCheck();
		Node e = this.el.typeCheck();

		Node lca = null;
		
		// richiamo lowestCommonAncestor sui tipi dell' espressione del then & else
		lca = FOOLlib.lowestCommonAncestor(t, e);
		if(lca != null){
			return lca;
		}

		System.out.println("Incompatible types in then-else branches");
		System.exit(0);
		return null;
	}

	@Override
	public String codeGeneration() {
		//Mi genero due nuove label
		String l1= FOOLlib.freshVariableLabel();
		String l2= FOOLlib.freshVariableLabel();
		
		/* 
		 * Prima di tutto controlliamo la condizione che ritorner�un valore booleano (0/1)
		 * quindi prendiamo la codegeneration di cond e mettiamo come secondo elemento 1, ovvero vediamo se � true  
		 */
		return this.cond.codeGeneration() //Mette sullo stack il risultato di cond
				+ "push 1\n"//MEtto sullo stack 1, true
				+ "beq "+ l1 +"\n"//Mi chiedo se la condizione allora � = 1, ovvero � true, verificata	
				
				+ this.el.codeGeneration() //Se non � vera continua qu� e fa else
				+ "b  "+ l2 +"\n"//Fatto else salta via senza eseguire then	
				
				+ l1 + ":\n"//Se torna 1, � true salto qu� ed eseguo il codice di then	
				+ this.th.codeGeneration()//Eseguo il codice di then
				+ l2 + ":\n";//Salto qu� se ho fatto else, e salto via praticamente senza passare per il then
	}
}
