package ast;

import interfaces.Node;
import lib.FOOLlib;

public class MultNode implements Node {

	private Node left;
	private Node right;

	public MultNode(Node left, Node right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public String toPrint(String indent) {
		return indent + "Times\n" + this.left.toPrint(indent + "  ") + this.right.toPrint(indent + "  ");
	}

	@Override
	public Node typeCheck() {
		/* funziona come il plusnode */
		if (!((FOOLlib.isSubType(this.left.typeCheck(), new IntTypeNode())) && (FOOLlib.isSubType(this.right.typeCheck(), new IntTypeNode())))) {
			System.out.println("Non integers in multiplication");
			System.exit(0);
		}
		return new IntTypeNode();
	}

	@Override
	public String codeGeneration() {
		/*
		 * Calcolo le due sotto espressioni: SX e DX che poi verranno pushati
		 * sullo stack, una volta pushati sullo stack faccio mult, che ha effetto
		 * sui primi due elementi dello stack appunto
		 */
		return this.left.codeGeneration() + this.right.codeGeneration() + "mult\n";
	}
}
