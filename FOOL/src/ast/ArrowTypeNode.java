package ast;

import java.util.ArrayList;

import interfaces.Node;

public class ArrowTypeNode implements Node {

	private ArrayList<Node> parList; // tipi dei parametri
	private Node returnNode; // tipo del ritorno

	/*
	 * Nodo relativo all'entry della symble table alla dichiarazione di
	 * funzione. Questo nodo contiene la dichiarazione di funzione dentro la
	 * symble table
	 */
	public ArrowTypeNode(ArrayList<Node> parList, Node ret) {
		this.parList = parList;
		this.returnNode = ret;
	}

	private String printParList(String s) {
		String parstr = "";
		for (Node dec : this.parList) {
			parstr += dec.toPrint(s + "  ");
		}
		return parstr;
	}

	public String toPrint(String indent) {
		return indent + "ArrowTypeNode\n" + printParList(indent) + this.returnNode.toPrint(indent + "  ->");
	}

	@Override
	public Node typeCheck() {
		/* Non verr� mai chiamato, sono sulla foglia */
		return null;
	}

	public Node getRet() {
		return this.returnNode;
	}

	public ArrayList<Node> getParList() {
		return this.parList;
	}

	@Override
	public String codeGeneration() {
		// mai usato
		return null;
	}
}