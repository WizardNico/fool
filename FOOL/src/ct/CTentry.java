package ct;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import ast.*;
import interfaces.Node;
import st.STentry;

/*
 * Class-Table entry � un entry della Class-Table, ovvero la Virtual Table, la tabella che si occupa di tenere in vita i record e riferimenti relativi agli oggetti. 
 * Questa a differenza della Symble-Table non verr� mai Garbage Collectata
 */
public class CTentry {

	// la virtual table mappa simboli dichiarati dentro la
	// classe o ereditati (se non hanno subito overriding)
	// in una STentry
	private HashMap<String, STentry> virtualTable;

	// (<=-1) ad ogni campo incontrato DECREMENTO
	private int offsetFields;

	// (>=0) ad ogni metodo incontrato INCREMENTO
	private int offsetMethods;

	/*
	 * contiene tutti i figli virtuali (cio� della super e dell'oggetto stesso)
	 * che sono campi. Essi sono ordinati in base al loro offset che viene
	 * mappato nelle posizioni dell'array come: -(offsetCampo)-1
	 */
	private ArrayList<Node> allFields;

	/*
	 * contiene tutti i figli virtuali (cio� della super e dell'oggetto stesso)
	 * che sono metodi. Essi sono ordinati in base al loro offset e sono mappati
	 * nell' array come: offset metodo
	 */
	private ArrayList<Node> allMethods;

	/*
	 * insieme che contiene i metodi/campi dichiarati da questa classe dei quali
	 * non posso farne l'override internamente ad essa. Nota che viene sempre
	 * inizializzata a vuota anche quando eredito da un altra classe.
	 */
	HashSet<Integer> locals;

	// costruttore vuoto, caso in cui non eredito da alcuna classe
	public CTentry() {
		this.virtualTable = new HashMap<String, STentry>();
		this.offsetFields = -1;
		this.offsetMethods = 0;
		this.allFields = new ArrayList<Node>();
		this.allMethods = new ArrayList<Node>();
		this.locals = new HashSet<Integer>();
	}

	// costruttore copia, se eridito da qualcuno copio tutte le sue info
	public CTentry(CTentry entry) {
		this.virtualTable = new HashMap<String, STentry>(entry.getVirtualTable());
		this.offsetFields = entry.getOffsetFields();
		this.offsetMethods = entry.getOffsetMethods();
		this.allFields = new ArrayList<Node>(entry.getAllFields());
		this.allMethods = new ArrayList<Node>(entry.getAllMethods());
		this.locals = new HashSet<Integer>();
	}

	/*
	 * vado ad aggiungere un campo alla classe, non ereditato da
	 * nessuno esso va quindi aggiunto alla virtual table (se non esiste) oppure
	 * va fatto l'overriding di quello ereditato
	 */
	public FieldNode addField(String name, Node type) {
		//Prendo il campo dalla virtual table, se c'� (overriding)
		STentry lastEntry = this.virtualTable.get(name);
		STentry entry;
		//Creo il nodo campo
		FieldNode field = new FieldNode(name, type);
		int offset;

		// caso di ovveriding, il campo esiste gi�
		if (lastEntry != null) {
			// leggo l'offset che era stato assegnato
			offset = lastEntry.getOffset();
			// creo una nuova STEntry che sostituisce la precedente
			entry = new STentry(1, type, offset);
			// sostituisco anche in allFields
			this.allFields.set(-(offset) - 1, field);

			// caso 2: campo nuovo
		} else {
			// creo una nuova STentry il nesting level � per forza 1
			// e utilizzo l'offset dei campi al quale ero arrivato
			entry = new STentry(1, type, this.offsetFields);
			//Mi salvo l'offset per il controllo che far� sotto se � una ridefinizione di un metodo locale
			offset = this.offsetFields;
			// il quale va decrementato (in base al layout dell'oggetto)
			this.offsetFields--;
			// aggiungo il campo in fondo come da layout
			this.allFields.add(field);
		}

		//ritorna true se non era presente, altrimenti false e do errore
		//Con questa operazione vado a scrivermi una mappa dei metodi locali che non posso overridare
		//(se non cera vuol dire che � nuovo quindi lo scrivo e lo segno come un campo interno a questa classe, originale di questa classe)
		//Se mi da false � una redifinizione interna alla stessa classe => ERRORE
		if (!this.locals.add(offset)) {
			System.out.println("Error: field " + name + " declared twice!");
			System.exit(0);
		} else {
			// inserisco l'entry nella virtual table
			this.virtualTable.put(name, entry);
		}
		return field;
	}

	/* Uguale a sopra */
	public void addMethod(String name, MethodNode method) {
		STentry lastEntry = this.virtualTable.get(name);
		STentry entry;

		int offset;

		if (lastEntry != null) {
			offset = lastEntry.getOffset();
			entry = new STentry(1, method.getSymType(), offset);
			this.allMethods.set(offset, method);
		} else {
			entry = new STentry(1, method.getSymType(), this.offsetMethods);
			offset = this.offsetMethods;
			entry.isMethod();
			//In questo caso incremento
			this.offsetMethods++;
			this.allMethods.add(method);

		}
		
		entry.setAsMethod();

		//Controllo che non sia una ridefinizione locale
		if (!this.locals.add(offset)) {
			System.out.println("Error: method " + name + " declared twice!");
			System.exit(0);
		} else {
			this.virtualTable.put(name, entry);
		}		
	}

	public String toPrint(String indent) {
		return indent + "CTEntry: virtualTable = " + this.virtualTable.toString() + "\n" + indent
				+ "CTEntry: offsetFields = " + this.offsetFields + "\n" + indent + "CTEntry: offsetMethods = "
				+ this.offsetMethods + "\n" + indent + "CTEntry: locals = " + this.locals.toString() + "\n";
	}

	public HashMap<String, STentry> getVirtualTable() {
		return this.virtualTable;
	}

	public int getOffsetFields() {
		return this.offsetFields;
	}

	public int getOffsetMethods() {
		return this.offsetMethods;
	}

	public ArrayList<Node> getAllFields() {
		return this.allFields;
	}

	public ArrayList<Node> getAllMethods() {
		return this.allMethods;
	}

	public HashSet<Integer> getLocals() {
		return this.locals;
	}
}
