// $ANTLR 3.5.2 C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g 2017-01-30 16:15:27

import java.util.HashMap;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class SVMParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "ADD", "BRANCH", "BRANCHEQ", "BRANCHLESSEQ", 
		"COL", "COPYFP", "DIV", "ERR", "HALT", "JS", "LABEL", "LOADFP", "LOADHP", 
		"LOADRA", "LOADRV", "LOADW", "MULT", "NUMBER", "POP", "PRINT", "PUSH", 
		"STOREFP", "STOREHP", "STORERA", "STORERV", "STOREW", "SUB", "WHITESP"
	};
	public static final int EOF=-1;
	public static final int ADD=4;
	public static final int BRANCH=5;
	public static final int BRANCHEQ=6;
	public static final int BRANCHLESSEQ=7;
	public static final int COL=8;
	public static final int COPYFP=9;
	public static final int DIV=10;
	public static final int ERR=11;
	public static final int HALT=12;
	public static final int JS=13;
	public static final int LABEL=14;
	public static final int LOADFP=15;
	public static final int LOADHP=16;
	public static final int LOADRA=17;
	public static final int LOADRV=18;
	public static final int LOADW=19;
	public static final int MULT=20;
	public static final int NUMBER=21;
	public static final int POP=22;
	public static final int PRINT=23;
	public static final int PUSH=24;
	public static final int STOREFP=25;
	public static final int STOREHP=26;
	public static final int STORERA=27;
	public static final int STORERV=28;
	public static final int STOREW=29;
	public static final int SUB=30;
	public static final int WHITESP=31;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public SVMParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public SVMParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return SVMParser.tokenNames; }
	@Override public String getGrammarFileName() { return "C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g"; }


	  // spazio di memoria per metterci il codice, non può essere privato perchè poi va passato alla virtual machine
	  int[] code = new int[ExecuteVM.CODESIZE];
	  
	  //contatore, per riempire l'array col codice
	  private int i = 0;
	  
	  // per le etichette
	  // HashMap da etichette a posti dove occorrono nel asm, la aggiorniamo ad ogni dichiarazione di etichetta LABEL COL
	  private HashMap<String,Integer> labelAdd = new HashMap<String,Integer>();
	  
	  // per i riferimenti ad etichette (che sono nelle istruzioni di salto)
	  private HashMap<Integer, String> labelRef = new HashMap<Integer, String>();  



	// $ANTLR start "assembly"
	// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:30:1: assembly : ( PUSH n= NUMBER | PUSH l= LABEL | POP | ADD | SUB | MULT | DIV | STOREW | LOADW |l= LABEL COL | BRANCH l= LABEL | BRANCHEQ l= LABEL | BRANCHLESSEQ l= LABEL | JS | LOADRA | STORERA | LOADRV | STORERV | LOADFP | STOREFP | COPYFP | LOADHP | STOREHP | PRINT | HALT )* ;
	public final void assembly() throws RecognitionException {
		Token n=null;
		Token l=null;

		try {
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:30:9: ( ( PUSH n= NUMBER | PUSH l= LABEL | POP | ADD | SUB | MULT | DIV | STOREW | LOADW |l= LABEL COL | BRANCH l= LABEL | BRANCHEQ l= LABEL | BRANCHLESSEQ l= LABEL | JS | LOADRA | STORERA | LOADRV | STORERV | LOADFP | STOREFP | COPYFP | LOADHP | STOREHP | PRINT | HALT )* )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:30:11: ( PUSH n= NUMBER | PUSH l= LABEL | POP | ADD | SUB | MULT | DIV | STOREW | LOADW |l= LABEL COL | BRANCH l= LABEL | BRANCHEQ l= LABEL | BRANCHLESSEQ l= LABEL | JS | LOADRA | STORERA | LOADRV | STORERV | LOADFP | STOREFP | COPYFP | LOADHP | STOREHP | PRINT | HALT )*
			{
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:30:11: ( PUSH n= NUMBER | PUSH l= LABEL | POP | ADD | SUB | MULT | DIV | STOREW | LOADW |l= LABEL COL | BRANCH l= LABEL | BRANCHEQ l= LABEL | BRANCHLESSEQ l= LABEL | JS | LOADRA | STORERA | LOADRV | STORERV | LOADFP | STOREFP | COPYFP | LOADHP | STOREHP | PRINT | HALT )*
			loop1:
			while (true) {
				int alt1=26;
				switch ( input.LA(1) ) {
				case PUSH:
					{
					int LA1_2 = input.LA(2);
					if ( (LA1_2==NUMBER) ) {
						alt1=1;
					}
					else if ( (LA1_2==LABEL) ) {
						alt1=2;
					}

					}
					break;
				case POP:
					{
					alt1=3;
					}
					break;
				case ADD:
					{
					alt1=4;
					}
					break;
				case SUB:
					{
					alt1=5;
					}
					break;
				case MULT:
					{
					alt1=6;
					}
					break;
				case DIV:
					{
					alt1=7;
					}
					break;
				case STOREW:
					{
					alt1=8;
					}
					break;
				case LOADW:
					{
					alt1=9;
					}
					break;
				case LABEL:
					{
					alt1=10;
					}
					break;
				case BRANCH:
					{
					alt1=11;
					}
					break;
				case BRANCHEQ:
					{
					alt1=12;
					}
					break;
				case BRANCHLESSEQ:
					{
					alt1=13;
					}
					break;
				case JS:
					{
					alt1=14;
					}
					break;
				case LOADRA:
					{
					alt1=15;
					}
					break;
				case STORERA:
					{
					alt1=16;
					}
					break;
				case LOADRV:
					{
					alt1=17;
					}
					break;
				case STORERV:
					{
					alt1=18;
					}
					break;
				case LOADFP:
					{
					alt1=19;
					}
					break;
				case STOREFP:
					{
					alt1=20;
					}
					break;
				case COPYFP:
					{
					alt1=21;
					}
					break;
				case LOADHP:
					{
					alt1=22;
					}
					break;
				case STOREHP:
					{
					alt1=23;
					}
					break;
				case PRINT:
					{
					alt1=24;
					}
					break;
				case HALT:
					{
					alt1=25;
					}
					break;
				}
				switch (alt1) {
				case 1 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:31:7: PUSH n= NUMBER
					{
					match(input,PUSH,FOLLOW_PUSH_in_assembly44); 
					n=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_assembly48); 
					code[i++] = PUSH; code[i++]=Integer.parseInt((n!=null?n.getText():null));
					}
					break;
				case 2 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:32:7: PUSH l= LABEL
					{
					match(input,PUSH,FOLLOW_PUSH_in_assembly58); 
					l=(Token)match(input,LABEL,FOLLOW_LABEL_in_assembly62); 
					code[i++] = PUSH; labelRef.put(i++,(l!=null?l.getText():null));
					}
					break;
				case 3 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:33:7: POP
					{
					match(input,POP,FOLLOW_POP_in_assembly75); 
					code[i++]=POP;
					}
					break;
				case 4 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:34:7: ADD
					{
					match(input,ADD,FOLLOW_ADD_in_assembly87); 
					code[i++]=ADD;
					}
					break;
				case 5 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:35:7: SUB
					{
					match(input,SUB,FOLLOW_SUB_in_assembly99); 
					code[i++]=SUB;
					}
					break;
				case 6 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:36:7: MULT
					{
					match(input,MULT,FOLLOW_MULT_in_assembly111); 
					code[i++]=MULT;
					}
					break;
				case 7 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:37:7: DIV
					{
					match(input,DIV,FOLLOW_DIV_in_assembly123); 
					code[i++]=DIV;
					}
					break;
				case 8 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:38:7: STOREW
					{
					match(input,STOREW,FOLLOW_STOREW_in_assembly136); 
					code[i++]=STOREW;
					}
					break;
				case 9 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:39:7: LOADW
					{
					match(input,LOADW,FOLLOW_LOADW_in_assembly146); 
					code[i++]=LOADW;
					}
					break;
				case 10 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:40:7: l= LABEL COL
					{
					l=(Token)match(input,LABEL,FOLLOW_LABEL_in_assembly164); 
					match(input,COL,FOLLOW_COL_in_assembly166); 
					labelAdd.put((l!=null?l.getText():null),i);
					}
					break;
				case 11 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:41:7: BRANCH l= LABEL
					{
					match(input,BRANCH,FOLLOW_BRANCH_in_assembly209); 
					l=(Token)match(input,LABEL,FOLLOW_LABEL_in_assembly213); 
					code[i++]=BRANCH; labelRef.put(i++,(l!=null?l.getText():null));
					}
					break;
				case 12 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:42:7: BRANCHEQ l= LABEL
					{
					match(input,BRANCHEQ,FOLLOW_BRANCHEQ_in_assembly238); 
					l=(Token)match(input,LABEL,FOLLOW_LABEL_in_assembly242); 
					code[i++]=BRANCHEQ; labelRef.put(i++,(l!=null?l.getText():null));
					}
					break;
				case 13 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:43:7: BRANCHLESSEQ l= LABEL
					{
					match(input,BRANCHLESSEQ,FOLLOW_BRANCHLESSEQ_in_assembly259); 
					l=(Token)match(input,LABEL,FOLLOW_LABEL_in_assembly263); 
					code[i++]=BRANCHLESSEQ; labelRef.put(i++,(l!=null?l.getText():null));
					}
					break;
				case 14 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:44:7: JS
					{
					match(input,JS,FOLLOW_JS_in_assembly276); 
					code[i++]=JS;
					}
					break;
				case 15 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:45:7: LOADRA
					{
					match(input,LOADRA,FOLLOW_LOADRA_in_assembly354); 
					code[i++]=LOADRA;
					}
					break;
				case 16 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:46:7: STORERA
					{
					match(input,STORERA,FOLLOW_STORERA_in_assembly369); 
					code[i++]=STORERA;
					}
					break;
				case 17 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:47:7: LOADRV
					{
					match(input,LOADRV,FOLLOW_LOADRV_in_assembly383); 
					code[i++]=LOADRV;
					}
					break;
				case 18 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:48:7: STORERV
					{
					match(input,STORERV,FOLLOW_STORERV_in_assembly398); 
					code[i++]=STORERV;
					}
					break;
				case 19 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:49:7: LOADFP
					{
					match(input,LOADFP,FOLLOW_LOADFP_in_assembly412); 
					code[i++]=LOADFP;
					}
					break;
				case 20 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:50:7: STOREFP
					{
					match(input,STOREFP,FOLLOW_STOREFP_in_assembly427); 
					code[i++]=STOREFP;
					}
					break;
				case 21 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:51:7: COPYFP
					{
					match(input,COPYFP,FOLLOW_COPYFP_in_assembly441); 
					code[i++]=COPYFP;
					}
					break;
				case 22 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:52:7: LOADHP
					{
					match(input,LOADHP,FOLLOW_LOADHP_in_assembly456); 
					code[i++]=LOADHP;
					}
					break;
				case 23 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:53:7: STOREHP
					{
					match(input,STOREHP,FOLLOW_STOREHP_in_assembly471); 
					code[i++]=STOREHP;
					}
					break;
				case 24 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:54:7: PRINT
					{
					match(input,PRINT,FOLLOW_PRINT_in_assembly485); 
					code[i++]=PRINT;
					}
					break;
				case 25 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\SVM.g:55:7: HALT
					{
					match(input,HALT,FOLLOW_HALT_in_assembly501); 
					code[i++]=HALT;
					}
					break;

				default :
					break loop1;
				}
			}


			          for(Integer refAdd : labelRef.keySet()){
			            //ciclo sull'insieme delle chiavi di label ref
			            code[refAdd]=labelAdd.get(labelRef.get(refAdd));
			          }
			    
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "assembly"

	// Delegated rules



	public static final BitSet FOLLOW_PUSH_in_assembly44 = new BitSet(new long[]{0x0000000000200000L});
	public static final BitSet FOLLOW_NUMBER_in_assembly48 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_PUSH_in_assembly58 = new BitSet(new long[]{0x0000000000004000L});
	public static final BitSet FOLLOW_LABEL_in_assembly62 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_POP_in_assembly75 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_ADD_in_assembly87 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_SUB_in_assembly99 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_MULT_in_assembly111 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_DIV_in_assembly123 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_STOREW_in_assembly136 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_LOADW_in_assembly146 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_LABEL_in_assembly164 = new BitSet(new long[]{0x0000000000000100L});
	public static final BitSet FOLLOW_COL_in_assembly166 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_BRANCH_in_assembly209 = new BitSet(new long[]{0x0000000000004000L});
	public static final BitSet FOLLOW_LABEL_in_assembly213 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_BRANCHEQ_in_assembly238 = new BitSet(new long[]{0x0000000000004000L});
	public static final BitSet FOLLOW_LABEL_in_assembly242 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_BRANCHLESSEQ_in_assembly259 = new BitSet(new long[]{0x0000000000004000L});
	public static final BitSet FOLLOW_LABEL_in_assembly263 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_JS_in_assembly276 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_LOADRA_in_assembly354 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_STORERA_in_assembly369 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_LOADRV_in_assembly383 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_STORERV_in_assembly398 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_LOADFP_in_assembly412 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_STOREFP_in_assembly427 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_COPYFP_in_assembly441 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_LOADHP_in_assembly456 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_STOREHP_in_assembly471 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_PRINT_in_assembly485 = new BitSet(new long[]{0x000000007FDFF6F2L});
	public static final BitSet FOLLOW_HALT_in_assembly501 = new BitSet(new long[]{0x000000007FDFF6F2L});
}
