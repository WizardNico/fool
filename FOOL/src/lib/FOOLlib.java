package lib;

import java.util.ArrayList;
import java.util.HashMap;

import ast.*;
import interfaces.Node;

public class FOOLlib {

	private static int variablesLabelCount = 0;
	private static int functionsLabelCount = 0;
	private static int methodLabelCount = 0;
	private static String funCode = "";
	// mappa le classi con le relative superclassi: classID -> superClassID
	private static HashMap<String, String> superType;

	/*
	 * Valuta se il tipo "a" � <= al tipo "b", dove "a" e "b" sono tipi di base:
	 * int o bool
	 */
	public static boolean isSubType(Node a, Node b) {
		boolean result;

		// se a e b hanno tipo funzionale
		if (a instanceof ArrowTypeNode && b instanceof ArrowTypeNode) {
			/*--RELAZIONE DI CO-VARIANZA sul tipo di ritorno (cio� posso prendere un tipo di ritorno pi� specifico)--*/

			// Prendo il ritorno della funzione a
			Node a_ret = ((ArrowTypeNode) a).getRet();
			// Prendo il ritorno della funzione b
			Node b_ret = ((ArrowTypeNode) b).getRet();

			// controllo che siano l'uno il sottotipo dell'altra, quindi vado a fondo
			boolean result_ret = FOOLlib.isSubType(a_ret, b_ret);

			/*--RELAZIONE DI CONTRO-VARIANZA sul tipo dei parametri (cio� posso prendere argomenti pi� generali)--*/

			// Prendo la lista dei parametri di a
			ArrayList<Node> a_par = ((ArrowTypeNode) a).getParList();
			// Prendo la lista dei parametri di b
			ArrayList<Node> b_par = ((ArrowTypeNode) b).getParList();

			boolean result_par = true;

			// Vado a controllare che ogni coppia di parametri siano sottotipo
			// l'uno dell'altro (parametro di b sottotipo di quello di a)
			for (int i = 0; i < a_par.size(); i++)
				result_par &= FOOLlib.isSubType(b_par.get(i), a_par.get(i));

			// Fondo i risultati
			result = result_ret && result_par;

		} else if (a instanceof ClassTypeNode && b instanceof ClassTypeNode) {

			// se sono entrambi classi vado a vedere le relazioni nella mappa superType
			String classA = ((ClassTypeNode) a).getType();
			String classB = ((ClassTypeNode) b).getType();

			// devo controllare che a sia sottotipo (o uguale) di b
			if (classA.equals(classB)) {
				// caso 1: � un oggetto della stessa classe
				result = true;
			} else {
				// caso 2: � un sottotipo
				// risalgo la mappa finche non incontro b (se lo incontro)
				result = false;
				String currentClass = classA;
				String superClass = null;
				// Inizio la risalita
				do {
					superClass = FOOLlib.superType.get(currentClass);//Prendo la superclass di classA...e cos� via

					if (superClass != null && superClass.equals(classB))
						result = true;

					currentClass = superClass;
				} while (result == false && superClass != null);
			}

		} else if (a instanceof EmptyTypeNode && b instanceof ClassTypeNode) {
			// oppure se a � null allora � sottotipo di qualsiasi classe
			result = true;
		} else {
			// altrimenti faccio il controllo di subtyping normale
			result = a.getClass().equals(b.getClass()) || (a instanceof BoolTypeNode && b instanceof IntTypeNode);
		}

		return result;
	}

	public static Node lowestCommonAncestor(Node a, Node b) {
		// casi particolari: ho un null allora torno l'altro
		if (a instanceof EmptyTypeNode)
			return b;

		if (b instanceof EmptyTypeNode)
			return a;
		
		// se sto lavorando con classi
		if (a instanceof ClassTypeNode && b instanceof ClassTypeNode) {			

			// considero a, risalgo le sue superclassi tramite la map superType
			String currentClassID = ((ClassTypeNode) a).getType();

			Node currentType = a;
			String superClassID = null;

			do {
				// controllo che b non sia un sottotipo diretto della classe attualmente considerata
				if (FOOLlib.isSubType(b, currentType)) {
					// nel caso in cui lo sia => ritorno la classe corrente a
					return currentType;
				}
				// altrimenti ricavo l'ID della superclasse tramite la map superType
				
				//Risalgo la gerarchia passando alla supercalsse della classe corrente a
				superClassID = FOOLlib.superType.get(currentClassID);
				currentClassID = superClassID;//E la setto come classe corrente di conseguenza
				
				//e con quello costruisco un nodo ClassTypeNode per il tipo
				//della super classe (aggiornando la var utilizzata nel controllo)
				currentType = new ClassTypeNode(superClassID);
			} while (superClassID != null);

		} else if (a instanceof ArrowTypeNode && b instanceof ArrowTypeNode) {
			// se sto lavorando con tipo funzionali (con lo stesso numero di parametri)
			ArrowTypeNode typeA = (ArrowTypeNode) a;
			ArrowTypeNode typeB = (ArrowTypeNode) b;

			if (typeA.getParList().size() != typeB.getParList().size()) {
				return null;
			}

			// controllo il tipo di ritorno ricorsivamente
			Node retType = FOOLlib.lowestCommonAncestor(typeA.getRet(), typeB.getRet());
			if (retType == null) {
				return null;
			}

			// Controllo i parametri
			ArrayList<Node> parList = new ArrayList<Node>();
			for (int i = 0; i < typeA.getParList().size(); i++) {
				if (FOOLlib.isSubType(typeA.getParList().get(i), typeB.getParList().get(i))) {
					parList.add(typeA.getParList().get(i));
				} else if (FOOLlib.isSubType(typeB.getParList().get(i), typeA.getParList().get(i))) {
					parList.add(typeB.getParList().get(i));
				} else
					// fallisco
					return null;
			}
			// sono arrivato in fondo, ritorno il tipo lowestCommonAncestor
			return new ArrowTypeNode(parList, retType);
		} else {
			if (a instanceof IntTypeNode || b instanceof IntTypeNode)
				return new IntTypeNode();
			else
				return new BoolTypeNode();
		}
		return null;
	}

	/* Tutte le volte che lo chiamo mi da una label nuova per le variabili */
	public static String freshVariableLabel() {
		return "variable" + (variablesLabelCount++);
	}

	/* Tutte le volte che lo chiamo mi da una label nuova per le funzioni*/
	public static String freshFunctionLabel() {
		return "function" + (functionsLabelCount++);
	}
	
	/* Tutte le volte che lo chiamo mi da una label nuova per i metodi*/
	public static String freshMethodLabel() {
		return "method" + (methodLabelCount++);
	}

	/*
	 * Aggiunge una linea vuota di separazione prima di funzione e aggiunge il
	 * codice alla stringa che usamo un po come una "repository"
	 */
	public static void putCode(String c) {
		funCode += "\n" + c;
	}

	/* Restituisce la repository con i codici delle funzioni */
	public static String getCode() {
		return funCode;
	}

	/* Setter per la super-type Map */
	public static void setSuperTypeMap(HashMap<String, String> supType) {
		superType = supType;
	}
}
