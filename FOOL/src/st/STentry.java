package st;

import interfaces.Node;

public class STentry {

	private int nestingLevel;
	
	private Node type;
	
	// utilizzato durante la generazione di codice in base al layout per
	// individuare questo oggetto
	private int offset;
	
	// permette di identificare entry di metodi in modo che vengano trattate
	// diversamente da CallNode
	private boolean isMethod;

	public STentry(int nestingLevel, int offset) {
		this.nestingLevel = nestingLevel;
		this.offset = offset;
	}

	public STentry(int nestingLevel, Node type, int offset) {
		this.nestingLevel = nestingLevel;
		this.type = type;
		this.offset = offset;
	}

	public void addType(Node t) {
		this.type = t;
	}

	public Node getType() {
		return this.type;
	}

	public void setAsMethod() {
		this.isMethod = true;
	}

	public boolean isMethod() {
		return this.isMethod;
	}

	public int getOffset() {
		return this.offset;
	}

	public int getNestingLevel() {
		return this.nestingLevel;
	}

	public String toPrint(String s) {
		return s + "STentry: nestlev " + Integer.toString(this.nestingLevel) + "\n" + s + "STentry: offset "
				+ Integer.toString(this.offset) + "\n" + s + "STentry: type\n" + this.type.toPrint(s + "  ");
	}

}