// $ANTLR 3.5.2 C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g 2017-01-30 16:15:26

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class FOOLLexer extends Lexer {
	public static final int EOF=-1;
	public static final int AND=4;
	public static final int ARROW=5;
	public static final int ASS=6;
	public static final int BOOL=7;
	public static final int CLASS=8;
	public static final int CLPAR=9;
	public static final int COLON=10;
	public static final int COMMA=11;
	public static final int COMMENT=12;
	public static final int CRPAR=13;
	public static final int DIV=14;
	public static final int DOT=15;
	public static final int ELSE=16;
	public static final int EQ=17;
	public static final int ERR=18;
	public static final int EXTENDS=19;
	public static final int FALSE=20;
	public static final int FUN=21;
	public static final int GE=22;
	public static final int ID=23;
	public static final int IF=24;
	public static final int IN=25;
	public static final int INT=26;
	public static final int INTEGER=27;
	public static final int LE=28;
	public static final int LET=29;
	public static final int LPAR=30;
	public static final int MINUS=31;
	public static final int NEW=32;
	public static final int NOT=33;
	public static final int NULL=34;
	public static final int OR=35;
	public static final int PLUS=36;
	public static final int PRINT=37;
	public static final int RPAR=38;
	public static final int SEMIC=39;
	public static final int THEN=40;
	public static final int TIMES=41;
	public static final int TRUE=42;
	public static final int VAR=43;
	public static final int WHITESP=44;

	int lexicalErrors=0;


	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public FOOLLexer() {} 
	public FOOLLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public FOOLLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g"; }

	// $ANTLR start "PLUS"
	public final void mPLUS() throws RecognitionException {
		try {
			int _type = PLUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:539:6: ( '+' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:539:8: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PLUS"

	// $ANTLR start "TIMES"
	public final void mTIMES() throws RecognitionException {
		try {
			int _type = TIMES;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:540:7: ( '*' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:540:9: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TIMES"

	// $ANTLR start "MINUS"
	public final void mMINUS() throws RecognitionException {
		try {
			int _type = MINUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:541:7: ( '-' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:541:9: '-'
			{
			match('-'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MINUS"

	// $ANTLR start "DIV"
	public final void mDIV() throws RecognitionException {
		try {
			int _type = DIV;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:542:7: ( '/' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:542:9: '/'
			{
			match('/'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIV"

	// $ANTLR start "OR"
	public final void mOR() throws RecognitionException {
		try {
			int _type = OR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:544:5: ( '||' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:544:7: '||'
			{
			match("||"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OR"

	// $ANTLR start "AND"
	public final void mAND() throws RecognitionException {
		try {
			int _type = AND;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:545:5: ( '&&' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:545:7: '&&'
			{
			match("&&"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AND"

	// $ANTLR start "EQ"
	public final void mEQ() throws RecognitionException {
		try {
			int _type = EQ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:546:5: ( '==' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:546:7: '=='
			{
			match("=="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EQ"

	// $ANTLR start "NOT"
	public final void mNOT() throws RecognitionException {
		try {
			int _type = NOT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:547:5: ( 'not' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:547:7: 'not'
			{
			match("not"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NOT"

	// $ANTLR start "GE"
	public final void mGE() throws RecognitionException {
		try {
			int _type = GE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:548:5: ( '>=' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:548:7: '>='
			{
			match(">="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GE"

	// $ANTLR start "LE"
	public final void mLE() throws RecognitionException {
		try {
			int _type = LE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:549:5: ( '<=' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:549:7: '<='
			{
			match("<="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LE"

	// $ANTLR start "ARROW"
	public final void mARROW() throws RecognitionException {
		try {
			int _type = ARROW;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:551:9: ( '->' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:551:11: '->'
			{
			match("->"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ARROW"

	// $ANTLR start "DOT"
	public final void mDOT() throws RecognitionException {
		try {
			int _type = DOT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:552:5: ( '.' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:552:7: '.'
			{
			match('.'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DOT"

	// $ANTLR start "CLPAR"
	public final void mCLPAR() throws RecognitionException {
		try {
			int _type = CLPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:553:7: ( '{' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:553:9: '{'
			{
			match('{'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CLPAR"

	// $ANTLR start "CRPAR"
	public final void mCRPAR() throws RecognitionException {
		try {
			int _type = CRPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:554:7: ( '}' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:554:9: '}'
			{
			match('}'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CRPAR"

	// $ANTLR start "CLASS"
	public final void mCLASS() throws RecognitionException {
		try {
			int _type = CLASS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:555:7: ( 'class' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:555:9: 'class'
			{
			match("class"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CLASS"

	// $ANTLR start "EXTENDS"
	public final void mEXTENDS() throws RecognitionException {
		try {
			int _type = EXTENDS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:556:9: ( 'extends' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:556:11: 'extends'
			{
			match("extends"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EXTENDS"

	// $ANTLR start "NEW"
	public final void mNEW() throws RecognitionException {
		try {
			int _type = NEW;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:557:7: ( 'new' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:557:9: 'new'
			{
			match("new"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NEW"

	// $ANTLR start "NULL"
	public final void mNULL() throws RecognitionException {
		try {
			int _type = NULL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:558:9: ( 'null' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:558:11: 'null'
			{
			match("null"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NULL"

	// $ANTLR start "COLON"
	public final void mCOLON() throws RecognitionException {
		try {
			int _type = COLON;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:560:7: ( ':' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:560:9: ':'
			{
			match(':'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COLON"

	// $ANTLR start "COMMA"
	public final void mCOMMA() throws RecognitionException {
		try {
			int _type = COMMA;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:561:7: ( ',' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:561:9: ','
			{
			match(','); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMA"

	// $ANTLR start "ASS"
	public final void mASS() throws RecognitionException {
		try {
			int _type = ASS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:562:5: ( '=' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:562:7: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ASS"

	// $ANTLR start "SEMIC"
	public final void mSEMIC() throws RecognitionException {
		try {
			int _type = SEMIC;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:563:7: ( ';' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:563:9: ';'
			{
			match(';'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SEMIC"

	// $ANTLR start "INTEGER"
	public final void mINTEGER() throws RecognitionException {
		try {
			int _type = INTEGER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:564:9: ( ( ( '1' .. '9' ) ( '0' .. '9' )* ) | '0' )
			int alt2=2;
			int LA2_0 = input.LA(1);
			if ( ((LA2_0 >= '1' && LA2_0 <= '9')) ) {
				alt2=1;
			}
			else if ( (LA2_0=='0') ) {
				alt2=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 2, 0, input);
				throw nvae;
			}

			switch (alt2) {
				case 1 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:564:11: ( ( '1' .. '9' ) ( '0' .. '9' )* )
					{
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:564:11: ( ( '1' .. '9' ) ( '0' .. '9' )* )
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:564:12: ( '1' .. '9' ) ( '0' .. '9' )*
					{
					if ( (input.LA(1) >= '1' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:564:22: ( '0' .. '9' )*
					loop1:
					while (true) {
						int alt1=2;
						int LA1_0 = input.LA(1);
						if ( ((LA1_0 >= '0' && LA1_0 <= '9')) ) {
							alt1=1;
						}

						switch (alt1) {
						case 1 :
							// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							break loop1;
						}
					}

					}

					}
					break;
				case 2 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:564:37: '0'
					{
					match('0'); 
					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INTEGER"

	// $ANTLR start "TRUE"
	public final void mTRUE() throws RecognitionException {
		try {
			int _type = TRUE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:565:6: ( 'true' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:565:8: 'true'
			{
			match("true"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TRUE"

	// $ANTLR start "FALSE"
	public final void mFALSE() throws RecognitionException {
		try {
			int _type = FALSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:566:7: ( 'false' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:566:9: 'false'
			{
			match("false"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FALSE"

	// $ANTLR start "LPAR"
	public final void mLPAR() throws RecognitionException {
		try {
			int _type = LPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:567:7: ( '(' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:567:9: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LPAR"

	// $ANTLR start "RPAR"
	public final void mRPAR() throws RecognitionException {
		try {
			int _type = RPAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:568:6: ( ')' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:568:8: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RPAR"

	// $ANTLR start "IF"
	public final void mIF() throws RecognitionException {
		try {
			int _type = IF;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:569:5: ( 'if' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:569:7: 'if'
			{
			match("if"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IF"

	// $ANTLR start "THEN"
	public final void mTHEN() throws RecognitionException {
		try {
			int _type = THEN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:570:7: ( 'then' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:570:9: 'then'
			{
			match("then"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "THEN"

	// $ANTLR start "ELSE"
	public final void mELSE() throws RecognitionException {
		try {
			int _type = ELSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:571:7: ( 'else' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:571:9: 'else'
			{
			match("else"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ELSE"

	// $ANTLR start "PRINT"
	public final void mPRINT() throws RecognitionException {
		try {
			int _type = PRINT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:572:7: ( 'print' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:572:9: 'print'
			{
			match("print"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PRINT"

	// $ANTLR start "LET"
	public final void mLET() throws RecognitionException {
		try {
			int _type = LET;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:573:5: ( 'let' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:573:7: 'let'
			{
			match("let"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LET"

	// $ANTLR start "IN"
	public final void mIN() throws RecognitionException {
		try {
			int _type = IN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:574:5: ( 'in' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:574:7: 'in'
			{
			match("in"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IN"

	// $ANTLR start "VAR"
	public final void mVAR() throws RecognitionException {
		try {
			int _type = VAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:575:5: ( 'var' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:575:7: 'var'
			{
			match("var"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "VAR"

	// $ANTLR start "FUN"
	public final void mFUN() throws RecognitionException {
		try {
			int _type = FUN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:576:5: ( 'fun' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:576:7: 'fun'
			{
			match("fun"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FUN"

	// $ANTLR start "INT"
	public final void mINT() throws RecognitionException {
		try {
			int _type = INT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:577:5: ( 'int' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:577:7: 'int'
			{
			match("int"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INT"

	// $ANTLR start "BOOL"
	public final void mBOOL() throws RecognitionException {
		try {
			int _type = BOOL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:578:7: ( 'bool' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:578:9: 'bool'
			{
			match("bool"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BOOL"

	// $ANTLR start "ID"
	public final void mID() throws RecognitionException {
		try {
			int _type = ID;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:580:5: ( ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )* )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:580:7: ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )*
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:580:27: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )*
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( ((LA3_0 >= '0' && LA3_0 <= '9')||(LA3_0 >= 'A' && LA3_0 <= 'Z')||(LA3_0 >= 'a' && LA3_0 <= 'z')) ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop3;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ID"

	// $ANTLR start "WHITESP"
	public final void mWHITESP() throws RecognitionException {
		try {
			int _type = WHITESP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:582:10: ( ( '\\t' | ' ' | '\\r' | '\\n' )+ )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:582:12: ( '\\t' | ' ' | '\\r' | '\\n' )+
			{
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:582:12: ( '\\t' | ' ' | '\\r' | '\\n' )+
			int cnt4=0;
			loop4:
			while (true) {
				int alt4=2;
				int LA4_0 = input.LA(1);
				if ( ((LA4_0 >= '\t' && LA4_0 <= '\n')||LA4_0=='\r'||LA4_0==' ') ) {
					alt4=1;
				}

				switch (alt4) {
				case 1 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:
					{
					if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt4 >= 1 ) break loop4;
					EarlyExitException eee = new EarlyExitException(4, input);
					throw eee;
				}
				cnt4++;
			}

			 _channel=HIDDEN; 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WHITESP"

	// $ANTLR start "COMMENT"
	public final void mCOMMENT() throws RecognitionException {
		try {
			int _type = COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:584:9: ( '/*' ( . )* '*/' )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:584:11: '/*' ( . )* '*/'
			{
			match("/*"); 

			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:584:16: ( . )*
			loop5:
			while (true) {
				int alt5=2;
				int LA5_0 = input.LA(1);
				if ( (LA5_0=='*') ) {
					int LA5_1 = input.LA(2);
					if ( (LA5_1=='/') ) {
						alt5=2;
					}
					else if ( ((LA5_1 >= '\u0000' && LA5_1 <= '.')||(LA5_1 >= '0' && LA5_1 <= '\uFFFF')) ) {
						alt5=1;
					}

				}
				else if ( ((LA5_0 >= '\u0000' && LA5_0 <= ')')||(LA5_0 >= '+' && LA5_0 <= '\uFFFF')) ) {
					alt5=1;
				}

				switch (alt5) {
				case 1 :
					// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:584:16: .
					{
					matchAny(); 
					}
					break;

				default :
					break loop5;
				}
			}

			match("*/"); 

			 _channel=HIDDEN; 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMENT"

	// $ANTLR start "ERR"
	public final void mERR() throws RecognitionException {
		try {
			int _type = ERR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:586:10: ( . )
			// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:586:12: .
			{
			matchAny(); 
			 System.err.println("Invalid char: "+getText()); lexicalErrors++; _channel=HIDDEN; 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ERR"

	@Override
	public void mTokens() throws RecognitionException {
		// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:8: ( PLUS | TIMES | MINUS | DIV | OR | AND | EQ | NOT | GE | LE | ARROW | DOT | CLPAR | CRPAR | CLASS | EXTENDS | NEW | NULL | COLON | COMMA | ASS | SEMIC | INTEGER | TRUE | FALSE | LPAR | RPAR | IF | THEN | ELSE | PRINT | LET | IN | VAR | FUN | INT | BOOL | ID | WHITESP | COMMENT | ERR )
		int alt6=41;
		alt6 = dfa6.predict(input);
		switch (alt6) {
			case 1 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:10: PLUS
				{
				mPLUS(); 

				}
				break;
			case 2 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:15: TIMES
				{
				mTIMES(); 

				}
				break;
			case 3 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:21: MINUS
				{
				mMINUS(); 

				}
				break;
			case 4 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:27: DIV
				{
				mDIV(); 

				}
				break;
			case 5 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:31: OR
				{
				mOR(); 

				}
				break;
			case 6 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:34: AND
				{
				mAND(); 

				}
				break;
			case 7 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:38: EQ
				{
				mEQ(); 

				}
				break;
			case 8 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:41: NOT
				{
				mNOT(); 

				}
				break;
			case 9 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:45: GE
				{
				mGE(); 

				}
				break;
			case 10 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:48: LE
				{
				mLE(); 

				}
				break;
			case 11 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:51: ARROW
				{
				mARROW(); 

				}
				break;
			case 12 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:57: DOT
				{
				mDOT(); 

				}
				break;
			case 13 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:61: CLPAR
				{
				mCLPAR(); 

				}
				break;
			case 14 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:67: CRPAR
				{
				mCRPAR(); 

				}
				break;
			case 15 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:73: CLASS
				{
				mCLASS(); 

				}
				break;
			case 16 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:79: EXTENDS
				{
				mEXTENDS(); 

				}
				break;
			case 17 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:87: NEW
				{
				mNEW(); 

				}
				break;
			case 18 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:91: NULL
				{
				mNULL(); 

				}
				break;
			case 19 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:96: COLON
				{
				mCOLON(); 

				}
				break;
			case 20 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:102: COMMA
				{
				mCOMMA(); 

				}
				break;
			case 21 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:108: ASS
				{
				mASS(); 

				}
				break;
			case 22 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:112: SEMIC
				{
				mSEMIC(); 

				}
				break;
			case 23 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:118: INTEGER
				{
				mINTEGER(); 

				}
				break;
			case 24 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:126: TRUE
				{
				mTRUE(); 

				}
				break;
			case 25 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:131: FALSE
				{
				mFALSE(); 

				}
				break;
			case 26 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:137: LPAR
				{
				mLPAR(); 

				}
				break;
			case 27 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:142: RPAR
				{
				mRPAR(); 

				}
				break;
			case 28 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:147: IF
				{
				mIF(); 

				}
				break;
			case 29 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:150: THEN
				{
				mTHEN(); 

				}
				break;
			case 30 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:155: ELSE
				{
				mELSE(); 

				}
				break;
			case 31 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:160: PRINT
				{
				mPRINT(); 

				}
				break;
			case 32 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:166: LET
				{
				mLET(); 

				}
				break;
			case 33 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:170: IN
				{
				mIN(); 

				}
				break;
			case 34 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:173: VAR
				{
				mVAR(); 

				}
				break;
			case 35 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:177: FUN
				{
				mFUN(); 

				}
				break;
			case 36 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:181: INT
				{
				mINT(); 

				}
				break;
			case 37 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:185: BOOL
				{
				mBOOL(); 

				}
				break;
			case 38 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:190: ID
				{
				mID(); 

				}
				break;
			case 39 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:193: WHITESP
				{
				mWHITESP(); 

				}
				break;
			case 40 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:201: COMMENT
				{
				mCOMMENT(); 

				}
				break;
			case 41 :
				// C:\\Users\\Andrea\\git\\fool\\FOOL\\FOOL.g:1:209: ERR
				{
				mERR(); 

				}
				break;

		}
	}


	protected DFA6 dfa6 = new DFA6(this);
	static final String DFA6_eotS =
		"\3\uffff\1\44\1\46\2\40\1\52\1\56\2\40\3\uffff\2\56\5\uffff\2\56\2\uffff"+
		"\5\56\15\uffff\3\56\6\uffff\3\56\4\uffff\4\56\2\uffff\1\122\1\124\4\56"+
		"\1\uffff\1\131\1\132\7\56\1\142\1\uffff\1\143\1\uffff\1\56\1\145\1\146"+
		"\1\56\2\uffff\1\150\2\56\1\153\1\154\1\155\1\56\2\uffff\1\56\2\uffff\1"+
		"\160\1\uffff\1\161\1\56\3\uffff\1\163\1\164\2\uffff\1\56\2\uffff\1\166"+
		"\1\uffff";
	static final String DFA6_eofS =
		"\167\uffff";
	static final String DFA6_minS =
		"\1\0\2\uffff\1\76\1\52\1\174\1\46\1\75\1\145\2\75\3\uffff\2\154\5\uffff"+
		"\1\150\1\141\2\uffff\1\146\1\162\1\145\1\141\1\157\15\uffff\1\164\1\167"+
		"\1\154\6\uffff\1\141\1\164\1\163\4\uffff\1\165\1\145\1\154\1\156\2\uffff"+
		"\2\60\1\151\1\164\1\162\1\157\1\uffff\2\60\1\154\1\163\3\145\1\156\1\163"+
		"\1\60\1\uffff\1\60\1\uffff\1\156\2\60\1\154\2\uffff\1\60\1\163\1\156\3"+
		"\60\1\145\2\uffff\1\164\2\uffff\1\60\1\uffff\1\60\1\144\3\uffff\2\60\2"+
		"\uffff\1\163\2\uffff\1\60\1\uffff";
	static final String DFA6_maxS =
		"\1\uffff\2\uffff\1\76\1\52\1\174\1\46\1\75\1\165\2\75\3\uffff\1\154\1"+
		"\170\5\uffff\1\162\1\165\2\uffff\1\156\1\162\1\145\1\141\1\157\15\uffff"+
		"\1\164\1\167\1\154\6\uffff\1\141\1\164\1\163\4\uffff\1\165\1\145\1\154"+
		"\1\156\2\uffff\2\172\1\151\1\164\1\162\1\157\1\uffff\2\172\1\154\1\163"+
		"\3\145\1\156\1\163\1\172\1\uffff\1\172\1\uffff\1\156\2\172\1\154\2\uffff"+
		"\1\172\1\163\1\156\3\172\1\145\2\uffff\1\164\2\uffff\1\172\1\uffff\1\172"+
		"\1\144\3\uffff\2\172\2\uffff\1\163\2\uffff\1\172\1\uffff";
	static final String DFA6_acceptS =
		"\1\uffff\1\1\1\2\10\uffff\1\14\1\15\1\16\2\uffff\1\23\1\24\1\26\2\27\2"+
		"\uffff\1\32\1\33\5\uffff\1\46\1\47\1\51\1\1\1\2\1\13\1\3\1\50\1\4\1\5"+
		"\1\6\1\7\1\25\3\uffff\1\46\1\11\1\12\1\14\1\15\1\16\3\uffff\1\23\1\24"+
		"\1\26\1\27\4\uffff\1\32\1\33\6\uffff\1\47\12\uffff\1\34\1\uffff\1\41\4"+
		"\uffff\1\10\1\21\7\uffff\1\43\1\44\1\uffff\1\40\1\42\1\uffff\1\22\2\uffff"+
		"\1\36\1\30\1\35\2\uffff\1\45\1\17\1\uffff\1\31\1\37\1\uffff\1\20";
	static final String DFA6_specialS =
		"\1\0\166\uffff}>";
	static final String[] DFA6_transitionS = {
			"\11\40\2\37\2\40\1\37\22\40\1\37\5\40\1\6\1\40\1\27\1\30\1\2\1\1\1\21"+
			"\1\3\1\13\1\4\1\24\11\23\1\20\1\22\1\12\1\7\1\11\2\40\32\36\6\40\1\36"+
			"\1\35\1\16\1\36\1\17\1\26\2\36\1\31\2\36\1\33\1\36\1\10\1\36\1\32\3\36"+
			"\1\25\1\36\1\34\4\36\1\14\1\5\1\15\uff82\40",
			"",
			"",
			"\1\43",
			"\1\45",
			"\1\47",
			"\1\50",
			"\1\51",
			"\1\54\11\uffff\1\53\5\uffff\1\55",
			"\1\57",
			"\1\60",
			"",
			"",
			"",
			"\1\64",
			"\1\66\13\uffff\1\65",
			"",
			"",
			"",
			"",
			"",
			"\1\74\11\uffff\1\73",
			"\1\75\23\uffff\1\76",
			"",
			"",
			"\1\101\7\uffff\1\102",
			"\1\103",
			"\1\104",
			"\1\105",
			"\1\106",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\110",
			"\1\111",
			"\1\112",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\113",
			"\1\114",
			"\1\115",
			"",
			"",
			"",
			"",
			"\1\116",
			"\1\117",
			"\1\120",
			"\1\121",
			"",
			"",
			"\12\56\7\uffff\32\56\6\uffff\32\56",
			"\12\56\7\uffff\32\56\6\uffff\23\56\1\123\6\56",
			"\1\125",
			"\1\126",
			"\1\127",
			"\1\130",
			"",
			"\12\56\7\uffff\32\56\6\uffff\32\56",
			"\12\56\7\uffff\32\56\6\uffff\32\56",
			"\1\133",
			"\1\134",
			"\1\135",
			"\1\136",
			"\1\137",
			"\1\140",
			"\1\141",
			"\12\56\7\uffff\32\56\6\uffff\32\56",
			"",
			"\12\56\7\uffff\32\56\6\uffff\32\56",
			"",
			"\1\144",
			"\12\56\7\uffff\32\56\6\uffff\32\56",
			"\12\56\7\uffff\32\56\6\uffff\32\56",
			"\1\147",
			"",
			"",
			"\12\56\7\uffff\32\56\6\uffff\32\56",
			"\1\151",
			"\1\152",
			"\12\56\7\uffff\32\56\6\uffff\32\56",
			"\12\56\7\uffff\32\56\6\uffff\32\56",
			"\12\56\7\uffff\32\56\6\uffff\32\56",
			"\1\156",
			"",
			"",
			"\1\157",
			"",
			"",
			"\12\56\7\uffff\32\56\6\uffff\32\56",
			"",
			"\12\56\7\uffff\32\56\6\uffff\32\56",
			"\1\162",
			"",
			"",
			"",
			"\12\56\7\uffff\32\56\6\uffff\32\56",
			"\12\56\7\uffff\32\56\6\uffff\32\56",
			"",
			"",
			"\1\165",
			"",
			"",
			"\12\56\7\uffff\32\56\6\uffff\32\56",
			""
	};

	static final short[] DFA6_eot = DFA.unpackEncodedString(DFA6_eotS);
	static final short[] DFA6_eof = DFA.unpackEncodedString(DFA6_eofS);
	static final char[] DFA6_min = DFA.unpackEncodedStringToUnsignedChars(DFA6_minS);
	static final char[] DFA6_max = DFA.unpackEncodedStringToUnsignedChars(DFA6_maxS);
	static final short[] DFA6_accept = DFA.unpackEncodedString(DFA6_acceptS);
	static final short[] DFA6_special = DFA.unpackEncodedString(DFA6_specialS);
	static final short[][] DFA6_transition;

	static {
		int numStates = DFA6_transitionS.length;
		DFA6_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA6_transition[i] = DFA.unpackEncodedString(DFA6_transitionS[i]);
		}
	}

	protected class DFA6 extends DFA {

		public DFA6(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 6;
			this.eot = DFA6_eot;
			this.eof = DFA6_eof;
			this.min = DFA6_min;
			this.max = DFA6_max;
			this.accept = DFA6_accept;
			this.special = DFA6_special;
			this.transition = DFA6_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( PLUS | TIMES | MINUS | DIV | OR | AND | EQ | NOT | GE | LE | ARROW | DOT | CLPAR | CRPAR | CLASS | EXTENDS | NEW | NULL | COLON | COMMA | ASS | SEMIC | INTEGER | TRUE | FALSE | LPAR | RPAR | IF | THEN | ELSE | PRINT | LET | IN | VAR | FUN | INT | BOOL | ID | WHITESP | COMMENT | ERR );";
		}
		@Override
		public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
			IntStream input = _input;
			int _s = s;
			switch ( s ) {
					case 0 : 
						int LA6_0 = input.LA(1);
						s = -1;
						if ( (LA6_0=='+') ) {s = 1;}
						else if ( (LA6_0=='*') ) {s = 2;}
						else if ( (LA6_0=='-') ) {s = 3;}
						else if ( (LA6_0=='/') ) {s = 4;}
						else if ( (LA6_0=='|') ) {s = 5;}
						else if ( (LA6_0=='&') ) {s = 6;}
						else if ( (LA6_0=='=') ) {s = 7;}
						else if ( (LA6_0=='n') ) {s = 8;}
						else if ( (LA6_0=='>') ) {s = 9;}
						else if ( (LA6_0=='<') ) {s = 10;}
						else if ( (LA6_0=='.') ) {s = 11;}
						else if ( (LA6_0=='{') ) {s = 12;}
						else if ( (LA6_0=='}') ) {s = 13;}
						else if ( (LA6_0=='c') ) {s = 14;}
						else if ( (LA6_0=='e') ) {s = 15;}
						else if ( (LA6_0==':') ) {s = 16;}
						else if ( (LA6_0==',') ) {s = 17;}
						else if ( (LA6_0==';') ) {s = 18;}
						else if ( ((LA6_0 >= '1' && LA6_0 <= '9')) ) {s = 19;}
						else if ( (LA6_0=='0') ) {s = 20;}
						else if ( (LA6_0=='t') ) {s = 21;}
						else if ( (LA6_0=='f') ) {s = 22;}
						else if ( (LA6_0=='(') ) {s = 23;}
						else if ( (LA6_0==')') ) {s = 24;}
						else if ( (LA6_0=='i') ) {s = 25;}
						else if ( (LA6_0=='p') ) {s = 26;}
						else if ( (LA6_0=='l') ) {s = 27;}
						else if ( (LA6_0=='v') ) {s = 28;}
						else if ( (LA6_0=='b') ) {s = 29;}
						else if ( ((LA6_0 >= 'A' && LA6_0 <= 'Z')||LA6_0=='a'||LA6_0=='d'||(LA6_0 >= 'g' && LA6_0 <= 'h')||(LA6_0 >= 'j' && LA6_0 <= 'k')||LA6_0=='m'||LA6_0=='o'||(LA6_0 >= 'q' && LA6_0 <= 's')||LA6_0=='u'||(LA6_0 >= 'w' && LA6_0 <= 'z')) ) {s = 30;}
						else if ( ((LA6_0 >= '\t' && LA6_0 <= '\n')||LA6_0=='\r'||LA6_0==' ') ) {s = 31;}
						else if ( ((LA6_0 >= '\u0000' && LA6_0 <= '\b')||(LA6_0 >= '\u000B' && LA6_0 <= '\f')||(LA6_0 >= '\u000E' && LA6_0 <= '\u001F')||(LA6_0 >= '!' && LA6_0 <= '%')||LA6_0=='\''||(LA6_0 >= '?' && LA6_0 <= '@')||(LA6_0 >= '[' && LA6_0 <= '`')||(LA6_0 >= '~' && LA6_0 <= '\uFFFF')) ) {s = 32;}
						if ( s>=0 ) return s;
						break;
			}
			NoViableAltException nvae =
				new NoViableAltException(getDescription(), 6, _s, input);
			error(nvae);
			throw nvae;
		}
	}

}
