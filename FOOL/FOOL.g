grammar FOOL;

@header {
import ast.*;
import java.util.ArrayList;
import java.util.HashMap;
import lib.*;
import st.*;
import ct.*;
import interfaces.*;
}

@lexer::members {
int lexicalErrors=0;
}

//Membri del parser
@members{
/* 
Inizialmente l'ambiente esterno ha nestinglevel = 0, quindi siccome incremento, lo setto a -1
all'ingresso di ogni scope il nesting level viene incrementato quando entro nel primo scope 
passa a 0 il che � il livello dell'ambiente globale, qui sono dichiarate anche le classi (i loro nomi)
le virtual table (symbol table per i componenti delle classi) sono a nesting level 1
*/
private int nestingLevel = -1;

/*
String � il nome e STentry � il valore
Ogni volta che entraimo in uno scope, visitiamo un ambiente, aggiungiamo una hashmap, che avr� posizione coincidente con il nesting level, quindi il frontedell'array � la tabella relativa 
all'ambiente in cui sono, quindi al livello di nesting level
*/
private ArrayList<HashMap<String, STentry>> symTable = new  ArrayList<HashMap<String, STentry>>();

// la class table mappa il nome della classe alla CTEntry che contiene varie cose tra cui pure la virtual table
private HashMap<String, CTentry> classTable = new HashMap<String, CTentry>();

// mappa ID di classi in ID di classi super, mi servir� poi per il sub-typing
private HashMap<String, String> superType = new HashMap<String,String>();
}


/*------------------------------------------------------------------
 * PARSER RULES
 *------------------------------------------------------------------*/

prog	returns [Node ast]
	: e=exp SEMIC	{$ast = new ProgNode($e.ast);}//Caso in cui non ho dichiarazioni, quindi un programma semplice          
  | LET   {
              // all'inizio del parsing passo il riferimento della map supertype a FOOLLib per poterlo utilizzare nel typechecking
              FOOLlib.setSuperTypeMap(superType);
              nestingLevel++;//Inizialmente voglio che sia la prima hashmap in livello 0
              HashMap<String, STentry> hm = new HashMap<String, STentry>();
              symTable.add(hm);
           }//Prima di aggiungere cose, qua entro in un ambiente quindi creo una hashmap e incremento il nesting level
           
          // posso quindi incontrare una lista di classi     
          c=cllist
          
          // seguita da una lista di dichiarazoni (variabili/funzioni)
          d=declist 
          
          IN 
          
          //Infine una espressione
          e=exp SEMIC { symTable.remove(nestingLevel--);//Sto uscendo dallo scope allora rimuovo il fronte e decremento il nesting-level
                        $ast = new ProgLetInNode($c.astList, $d.astList, $e.ast);
                       }//non gli passo solo exp ma anche declist che sar� una foresta di nodi (parseTree), cio� tutte le vecchie dichiarazioni fatte in precedenza
                        //Stessa cosa per cllist, gli passo tutte le vecchie dichiarazioni di classi fatte in principio.
	;

/* 
gestione della dichiarazione delle classi per semplicit� le classi 
sono contenute solamente nell'ambiente globale (nesting level 0)
*/
cllist returns [ArrayList<Node> astList] 
  : {$astList = new ArrayList<Node>();}// inizializzo la lista di classi che verr� restituita a prog
  
  (CLASS i=ID {     
      // la symbol table di livello 0 include STEntry per i nomi delle classi
      // questo per controllare che nello stesso scope non vengano dichiarate altre cose
      // con lo stesso nome
    
      // l'offset della entry della dichiarazoine della classe non verr� mai utilizzato in quanto
      // la dichiarazione non viene messa in memoria (stack o heap), quindi mettiamo un numero fittizio    
      HashMap<String,STentry> hm = symTable.get(nestingLevel);
      if(hm.put($i.text,new STentry(nestingLevel, null, 3333))!=null){ //Vado ad inserire a nstlv 0 il nome della classe, qu� controllo che la classe non sia gi� dichiarata nella lista a lv.0
        System.out.println("Error: id "+$i.text +" at line "+ $i.line +" already declared!");
        System.exit(0);
      }
    
      // creo la CTEntry che conterr�le info della classe
      CTentry classEntry = new CTentry();
      //Non ho superclassi per ora
      CTentry superClassEntry = null;
    }
     
   (EXTENDS ei=ID {
        // se finisco qua dentro significa che estendo da qualcuno, allora devo recuperare la CTEntry
        // dalla class table relativa alla super classe (controllando che esista) e utilizzare il II costruttore di CTEntry
        // quella che copia di netto la super-CTentry
        superClassEntry = classTable.get($ei.text);
        if(superClassEntry == null){
           System.out.println("Error: class "+$ei.text +" at line "+ $ei.line +" not declared!");
           System.exit(0);
        }
        // Copio la CTEntry della superclass, andando a ridefinire la vecchia versione
        classEntry = new CTentry(superClassEntry);
      
        //aggiorno la superType map tenendo conto di: chi eredita da chi
        superType.put($i.text, $ei.text);   
     }
   )?  //Questo blocco (extends pu� esserci come non esserci)
   
   //DEFINIAMO IL CONTENUTO DELL'OGGETTO:
   {      //Lista di campi vuota
          ArrayList<Node> fieldsList = new ArrayList<Node>();         
          //Lista di metodi vuota
          ArrayList<Node> methodsList = new ArrayList<Node>();
          
          // creo un nodo di tipo classe contenente il nome della classe
          ClassTypeNode classType = new ClassTypeNode($i.text);
          
          // creo un nodo classe a cui passo le intestazioni della classe
          ClassNode c = new ClassNode(classType, fieldsList, methodsList, classEntry, superClassEntry);
          
          // lo aggiungo alla lista delle classi (lista che verr�infine restituita)
          $astList.add(c);
    
          // Inserisco l'entry anche nella class table (il controllo che non sia un nome gi� usato � stato fatto prima tramite la symbol table).
          classTable.put($i.text, classEntry);
    
          
          //viene creato un nuovo livello e la relativa Symbol Table (anzich� creata vuota):
          //viene settata alla Virtual Table contenuta dentro la nuova CTentry (quindi gli metto dentro tutti i metodi/campi dell'oggetto)
          //incremento perch� la virtual table � sempre ad offset 1
          //in questo modo alla fine riesco a rimuoverla correttamente
          //i campi e metodi vengono settati a nesting level 1 di default (senza doverglielo passare, vedi addMethod e addField in CTEntry)
          nestingLevel++; 
          symTable.add(classEntry.getVirtualTable());
   }
    
    //Ora scorro gli elementi della classe e aggiorno la CTEntry ad ogni campo/metodo incontrato:
   
    /* 
     Iniziamo con il costruttore, che contiene i campi della classe che devono essere inizializzati
     alla creazione degli oggetti in quanto immutabili
    */
    //Primo campo inserito
    LPAR ( ffid=ID COLON fft=basic {// lo aggiungo alla CTEntry e alla classe
           // addField ritorna il nodo field che vado ad aggiungere alla lista dei campi definiti in questa classe
           // FieldList � la lista di campi della classe, che ho gi� creato sopra, la vado a popolare (sia quella che la CTentry)
           fieldsList.add(classEntry.addField($ffid.text,$fft.ast));
         }
      
         // mi occupo degli altri eventuali campi     
         (COMMA ofid=ID COLON oft=basic {fieldsList.add(classEntry.addField($ofid.text,$oft.ast));})* 
        )? 
    RPAR    
    
    // inizia la dichiarazione e definizione dei metodi
    CLPAR (
      FUN mid=ID COLON mt=basic{

       // creo il nodo dell'AST
       MethodNode method = new MethodNode($mid.text, $mt.ast);
       
       // aggiungo il metodo alla lista dei metodi
       methodsList.add(method);
       
       // memorizzo il tipo dei parametri il quale andr� insieme al tipo di ritorno a comporre il tipo complessivo del metodo
       ArrayList<Node> parTypes = new ArrayList<Node>();
        
       // creo la symbol table che rappresenta il contesto del metodo
       nestingLevel++;//Entro dentro lo scope relativo al metodo
       HashMap<String, STentry> hmn = new HashMap<String,STentry>();
       symTable.add(hmn);
       
       // qua mi salvo le dichiarazioni di variabili (per comodit� non permettiamo parametri funzionali)
       ArrayList<Node> varList = new ArrayList<Node>();
       
       // per quanto riguarda il layout dei metodi devo rifarmi a quello delle funzioni: i parametri iniziano dall'offset 1 e vado ad incremento
       int parOffset = 1;
       
       // le dichiarazioni da -2 e decremento
       int varOffset = -2;
       
       // NOTA: a 0 ho l'AL e a -1 il RA
       }
      
       // lavoro sui parametri aggiungengoli al metodo e alla symbol table man mano che li incontro
       LPAR 
         (
           // primo parametro
           mpfid=ID COLON mpft=type {
             ParNode firstPar = new ParNode($mpfid.text,$mpft.ast);
             method.addParameter(firstPar);
             parTypes.add($mpft.ast);
                        
             // verifico eventuali duplicati e aggiungo alla symbol table (primo par) 
             // NOTA: i parametri sono allo stesso livello del corpo della fun
             if(hmn.put($mpfid.text,new STentry(nestingLevel, $mpft.ast, parOffset++))!=null){ 
               System.out.println("Error: id "+$mpfid.text +" at line "+ $mpfid.line +" already declared!");
               System.exit(0);
             };     
           }
           // parametri successivi
           (COMMA mpnid=ID COLON mtnt=type {
              ParNode nextPat = new ParNode($mpnid.text,$mtnt.ast);
              method.addParameter(nextPat);
              parTypes.add($mtnt.ast);
              
              if(hmn.put($mpnid.text,new STentry(nestingLevel,$mtnt.ast, parOffset++))!=null){ 
                System.out.println("Error: id "+$mpnid.text +" at line "+ $mpnid.line +" already declared!");
                System.exit(0);
              };  
            })* )?        
      RPAR
      // ora ci sono le dichiarazioni (che non possono essere ulteriormente annidate) e potrebbero anche non esserci
      (LET
        // scorro tutte le variabili
        ( VAR vid=ID COLON vty=basic ASS vexp=exp SEMIC {
             VarNode v = new VarNode($vid.text, $vty.ast, $vexp.ast);
             varList.add(v);
             
             // verifico eventuali duplicati
             if(hmn.put($vid.text,new STentry(nestingLevel,$vty.ast, varOffset--))!=null){ 
                System.out.println("Error: id "+$vty.text +" at line "+ $vid.line +" already declared!");
                System.exit(0);
             };
          })*
        // devo aggiungere le dichiarazioni al metodo
        {method.addDec(varList);}      
      IN)? 
      
      { 
        // istanzio il nodo che rappresenta il tipo del metodo (tipi dei parametri, type del metodo)
        ArrowTypeNode methodType = new ArrowTypeNode(parTypes, $mt.ast);
        
        // aggiungo il tipo complessivo al MethodNode
        method.addSymType(methodType);  
        
        // Aggiungo il metodo alla virtual table e ad allMethods
        // NOTA: � necessario farlo prima di processare l'exp nel caso in cui il metodo richiami se stesso
        classEntry.addMethod($mid.text, method);
      }
      
      //Aggiungo il body al metodo
      mexp=exp {method.addBody($mexp.ast);}
      
      // � finito lo scope del metodo quindo posso rimuovere la symbol table corrispondente e decrementare il nestinglevel, quindi elimino il contesto del metodo
      SEMIC {symTable.remove(nestingLevel--);}      
    )* 
      
  // Ora devo ricordarmi di chiudere il livello interno della classe (livello virtual table) torno a lv.0
  //A livello zero della symTable ricordo che ho tutti i nomi delle classi, con relativa STEntry, in questo modo le tengo in vita              
  CRPAR{symTable.remove(nestingLevel--);}
      
// potrebbero esserci altre classi quindi mettiamo * per ripetere se necessario 
)*;  	
	 	
// gestione di dichiarazioni di variabili e funzioni
declist returns [ArrayList<Node> astList] : {
      // creo l'arraylist vuoto, esso conterr� le dichiarazioni
      $astList = new ArrayList<Node>();
      
      // l'offset mi serve per recuperare le cose in fase di esecuzione
      // inizializzo l'offset a -2 perch� nel caso di AR dell'ambiente globale a -1 abbiamo il RA fittizio
      // mentre nel caso di layout AR funzione, a 0 c'� l'AL e a -1 il RA vero
      int offset = -2;
    }
    
    ( 
      // le dichiarazioni possono essere di variabili o funzioni
    ( 
       // DICHIARAZIONE DI VARIABILE
       VAR i=ID COLON t=type ASS e=exp
        {
          VarNode v = new VarNode($i.text, $t.ast, $e.ast);
          $astList.add(v);
          // ora che ho dichiarato la var la aggiungo alla symbol table
          // recupero l'hash table dell'ambiente dove sto parsando
          HashMap<String,STentry> hm = symTable.get(nestingLevel);
          // controllo che niente sia dichiarato con lo stesso nome
          if(hm.put($i.text,new STentry(nestingLevel, $t.ast, offset--))!=null){ 
            System.out.println("Error: id "+$i.text +" at line "+ $i.line +" already declared!");
            System.exit(0);
            };
         }
       |
       // DICHIARAZIONE DI FUNZIONE                          
       FUN i=ID COLON t=type {
          FunNode f = new FunNode($i.text, $t.ast); 
          $astList.add(f);  
          HashMap<String,STentry> hm = symTable.get(nestingLevel);
          
         // creo una entry con solo il nesting level e l'offset
         // ci metter� il tipo quando lo sapr� (lo trover� solo dopo aver letto il tipo di tutti i parametri)
         STentry entry = new STentry(nestingLevel, offset);
         
         // la funzione occupa due offset (vedi layout high order)
         offset -= 2;
         
         // inserisco l'ID della funzione nella symbol table                                               
         if(hm.put($i.text, entry)!=null){
          System.out.println("Error: id "+$i.text +" at line "+ $i.line +" already declared!");
          System.exit(0);
         };
         
         // i parametri assumiamo facciano parte del corpo della funzione
         // creo una hashmap che rappresenta il contesto interno alla funzione incrementando allora il nestingLvl.
         nestingLevel++; 
         HashMap<String,STentry> hmn = new HashMap<String,STentry>();
         symTable.add(hmn);
         
         // creo un array list per mantenere il tipo dei parametri             
         ArrayList<Node> parTypes = new ArrayList<Node>();
         int parOffset = 1; //i parametri iniziano da 1 nel layout e l'offset si incrementa (quindi da 1 in su)
         }
         LPAR 
         
         // dichiarazione dei parametri
          (fid=ID COLON fty=type 
            { 
              parTypes.add($fty.ast);
              ParNode fpar = new ParNode($fid.text, $fty.ast);
              f.addParameter(fpar);
              
              // nel caso in cui sia presente qualche parametro di tipo funzionale devo riservare due spazi. 
              if($fty.ast instanceof ArrowTypeNode){
                parOffset++;
              }
              
              if(hmn.put($fid.text,new STentry(nestingLevel, $fty.ast, parOffset++))!=null){
                System.out.println("Error: id "+$fid.text +" at line "+ $fid.line +" already declared!");
                System.exit(0);
              };
              
             }                                
          
             (COMMA id=ID COLON ty=type 
              { 
                parTypes.add($ty.ast);
                ParNode par = new ParNode($id.text, $ty.ast);
                f.addParameter(par);
                if($ty.ast instanceof ArrowTypeNode){parOffset++;}
                if(hmn.put($id.text,new STentry(nestingLevel,$ty.ast, parOffset++))!=null){
                  System.out.println("Error: id "+$id.text +" at line "+ $id.line +" already declared!");
                  System.exit(0);
                };
               
               }
              )*  //potrebbero esserci pi� parametri
              )?  //potrebbero non esserci affatto parametri
         RPAR 
          {// ora posso istanziare il nodo che rappresenta il tipo della funzione
            ArrowTypeNode functionType = new ArrowTypeNode(parTypes, $t.ast);
            entry.addType(functionType);
            // aggiungo il tipo anche al FunNode
            f.addSymType(functionType);
          }
         (LET d=declist IN{f.addDeclarations($d.astList);})? e=exp 
          {//chiudo lo scope
            symTable.remove(nestingLevel--);
            f.addBody($e.ast);
          }
         )SEMIC
        )+
        ;   
        	 
        	 
        	 	
exp returns [Node ast] :
  v=term {$ast = $v.ast;}
        (
           PLUS l = term {$ast = new PlusNode($ast,$l.ast);}
         | MINUS l = term {$ast = new MinusNode($ast,$l.ast);}
         | OR  l = term {$ast = new OrNode($ast,$l.ast);}
        )* 
 	;
 	
 	
 	
term	returns [Node ast]    
	: f=factor {$ast= $f.ast;}
	    (
	       TIMES l = factor {$ast= new MultNode ($ast,$l.ast);}
	     | DIV  l = factor {$ast = new DivNode($ast,$l.ast);}
	     | AND  l = factor {$ast = new AndNode($ast,$l.ast);}
	    )*
	;
	
	
	
factor	returns [Node ast]  
	: f=value {$ast= $f.ast;}
	    (
	       EQ l = value {$ast = new EqualNode ($ast,$l.ast);}
	     | GE v = value {$ast = new GreaterEqualNode($ast,$v.ast);}
       | LE v = value {$ast = new LowerEqualNode($ast,$v.ast);}
	    )*
 	;	 	
 	
 	
 	          	
value	returns [Node ast]
	: n=INTEGER {$ast= new IntNode(Integer.parseInt($n.text));}  
	
	| TRUE {$ast= new BoolNode(true);}  
	
	| FALSE {$ast= new BoolNode(false);}  
	
	| NULL {$ast = new EmptyNode();}
	
	| LPAR e=exp RPAR {$ast= $e.ast;}  
	
	| IF x=exp THEN CLPAR y=exp CRPAR ELSE CLPAR z=exp CRPAR {$ast= new IfNode($x.ast,$y.ast,$z.ast);}	 
	
	| PRINT LPAR e=exp RPAR	{$ast= new PrintNode($e.ast);} 	
	  
	| NOT LPAR e=exp {$ast = new NotNode($e.ast);} RPAR 
	 
	| NEW nid=ID LPAR{
          // creo la lista dei parametri che contengono le espressioni passate alla new
          ArrayList<Node> parList = new ArrayList<Node>();
          
          // recupero la CTEntry che descrive la classe
          CTentry classEntry = classTable.get($nid.text);
          
          // verifico che sia effettivamente definita
          if(classEntry == null){
            System.out.println("Error: class "+$nid.text+" at line " +$nid.line+ " not declared!");
            System.exit(0);
          }
          
          // creo il new node
          NewNode nn = new NewNode($nid.text, classEntry, parList);
        }
        // aggiungo i parametri man mano che li incotro, se ci sono
        (nfe=exp {parList.add($nfe.ast);} (COMMA nne=exp {parList.add($nne.ast);})* )? 
         
        RPAR{$ast = nn;}// restituisco il new Node appena creato
	 
	| i=ID//Questa � una foglia devo allora attacargli il valore relativo che ho nella tabella... se c'�!
	  { //Cerco la dichiarazione (pallina) da attaccare alla foglia dell'albero
	    int j=nestingLevel;
	    STentry entry=null; //Qu� metter� la mia pallina se la trovo
	    while(j>=0 && entry == null)//Qu� scorro tutte le tabelle fino a che non lo trovo o che arrivo alla fine della lista di tabelle
	      entry = symTable.get(j--).get($i.text); //Qu� ottengo la hashmap e vado a controllare che ci sia l'id che cerco
	    
	    if(entry==null){//Se anche dopo il ciclo while � null allora non esiste
	      System.out.println("Id: " + $i.text + " at line: " + $i.line + " not declared!"); 
        System.exit(0);
      }
	    
	    //Se arriviamo qu� invece entry � diverso da null, quindi abbiamo trovato il valore (pallina) da attaccare, quindi idnode ha un altro figlio 
	    //Quindi gli metto il nome, la pallina con il valore, nestinglevel
	    $ast= new IdNode($i.text, entry, nestingLevel);}
	    
	   // se vado oltre da questo punto posso avere allora: chiamata di funzione x() o invocazione metodo x.m()
	    
	    /*----CHIAMATA A FUNZIONE----*/
	    
	   //Chiamata a funzione con i parametri, ()? un id da solo, senza le parentesi, possono non esserci
	   //se ho le parentesi dentro opzionalemnte posso avere uno o pi� parametri.
	   //Idealemnte � che se ho un id da solo mi creo un id node, se invece ho una chiamata a funzione effettiva, mi creo un callNode
	   //Nota che i parametri sono delle exp quindi possono essere anche delle espressioni, calcoli eccetera, non solo dei semplici numeri
	   ( LPAR {ArrayList<Node> argList = new ArrayList<Node>();}//Creo la lista di argomenti
	       (fa=exp {argList.add($fa.ast);} (COMMA a=exp {argList.add($a.ast);})* )? 
	     RPAR //Aggiungo l'albero sitattico dei vari parametri mano a mano che li trovo
	       {$ast= new CallNode($i.text, entry, argList, nestingLevel);}//Arglist conterr� le varie 'fa' e 'a' first argument e argument, qua se prima era idnode viene ovviamente sostituito
	   
	   /*----INVOCAZIONE METODO----*/
	   | DOT mid=ID{
         // la variabile che punta all'oggetto � dichiarata nello stack
         // l'id dell'oggetto della classe � catturato da $i settato sopra
              
         // Verifico se classe e metodo esistono
              
         // ricavo il tipo dell'oggetto
         ClassTypeNode classType = (ClassTypeNode)entry.getType();
         
         // cerco nella class table la classe
         CTentry classEntry = classTable.get(classType.getType());
         
         if(classEntry == null){
            System.out.println("Error: class "+classType.getType()+" at line " +$i.line+ " not declared!");
            System.exit(0);
         } 
         
         // cerco il metodo nella virtual table relativa all'oggetto in esame
         STentry methodEntry = classEntry.getVirtualTable().get($mid.text);
         if(methodEntry == null){
           System.out.println("Error: method "+$mid.text+" at line " +$mid.line+ " not defined!");
           System.exit(0);
         }
    }
    
    // parametri del metodo
    { // creo una lista di parametri
      ArrayList<Node> parList = new ArrayList<Node>();
    }
    
    LPAR (fe = exp {parList.add($fe.ast);} (COMMA ne=exp {parList.add($ne.ast);})* )? RPAR 
    {// creo il nodo che verr� restituito
     $ast = new ClassCallNode($i.text, entry, $mid.text, methodEntry, parList, nestingLevel);
    }
)?; 


type  returns [Node ast]  : 
         b = basic {$ast = $b.ast;} 
       | a = arrow {$ast = $a.ast;} 
       ;
        
// IntNode � un valore intero
// IntTypeNode rappresenta la dichiarazione di una variabile di tipo int => non ha nessun parametro
basic  returns [Node ast] : 
    INT {$ast = new IntTypeNode();}   
  | BOOL {$ast = new BoolTypeNode();}    
  | i=ID {$ast = new ClassTypeNode($i.text);}                                   
  ;  
  
  
 arrow  returns [Node ast] : 
  {
    //lista dei parametri
    ArrayList<Node> parList = new ArrayList<Node>();
  }
  LPAR (t=type {parList.add($t.ast);} 
  (COMMA t=type {parList.add($t.ast);})* )? 
  RPAR ARROW b=basic {$ast = new ArrowTypeNode(parList,$b.ast);}// il tipo della funzione da ritornare
  ; 
  
  		
/*------------------------------------------------------------------
 * LEXER RULES
 *------------------------------------------------------------------*/

PLUS	: '+' ;
TIMES	: '*' ;
MINUS : '-' ;
DIV   : '/' ;

OR  : '||';
AND : '&&';
EQ  : '==' ;
NOT : 'not' ;
GE  : '>=' ;
LE  : '<=' ;

ARROW   : '->' ;  
DOT : '.' ;
CLPAR : '{' ;
CRPAR : '}' ;
CLASS : 'class' ; 
EXTENDS : 'extends' ; 
NEW   : 'new' ; 
NULL    : 'null' ; 

COLON : ':' ;
COMMA : ',' ;
ASS : '=' ;
SEMIC	: ';' ;
INTEGER	: (('1'..'9')('0'..'9')*) | '0' ;   
TRUE	: 'true' ;
FALSE	: 'false' ;
LPAR 	: '(' ;
RPAR	: ')' ;
IF 	: 'if' ;
THEN 	: 'then' ;
ELSE 	: 'else' ;
PRINT	: 'print' ; 
LET : 'let' ;
IN  : 'in' ;
VAR : 'var' ;
FUN : 'fun' ;
INT : 'int' ;
BOOL  : 'bool' ;

ID  : ('a'..'z'|'A'..'Z') ('a'..'z'|'A'..'Z'|'0'..'9')* ;

WHITESP  : ( '\t' | ' ' | '\r' | '\n' )+    { $channel=HIDDEN; } ;

COMMENT : '/*' .* '*/' { $channel=HIDDEN; } ;

ERR      : . { System.err.println("Invalid char: "+$text); lexicalErrors++; $channel=HIDDEN; } ; 

